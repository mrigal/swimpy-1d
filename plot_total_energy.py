#! /usr/bin/python2

import numpy as np
import matplotlib.pyplot as plt

totalEnergy = np.loadtxt("total_energy.dat")
# print(totalEnergy)
plt.plot(totalEnergy[:,0], totalEnergy[:,1])
plt.xlabel("Time")
plt.ylabel("Total energy")
plt.title("Iterative kinetic scheme")
plt.grid()
# plt.show()

plt.savefig("iterative_scheme_total_energy.pdf")
