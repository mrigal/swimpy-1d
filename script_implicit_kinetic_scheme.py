#! /usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
from lib.SaintVenant1D import *
from lib.Mesh1D import *
from lib.OutputManager import *
from lib.numerical_schemes.ImplicitKineticScheme import *
from lib.time_integrators.CustomTimeIntegrator import *
from lib.reconstruction_operators import *

def bathymetry(x):
    # return (1 + np.cos(4*np.pi*x))*(x >= .25)*(x < .75) - 2.5
    return -2.5*np.ones(x.shape)

def initCond_h(x):
    #return 0.01*np.sin(2*np.pi*x) - bathymetry(x)
    return - bathymetry(x)

def initCond_q(x):
    #return .75*initCond_h(x)
    return -0*np.ones(x.shape)

def mixedBoundaryConditionsType(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    types = np.empty(nComponents)
    if X == 0:
        types[0] = SchemeClass.NEUMANN_CONDITION
        types[1] = SchemeClass.NEUMANN_CONDITION
    elif X == 1:
        types[1] = SchemeClass.NEUMANN_CONDITION
        types[0] = SchemeClass.NEUMANN_CONDITION
    else:
        raise ValueError("Unexpected boundary position.")
    return types

def mixedBoundaryConditionsValue(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    values = np.empty(nComponents)
    if X == 0:
        values[0] = 0. #2.5*(1 - .05*np.sin(time/4))
        values[1] = 0.
    elif X == 1:
        values[0] = 0.
        values[1] = 0. #2.5*.25*(1 + .2*np.sin(3*time))
    else:
        raise ValueError("Unexpected boundary position.")
    return values

def boundaryConditionsType(X, time):
    raise NotImplementedError

def boundaryConditionsValue(X, time):
    # nComponents = SaintVenant1D.SYSTEM_SIZE
    # values = np.empty(nComponents)
    #Enforce water height given by initial condition to preserve lake at rest
    value = initCond_h(X)
    return value

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    line[0][0].set_data(X[ic], W[ic,0] + z[ic])
    line[0][1].set_data(X[ic], z[ic])
    line[1][0].set_data(X[ic], W[ic,1])
    # figure.canvas.draw()
    figure.canvas.flush_events()
    # plt.draw()

if "__main__" == __name__:

    #Setting the conservation law and bathymetry
    g = 9.81
    cl = SaintVenant1D()
    cl.SetGravityConstant(g)
    cl.SetBathymetry(bathymetry)

    #Setting the mesh
    xMin = 0
    xMax = 1
    numberOfCells = 64
    numberOfGhosts = 2 #Should be even, required for boundary conditions
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()

    #Setting the time integrator
    initialTime = 0
    finalTime = .0000005

    # ti = ExplicitRungeKutta()
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    #Setting the output manager
    numberOutputs = 5000
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    om = OutputManager()
    om.SetOutputTimes(outputTimes)

    # subplotGrid = [121, 122]
    # styles = ['b-', 'r-']
    # om.SetPlotRealTime(plotter_h, subplots=subplotGrid, linestyles=styles)

    plt.ion()
    figure = plt.figure(figsize=(9, 3.5))
    # figure.tight_layout()
    plt.subplots_adjust(left=0.075, right=0.925, top=0.9, bottom=0.15)
    axis = []
    lines = []
    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(121))
    # axis[-1].axis([0, 1, -2.5, 1])
    axis[-1].axis([0, 1, -10e-10, 10e-10])
    axis[-1].set_title("Free surface")
    lines.append(axis[-1].plot([], [], 'b:', [], [], 'k-'))
    lines[-1][0].set_label("Free surface")
    lines[-1][1].set_label("Bathymetry")
    axis[-1].legend()
    plt.grid()
    #Discharge in a separate subfigure
    axis.append(figure.add_subplot(122))
    # axis[-1].axis([0, 1, -1.5, 2])
    axis[-1].axis([0, 1, -5*10e-2, 5*10e-2])
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'r--'))
    lines[-1][0].set_label("Discharge")
    axis[-1].legend()
    plt.grid()
    #Uploading features into output manager
    om.SetRealTimePlotFigure(figure)
    om.SetRealTimePlotAxis(axis)
    om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    #Setting the scheme with boundary conditions
    CFL_number = .45
    # scheme = Rusanov()
    # scheme = HLL()
    # scheme = RoeScheme()
    scheme = ImplicitKineticScheme()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    # scheme.SetReconstructionOperator(hydrostaticReconstruction, name="hydroRecO1")
    # scheme.SetReconstructionOperator(
    #     secondOrderHydrostaticReconstruction, name="hydroRecO2"
    # )
    # scheme.SetLimiter(minmodLimiter, name="minmod")
    # scheme.SetMixedBNDC(
    #     mixedBoundaryConditionsType,
    #     mixedBoundaryConditionsValue
    # )
    scheme.SetPeriodicBNDC()
    # scheme.SetWeaklyPrescribedSubsonicRiemannBNDC_H(
    #     boundaryConditionsType,
    #     boundaryConditionsValue
    # )

    om.ActivatePlotRealTime()
    om.DeactivateDumpDataToFile()

    scheme.Solve(cl, mesh, initialCondition, om, silent=False)

    om.DeactivatePlotRealTime()
