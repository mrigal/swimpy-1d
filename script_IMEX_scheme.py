#! /usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from lib import LAR_splitting, Mesh1D, OutputManager
from lib.numerical_schemes import *
from lib.time_integrators import Linear_IMEX_DIRK
from lib.reconstruction_operators import *

# def bathymetry(x):
#     return (1 + np.cos(4*np.pi*x))*(x >= .25)*(x < .75) - 2.5
#     # return -2.5*np.ones(x.shape)

# def initCond_h(x):
#     return 0.01*np.sin(2*np.pi*x) - bathymetry(x)
#     # return - bathymetry(x)

# def initCond_q(x):
#     #return .75*initCond_h(x)
#     return -0*np.ones(x.shape)

def bathymetry(x):
    return (1 + np.cos(4*np.pi*x))*(x >= .25)*(x < .75) - 2.5
    # return -2.5*np.ones(x.shape)

def initCond_h(x):
    # return 0.1*np.sin(2*np.pi*x) - bathymetry(x)
    return - bathymetry(x)

def initCond_q(x):
    #return .75*initCond_h(x)
    return -0*np.ones(x.shape)

def mixedBoundaryConditionsType(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    types = np.empty(nComponents)
    if X == 0:
        types[0] = SchemeClass.NEUMANN_CONDITION
        types[1] = SchemeClass.NEUMANN_CONDITION
    elif X == 1:
        types[1] = SchemeClass.NEUMANN_CONDITION
        types[0] = SchemeClass.NEUMANN_CONDITION
    else:
        raise ValueError("Unexpected boundary position.")
    return types

def mixedBoundaryConditionsValue(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    values = np.empty(nComponents)
    if X == 0:
        values[0] = 0. #2.5*(1 - .05*np.sin(time/4))
        values[1] = 0.
    elif X == 1:
        values[0] = 0.
        values[1] = 0. #2.5*.25*(1 + .2*np.sin(3*time))
    else:
        raise ValueError("Unexpected boundary position.")
    return values

def boundaryConditionsType(X, time):
    raise NotImplementedError

def boundaryConditionsValue(X, time):
    # nComponents = SaintVenant1D.SYSTEM_SIZE
    # values = np.empty(nComponents)
    #Enforce water height given by initial condition to preserve lake at rest
    value = initCond_h(X)
    return value

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    line[0][0].set_data(X[ic], W[ic,0] + z[ic])
    line[0][1].set_data(X[ic], z[ic])
    line[1][0].set_data(X[ic], W[ic,1])
    # figure.canvas.draw()
    figure.canvas.flush_events()
    # plt.draw()

if "__main__" == __name__:

    #Setting the conservation law and bathymetry
    g = 9.81
    # cl = SaintVenant1D()
    cl = LAR_splitting()
    cl.SetGravityConstant(g)
    cl.SetBathymetry(bathymetry)

    #Setting the mesh
    xMin = 0
    xMax = 1
    numberOfCells = 64
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()

    #Setting the time integrator
    initialTime = 0
    finalTime = .5

    # ti = ExplicitRungeKutta()
    # ti = CustomTimeIntegrator()
    ti = Linear_IMEX_DIRK()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    #Butcher tableau corresponding to the Heun method (trapezoidal rule)
    # AE = np.array([[0]]) #Partial update weights
    # bE = np.array([1])   #Final update weights
    # cE = np.array([0])   #Partial time stepping
    # AI = np.array([[1]]) #Partial update weights
    # bI = np.array([1])   #Final update weights
    # cI = np.array([1])   #Partial time stepping
    # ti.SetButcherTableau(AE, bE, cE, AI, bI, cI, name="EulExp-EulImp")
    # AE = np.array([[0]]) #Partial update weights
    # bE = np.array([1])   #Final update weights
    # cE = np.array([0])   #Partial time stepping
    # ti.SetButcherTableau(AE, bE, cE, AE, bE, cE, name="EulExp")
    # AE = np.array([[0, 0], [1, 0]]) #Partial update weights
    # bE = np.array([.5, .5])   #Final update weights
    # cE = np.array([0, 1])   #Partial time stepping
    # ti.SetButcherTableau(AE, bE, cE, AE, bE, cE, name="Trapeze/Heun")

    ############################################################################
    #Order 1 in time + order 2 in space = instability!
    
    # #Eplicit Euler
    # c = 0.;
    # A = 0.;
    # b = 1.;
    # 
    # #Implicit Euler
    # cI = 1.;
    # AI = 1.;
    # bI = 1.;
    
    # #Midpoint
    # c = [0; .5];
    # A = [0, 0; .5, 0];
    # b = [0, 1];
    
    # #Heun or trapeze
    # c = [0; 1];
    # A = [0, 0; 1, 0];
    # b = [.5, .5];
    
    # #Raltson
    # c = [0.; 2./3.];
    # A = [0., 0.; 2./3., 0.];
    # b = [.25, .75];
    
    # #RK-4
    # c = [0.; .5; .5; 1.];
    # A = [0., 0., 0., 0.; .5, 0., 0., 0.; 0., .5, 0., 0.; 0., 0., 1., 0.];
    # b = [1./6., 1./3., 1./3., 1./6.];
    
    # # 2nd order
    # cE = [0.; .5];
    # AE = [0., 0.; .5, 0.];
    # bE = [0., 1.];
    # scheme.setRungeKuttaNonstiff(cE, AE, bE)
    # 
    # cI = [0; 1];
    # AI = [0., 0.; .5, .5];
    # bI = [.5, .5];
    # scheme.setRungeKuttaStiff(cI, AI, bI);
    
    # #Negative coefficient
    # cE = [0.; 1.];
    # AE = [0., 0.; 1., 0.];
    # bE = [.5, .5];
    # scheme.setRungeKuttaNonstiff(cE, AE, bE)
    # 
    # cI = [-1; 2];
    # AI = [-1., 0.; 1., 1.];
    # bI = [.5, .5];
    # scheme.setRungeKuttaStiff(cI, AI, bI);
    
    #
    
    # #Euler #Warning: might be unstable due to 2nd order space accuracy in wave treatment
    # c = 0.;
    # A = 0.;
    # b = 1.;
    # cI = 0;
    # AI = 1;
    # bI = 1;
    
    #Explicit Euler + Crank Nicolson
    # c = [0.; 1.];
    # A = [0., 0.; 0., 0.];
    # b = [1., 0.];
    # cI = [0.; 1.];
    # AI = [0., 0.; .5, .5];
    # bI = [.5, .5];
    
    # #L-stable 2nd order 2 stages
    # c = [0.; 1.];
    # A = [0., 0.; 1., 0.]; #Heun
    # b = [.5, .5];
    # gamma = .5*(2 - sqrt(2)); #1 - 1/sqrt(2);
    # cI = [gamma; 1-gamma];
    # AI = [gamma, 0.; 1-2*gamma, gamma];
    # bI = [.5, .5];
    
    # #L-stable 2nd order 3 stages
    # AE = np.array([[0., 0., 0.], [.5, 0., 0.], [.5, .5, 0.]])
    # bE = np.array([1./3., 1./3., 1./3.])
    # cE = np.array([[0.], [.5], [1.]])
    # AI = np.array([[.25, 0., 0.], [0., .25, 0.], [1./3., 1./3., 1./3.]])
    # bI = [1./3., 1./3., 1./3.];
    # cI = [.25; .25; 1.];

    #GSA L-stable 2nd order 3 stages
    gamma = 1 - .5*np.sqrt(2)
    delta = 1 - .5/gamma
    AE = np.array([[0., 0., 0.], [gamma, 0., 0.], [delta, 1-delta, 0.]])
    bE = np.array([delta, 1-delta, 0.])
    cE = np.array([[0.], [gamma], [1.]])
    AI = np.array([[0., 0., 0.], [0., gamma, 0.], [0., 1-gamma, gamma]])
    bI = np.array([0., 1-gamma, gamma])
    cI = np.array([[0], [gamma], [1.]])
    ti.SetButcherTableau(AE, bE, cE, AI, bI, cI, name="ARS-222")
    
    # #Test!
    # c = [0;.5;.5;1;0;.5;.5;1];
    # A = [0,0,0,0,0,0,0,0;
    #      .5,0,0,0,0,0,0,0;
    #      .5,0,0,0,0,0,0,0;
    #      .5,0,.5,0,0,0,0,0;
    #      0,0,0,0,0,0,0,0;
    #      -.5,.75,0,-.25,.5,0,0,0;
    #      -.5,.75,0,-.25,.5,0,0,0;
    #      -.5,1,-.5,0,.5,0,.5,0];
    # b = [-.5,1,-.5,0,.5,0,.5,0];
    # 
    # cI = [.5,.5,1,1,.5,.5,1,1];
    # AI = [.5,0,0,0,0,0,0,0;
    #       .5,0,0,0,0,0,0,0;
    #       .5,0,.5,0,0,0,0,0;
    #       .5,0,.5,0,0,0,0,0;
    #       -.5,.75,0,-.25,.5,0,0,0;
    #       -.5,.75,0,-.25,.5,0,0,0;
    #       -.5,1,-.5,0,.5,0,.5,0;
    #       -.5,1,-.5,0,.5,0,.5,0];
    # bI = [-.5,1,-.5,0,.5,0,.5,0];
    
    # # 2nd order
    # c = [0.; .5];
    # A = [0., 0.; .5, 0.];
    # b = [0., 1.];
    # cI = [0; 1];
    # AI = [0., 0.; .5, .5];
    # bI = [.5, .5];

    ############################################################################

    #Setting the output manager
    numberOutputs = 200
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    om = OutputManager()
    om.SetOutputTimes(outputTimes)

    # subplotGrid = [121, 122]
    # styles = ['b-', 'r-']
    # om.SetPlotRealTime(plotter_h, subplots=subplotGrid, linestyles=styles)

    plt.ion()
    figure = plt.figure(figsize=(9, 3.5))
    # figure.tight_layout()
    plt.subplots_adjust(left=0.075, right=0.925, top=0.9, bottom=0.15)
    axis = []
    lines = []
    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(121))
    # axis[-1].axis([0, 1, -2.5, 1])
    axis[-1].axis([0, 1, -10e-13, 10e-13])
    axis[-1].set_title("Free surface")
    lines.append(axis[-1].plot([], [], 'b:', [], [], 'k-'))
    lines[-1][0].set_label("Free surface")
    lines[-1][1].set_label("Bathymetry")
    axis[-1].legend()
    plt.grid()
    #Discharge in a separate subfigure
    axis.append(figure.add_subplot(122))
    # axis[-1].axis([0, 1, -1.5, 2])
    axis[-1].axis([0, 1, -5*10e-13, 5*10e-13])
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'r--'))
    lines[-1][0].set_label("Discharge")
    axis[-1].legend()
    plt.grid()
    #Uploading features into output manager
    om.SetRealTimePlotFigure(figure)
    om.SetRealTimePlotAxis(axis)
    om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    #Setting the scheme with boundary conditions
    CFL_number = .25

    ############################################################################
    scheme = SplittedSchemeClass()
    slowOperator = HLL_hConst() #Constant water height required for well-balancedness
    # slowOperator = Rusanov_hConst()
    ro = MUSCL_O2()
    slowOperator.SetReconstructionOperator(ro)
    fastOperator = FD_centered_gravity_waves_O2()
    scheme.SetSlowOperatorDiscretization(slowOperator)
    scheme.SetFastOperatorDiscretization(fastOperator)
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    # scheme.SetNumericalViscosity(10)
    scheme.SetNumericalViscosity(1.125)
    scheme.SetPeriodicBNDC()
    ############################################################################

    om.ActivatePlotRealTime()
    om.DeactivateDumpDataToFile()

    scheme.Solve(cl, mesh, initialCondition, om, silent=False)

    om.DeactivatePlotRealTime()
