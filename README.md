# swimpy-1d

This program provides implicit solvers (and more) for the one dimensionnal shallow water system in a Python framework.

$$
\begin{align*}
&
\partial_t h + \partial_x (hu) = 0 \\
&
\partial_t (hu) + \partial_x (hu^2 + gh^2/2) = -ghz'
\end{align*}
$$

It features explicit and implicit kinetic solvers alongside hydrostatic reconstruction for preserving the lakes at rest and the positivity.

## Examples

Scripts from the `examples` folder provide minimalist examples to get started quickly. They can be executed from the swimpy-1d folder (containing `lib/`) with the following command:
```bash
python -m examples.script_name
```
Note that the script name doesn't include the .py extension. For instance:
```bash
python -m examples.generate_uniform_mesh
```
Alternatively, these scripts can be copied in the swimpy-1d repertory and executed without the `-m` flag.

## Mesh

The mesh can be generated with uniformly spaced vertices or not. In the uniform case, the domain boundaries, the number of interior cells and the number of ghost cells have to be specified before calling the `GenerateMesh()` method:
```Python
from lib.Mesh1D import Mesh1D

mesh = Mesh1D()
mesh.SetDomainBounds(0, 1)
mesh.SetUniformMesh(50) #Number of interior cells
mesh.SetNumberOfGhosts(2) #Should be even
mesh.GenerateMesh()
```
In the nonuniform case, a list or a 1d array containing the vertices coordinates has to be specified by the user through the method `SetNonuniformMesh(listOfVertices)`. If the ghost cells are provided, the key `ghosts=True` has to be set. Otherwise ghost cells are automatically generated using the size of the left- and right-most interior cells. In either case, the domain boundaries are computed accordingly.
```Python
from lib.Mesh1D import Mesh1D

listOfVertices = [-.125, 0, .125, .375, .625, .875, 1, 1.125]
mesh = Mesh1D()
mesh.SetNonuniformMesh(listOfVertices, ghosts=True)
mesh.SetNumberOfGhosts(2)
mesh.GenerateMesh()
```


## Conservation law and bathymetry

A conservation law has to be defined for the numerical scheme to access the flux, eigenvalues and bathymetry among other things.

Currently, the only conservation law implemented is `SaintVenant1D`, corresponding to the 1d shallow water system in $(h,q)$ coordinates. It is initialized by either specifying the gravity constant $g$ or the Froude characteristic number. A bathymetry has to be given as a function accepting a numpy 1d array as input, and returning a numpy 1d array of the same size.
```Python
import numpy as np
from lib.SaintVenant1D import SaintVenant1D

def flat_bathymetry(verticesArray):
	return -np.ones(verticesArray.shape)
	
g = 9.81
consevationLaw = SaintVenant1D()
consevationLaw.SetGravityConstant(g)
consevationLaw.SetBathymetry(flat_bathymetry)
```

## Time integrator

The purpose of time integrators is to discretize the time derivative appearing in the conservation law. They allow to update the numerical solution through a given time step with some spatial discretization. In *swimpy-1d* the time integrator is specified alongside the starting and ending times of the simulation. The following example loads the explicit Euler time integrator:
```Python
from lib.time_integrators.ExplicitEuler import ExplicitEuler

timeIntegrator = ExplicitEuler()
initialTime = 0
finalTime = 1.5
timeIntegrator.SetStartingTime(initialTime)
timeIntegrator.SetEndingTime(finalTime)
```

Next we illustrate how to set a generic explicit Runge-Kutta time integrator:
```Python
import numpy as np
from lib.time_integrators.ExplicitRungeKutta import ExplicitRungeKutta

#Setting the time integrator
initialTime = 0
finalTime = 1.5
timeIntegrator = ExplicitRungeKutta()
timeIntegrator.SetStartingTime(initialTime)
timeIntegrator.SetEndingTime(finalTime)

#Butcher tableau corresponding to the Heun method (trapezoidal rule)
A = np.array([[0, 0], [1, 0]]) #Partial update weights
b = np.array([.5, .5])         #Final update weights
c = np.array([0, 1])           #Partial time stepping
timeIntegrator.SetButcherTableau(A, b, c, name="Heun") #The name is optional
```


## Output manager

There are two ways of outputing numerical results in swimpy-1d: either by plotting the results in real time, or by dumping them into a data file for post-processing.

### Real time plotting
First, a plotter has to be defined. It is a function of the output manager itself, conservation law, unknown state, mesh and scheme:
```Python
import matplotlib.pyplot as plt
from lib.OutputManager import *

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    line[0][0].set_data(X[ic], W[ic,0] + z[ic]) #Free surface and bathymetry on the
    line[0][1].set_data(X[ic], z[ic])           #same subfigure
    line[1][0].set_data(X[ic], W[ic,1]) #Discharge on a second subfigure
    figure.canvas.flush_events()
```
Then the output manager has to be set. For instance:
```Python
initialTime = 0; finalTime = 1.5

#Setting the output times
om = OutputManager()
outputTimes = np.linspace(initialTime, finalTime, 100)
om.SetOutputTimes(outputTimes)

plt.ion() #Instant refresh
figure = plt.figure(figsize=(9, 3.5))
axis = []; lines = []

#Free surface and bathymetry on the same subfigure
axis.append(figure.add_subplot(121))
axis[-1].axis([0, 1, -2.5, 1]) #Can be incorporated in the plotter for dynamic axis
axis[-1].set_title("Free surface")
lines.append(axis[-1].plot([], [], 'b:', [], [], 'k-'))
lines[-1][0].set_label("Free surface")
lines[-1][1].set_label("Bathymetry")
axis[-1].legend()

#Discharge in a separate subfigure
axis.append(figure.add_subplot(122))
axis[-1].axis([0, 1, -1.5, 2])
axis[-1].set_title("Discharge")
lines.append(axis[-1].plot([], [], 'r--'))
lines[-1][0].set_label("Discharge")
axis[-1].legend()

#Interfacing plotter features with output manager
om.SetRealTimePlotFigure(figure)
om.SetRealTimePlotAxis(axis)
om.SetRealTimePlotLines(lines)
om.SetRealTimePlotter(plotter)

om.ActivatePlotRealTime()
```

### Dump to data file
Start by defining a data dumper with the same signature as in the following exampe:
```Python
import numpy as np
from lib.Mesh1D import Mesh1D

def dump_data_to_file(om, cl, W, mesh, scheme, opts):
    ic = mesh.GetInteriorCellsRange()
    X = mesh.GetCellCentersArray()[ic]
    array = np.concatenate((X[:,np.newaxis], W), axis=1)
    fileID = om.OpenDataFile(binary=False, mode="w", ext="txt")
    fileID.write("# Header; Position, Free surface, Discharge, Bathymetry\n")
    np.savetxt(fileID, array, fmt="%g")
    fileID.close()
```
Then the output manager has to be set as below:
```Python
mesh = Mesh1D()
numberOfCells = 4;
mesh.SetDomainBounds(0, 1)
mesh.SetUniformMesh(numberOfCells)
mesh.SetNumberOfGhosts(2)
mesh.GenerateMesh()

#Setting the output manager
om = OutputManager()
initialTime = 0; finalTime = 1.5
outputTimes = np.linspace(initialTime, finalTime, 50)
om.SetOutputTimes(outputTimes)

om.SetBaseFilename("testing_output")
om.SetDumpDataToFile(dump_data_to_file)

#Now we dump some data to a file for the sake of the example
om.ResetOutputCounter()

# Data array
dataArray = np.array(
    [[0., 0.25, -0.5],
     [-0.1, 0.25, -0.4],
     [-0.1, 0.125, -0.3],
     [0., 0., -0.2]]
)

om.ActivateDumpDataToFile() # By default it is deactivated
om.DoDumpDataToFile(None, dataArray, mesh, None, None)
om.IncreaseOutputCounter()
om.DeactivateDumpDataToFile()
```

## Setting the numerical scheme and running the solver

### Choosing the scheme
An instance of the scheme is created using the syntax `scheme = <SchemeName>()`, after the corresponding package/library has been loaded with
```Python
from lib.numerical_schemes.<SchemeName> import <SchemeName>
```

The aforementionned `<SchemeName>` is any member of the following list:

- Rusanov;
- HLL;
- Roe;
- ExplicitKineticScheme;

Available soon (?):
- ImplicitKineticScheme;
- ...

The CFL number is chosen with `scheme.SetCFLNumber(CFL_number)`. Its value is defaulted to 0.5. 

### Linking the time integrator
The time integrator previously defined is linked to the scheme with the `SetTimeIntegrator` method:
```Python
scheme.SetTimeIntegrator(timeIntegrator)
```

### Defining the boundary conditions

It is necessary to define some boundary conditions due to the boundedness of the computational domain. Those limit conditions are enforced by filling the ghost cells generated with the mesh. Hence the scheme will need to know what kind of boundary conditions are enforced (Dirichlet, Neumann, Periodic, ...). There are several categories of boundary conditions:
* All periodic boundary conditions:
```Python
scheme.SetPeriodicBoundaryConditions()
```
* Mixed boundary conditions: possibility to mix any standard conditions on a per component basis (Dirichlet, Neumann or periodic). We proceed as follows:
```Python
def mixedBoundaryConditionsType(X, time):
    types = np.empty(2)
    types[0] = \ #Water height condition types
        SchemeClass.DIRICHLET_CONDITION*(0 == X) + SchemeClass.NEUMANN_CONDITION*(1 == X)
    types[1] = \ #Discharge condition types
        SchemeClass.DIRICHLET_CONDITION*(1 == X) + SchemeClass.NEUMANN_CONDITION*(0 == X)
    return types

def mixedBoundaryConditionsValue(X, time):
    values = np.empty(2)
    values[0] = \ #Water height condition values
        2.5*(1-.05*np.sin(time/2))*(0 == X) + 0*(1 == X)
    values[1] = \ #Discharge condition values
        0.625*(1-.2*np.sin(time/2))*(1 == X) + 0*(0 == X)
    return values

scheme.SetMixedBoundaryCondtions(
    mixedBoundaryConditionsType,
    mixedBoundaryConditionsValue
)
```


### Reconstruction operator
Reconstruction operators such as the 1st and 2nd order hydrostatic reconstruction, or the MUSCL reconstruction with limiter, can be set via the method:
```Python
scheme.SetReconstructionOperator(reconstructionOperator, name="reconstructionOperatorName")
```
Available reconstruction operators are listed below:

- 2nd order MUSCL with minmod limiter;
- 1st order hydrostatic reconstruction (requires at least 2 ghost cells);
- 2nd order hydrostatic reconstruction (requires at least 4 ghost cells);

### Running the solver
Once those parameters have been set, we run the solver with the `Solve()` method:
```Python
scheme.Solve(conservationLaw, mesh, initialCondition, outputManager)
```

The initial condition is given by the list `initialCondition` whose length must be equal to the number of quantities of interest defined in the conservation law (e.g. `conservationLaw.SYSTEM_SIZE`, see below), and whose components must be functions accepting a 1d numpy array and returning a 1d numpy array of the same size:
```Python
import numpy as np
def water_height(X):
	return np.ones(X.shape)
def discharge(X):
	return np.zeros(X.shape)
initialCondition = [water_height, discharge]
```
