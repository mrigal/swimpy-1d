#! /usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from lib import SaintVenant1D, LAR_splitting, Mesh1D, OutputManager
from lib.numerical_schemes import *
from lib.time_integrators import ExplicitRungeKutta, Linear_IMEX_DIRK
from lib.reconstruction_operators import *

FR = 10**(-3)
Xmin = -2/FR
Xmax =  2/FR
L = Xmax - Xmin

def bathymetry(x):
    return -np.ones(x.shape)
    # return -1 + (-.75<=FR*x)*(FR*x<=.75)*.125*(1 + np.sin(-8*np.pi*FR*x/3 - .5*np.pi))

def initCond_h(x):
    z = bathymetry(x)
    return -z + 10*FR*FR*np.sin(np.pi*FR*x) + 10*FR*FR*z

def initCond_q(x):
    return 1 + 5*FR*(1 + np.sin(4*np.pi/L*FR*x + .5*np.pi))

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    line[0][0].set_data(X[ic], W[ic,0] + z[ic])
    line[0][1].set_data(X[ic], z[ic])
    line[1][0].set_data(X[ic], W[ic,1])
    time = om.GetNextOutputTime()
    finalTime = om.GetFinalOutputTime()
    figure.suptitle("Time={:.5f} / {}".format(time, finalTime))
    figure.canvas.flush_events()
    # plt.pause(2)
    # axis[0].plot()
    # axis[1].plot()
    # plt.show(block=True)

if "__main__" == __name__:

    #Setting the conservation law and bathymetry
    cl = LAR_splitting()
    cl.SetFroudeCaracteristicNumber(FR)
    cl.SetBathymetry(bathymetry)

    #Setting the mesh
    numberOfCells = 64
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh = Mesh1D()
    mesh.SetDomainBounds(Xmin, Xmax)
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()

    #Setting the time integrator
    initialTime = 0
    # finalTime = 0.9995
    finalTime = 10

    ti = Linear_IMEX_DIRK()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    #Butcher tableau corresponding to the Heun method (trapezoidal rule)
    # AE = np.array([[0]]) #Partial update weights
    # bE = np.array([1])   #Final update weights
    # cE = np.array([0])   #Partial time stepping
    # ti.SetButcherTableau(AE, bE, cE, AE, bE, cE, name="EulExp")
    # AE = np.array([[0, 0], [1, 0]]) #Partial update weights
    # bE = np.array([.5, .5])   #Final update weights
    # cE = np.array([0, 1])   #Partial time stepping
    # ti.SetButcherTableau(AE, bE, cE, AE, bE, cE, name="Trapeze/Heun")

    ############################################################################

    
    # #L-stable 2nd order 3 stages
    # AE = np.array([[0., 0., 0.], [.5, 0., 0.], [.5, .5, 0.]])
    # bE = np.array([1./3., 1./3., 1./3.])
    # cE = np.array([[0.], [.5], [1.]])
    # AI = np.array([[.25, 0., 0.], [0., .25, 0.], [1./3., 1./3., 1./3.]])
    # bI = np.array([1./3., 1./3., 1./3.])
    # cI = np.array([[.25], [.25], [1.]])
    # ti.SetButcherTableau(AE, bE, cE, AI, bI, cI, name="nonGSA")

    # #Non-GSA, O2, 2 stages
    # gamma = 1 - .5*np.sqrt(2)
    # delta = 1 - .5/gamma
    # AE = np.array([[0., 0., 0.], [gamma, 0., 0.], [delta, 1-delta, 0.]])
    # bE = np.array([delta, 1-delta, 0.])
    # cE = np.array([[0.], [gamma], [1.]])
    # AI = np.array([[0., 0., 0.], [0., gamma, 0.], [0., 1-gamma, gamma]])
    # bI = np.array([0., 1-gamma, gamma])
    # cI = np.array([[0], [gamma], [1.]])
    # ti.SetButcherTableau(AE, bE, cE, AI, bI, cI, name="ARS-222")

    #GSA, O2, L-stable, 3 stages
    gamma = 1 - .5*np.sqrt(2)
    delta = 1 - .5/gamma
    AE = np.array([[0., 0., 0.], [gamma, 0., 0.], [delta, 1-delta, 0.]])
    bE = np.array([delta, 1-delta, 0.])
    cE = np.array([[0.], [gamma], [1.]])
    AI = np.array([[0., 0., 0.], [0., gamma, 0.], [0., 1-gamma, gamma]])
    bI = np.array([0., 1-gamma, gamma])
    cI = np.array([[0], [gamma], [1.]])
    ti.SetButcherTableau(AE, bE, cE, AI, bI, cI, name="ARS-222")
    
    ############################################################################

    #Setting the output manager
    numberOutputs = 200
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    # outputTimes = [finalTime]
    om = OutputManager()
    om.SetOutputTimes(outputTimes)

    # subplotGrid = [121, 122]
    # styles = ['b-', 'r-']
    # om.SetPlotRealTime(plotter_h, subplots=subplotGrid, linestyles=styles)

    plt.ion()
    # figure = plt.figure(figsize=(9, 3.5))
    figure = plt.figure(figsize=(12, 5))
    # figure.tight_layout()
    plt.subplots_adjust(left=0.075, right=0.925, top=0.9, bottom=0.15)
    axis = []
    lines = []
    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(121))
    axis[-1].axis([Xmin, Xmax, -22.5*FR*FR, 2.5*FR*FR])
    # axis[-1].axis([Xmin, Xmax, -1, 10000*2.5*FR*FR])
    # axis[-1].axis([Xmin, Xmax, -1, 0.5])
    axis[-1].set_title("Free surface")
    lines.append(axis[-1].plot([], [], 'b:', [], [], 'k-'))
    lines[-1][0].set_label("Free surface")
    lines[-1][1].set_label("Bathymetry")
    axis[-1].legend()
    plt.grid()
    #Discharge in a separate subfigure
    axis.append(figure.add_subplot(122))
    axis[-1].axis([Xmin, Xmax, 1, 1+20*FR])
    # axis[-1].axis([Xmin, Xmax, 1+10*FR-.5*10**(-2), 1+10*FR+.5*10**(-2)])
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'r--'))
    lines[-1][0].set_label("Discharge")
    axis[-1].legend()
    plt.grid()
    #Uploading features into output manager
    om.SetRealTimePlotFigure(figure)
    om.SetRealTimePlotAxis(axis)
    om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    #Setting the scheme with boundary conditions
    CFL_number = .25

    ############################################################################
    # cl = SaintVenant1D()
    # cl.SetFroudeCaracteristicNumber(FR)
    # cl.SetBathymetry(bathymetry)
    # ti = ExplicitRungeKutta()
    # A = np.array([[0, 0], [1, 0]]) #Partial update weights
    # b = np.array([.5, .5])         #Final update weights
    # c = np.array([0, 1])           #Partial time stepping
    # ti.SetButcherTableau(A, b, c, name="Heun")
    # ti.SetStartingTime(initialTime)
    # ti.SetEndingTime(finalTime)
    # # scheme = HLL()
    # # scheme = Rusanov()
    # scheme = RoeScheme()
    # # scheme = ExplicitKineticScheme()
    # scheme.SetTimeIntegrator(ti)
    # CFL_number = .25/10
    # scheme.SetCFLNumber(CFL_number)
    # scheme.SetNumericalViscosity(1.125)
    # # ro = NoReconstruction()
    # # ro = HydroRec()
    # ro = HydroRec_O2()
    # # ro = MUSCL_O2()
    # # ro.SetLimiter(minmodLimiter, name="minmod")
    # scheme.SetReconstructionOperator(ro)
    # scheme.SetPeriodicBNDC()
    ############################################################################
    scheme = SplittedSchemeClass()
    slowOperator = HLL_hConst() #Constant water height required for well-balancedness
    # slowOperator = Rusanov()
    # ro = NoReconstruction()
    ro = MUSCL_O2()
    # ro.SetLimiter(minmodLimiter, name="minmod")
    slowOperator.SetReconstructionOperator(ro)
    fastOperator = FD_centered_gravity_waves_O2()
    scheme.SetSlowOperatorDiscretization(slowOperator)
    scheme.SetFastOperatorDiscretization(fastOperator)
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    scheme.SetPeriodicBNDC()
    ############################################################################

    om.ActivatePlotRealTime()
    om.DeactivateDumpDataToFile()

    scheme.Solve(cl, mesh, initialCondition, om, silent=False)

    om.DeactivatePlotRealTime()
