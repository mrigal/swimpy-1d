import time as tm
import numpy as np
import matplotlib.pyplot as plt

class OutputManager(object):

    def __init__(self):
        self.CPU_time = 0
        self.OutputTimes = None
        self.PlotRealTime = None
        self.DumpDataToFile = None
        self.BaseFileName = None
        self.IsRealTimePlotActivated = False
        self.IsDumpDataToFileActivated = False
        self.Figure = None
        self.ax = None
        self.line = None
        self.Subplots = []
        self.NextOutput = None
        self.NumberOfOutputs = None

        self._StartTime = 0

    def __str__(self):
        string = \
            "OutputManager instance:\n" \
            "    Number of output times: {}\n" \
            "    Current output counter: {}\n" \
            "    Plot real time: {}\n" \
            "    Dump data to file: {}\n" \
            "    Base file name: {}".format(
                self.NumberOfOutputs,
                self.NextOutput,
                self.IsRealTimePlotActivated,
                self.IsDumpDataToFileActivated,
                self.BaseFileName
            )
        return string

    def ResetCPUTime(self):
        self.CPU_time = 0

    def StartMeasuringCPUTime(self):
        self._StartTime = tm.time()

    def StopMeasuringCPUTime(self):
        self.CPU_time += tm.time() - self._StartTime

    def GetCPUTime(self):
        return self.CPU_time

    def SetRealTimePlotter(self, function, subplots=[111], linestyles=None):
        if function is None:
            self.IsRealTimePlotActivated = False
        else:
            self.PlotRealTime = function

        # plt.ion()
        # self.Figure = plt.figure()
        # self.ax = []
        # self.line = []
        # if linestyles is None:
        #     for i, k in enumerate(subplots):
        #         self.ax.append(self.Figure.add_subplot(k))
        #         self.line.append(self.ax[i].plot([], [])[0])
        # else:
        #     for i, k in enumerate(subplots):
        #         self.ax.append(self.Figure.add_subplot(k))
        #         self.line.append(self.ax[i].plot([], [], linestyles[i], [], []))

    def SetDumpDataToFile(self, function):
        if function is None:
            self.IsDumpDataToFileActivated = False
        else:
            self.DumpDataToFile = function

    def SetRealTimePlotFigure(self, figure):
        self.Figure = figure

    def GetRealTimePlotFigure(self):
        return self.Figure

    def SetRealTimePlotAxis(self, axis):
        self.ax = axis

    def GetRealTimePlotAxis(self):
        return self.ax

    def SetRealTimePlotLines(self, lines):
        self.line = lines

    def GetRealTimePlotLines(self):
        return self.line

    def ActivatePlotRealTime(self):
        self.IsRealTimePlotActivated = True
        # self.Figure = plt.figure()
        # self.ax = self.Figure.add_subplot(111)
        # self.line = self.ax.plot([], [], 'r-')
        ###
        # plt.ion()
        # self.Figure, self.ax = plt.subplots(1, 2, figsize=(8,6))
        # self.line, = self.ax[0].plot([], [])
        ###
        # self.Figure = plt.figure()
        # self.ax = []
        # self.ax.append(self.Figure.add_subplot(121))
        # self.ax.append(self.Figure.add_subplot(122))
        # self.line = self.ax[0].plot([], [], 'r-')

    def DeactivatePlotRealTime(self):
        self.IsRealTimePlotActivated = False

    def ActivateDumpDataToFile(self):
        self.IsDumpDataToFileActivated = True

    def DeactivateDumpDataToFile(self):
        self.IsDumpDataToFileActivated = False

    def DoPlotRealTime(self, cl, W, mesh, scheme):
        if self.IsRealTimePlotActivated is True:
            self.PlotRealTime(self, cl, W, mesh, scheme)

    def SetBaseFilename(self, base):
        self.BaseFileName = base

    def GetBaseFilename(self):
        return self.BaseFileName

    def DoDumpDataToFile(self, cl, W, mesh, scheme):
        if self.IsDumpDataToFileActivated is True:
            self.DumpDataToFile(self, cl, W, mesh, scheme)

    def GetDataFileName(self, ext="dat"):
        width = max(1, np.ceil(np.log(self.NumberOfOutputs+1)/np.log(10)));
        formatedString = "{{}}_{{:0{:d}d}}.{{}}".format(int(width))
        fileName = formatedString.format(
            self.BaseFileName, self.NextOutput, ext
        )
        return fileName

    def OpenDataFile(self, binary=True, mode="w", ext="dat"):

        fileName = self.GetDataFileName(ext=ext)
        if binary is True:
            fileID = open(fileName, mode + "b")
        else:
            fileID = open(fileName, mode)

        return fileID

    def NextOutputTimeHasBeenReached(self, time):
        if self.NextOutput >= self.NumberOfOutputs:
            return False
        else:
            return (time >= self.OutputTimes[self.NextOutput])

    def SetOutputTimes(self, outputTimes):
        self.OutputTimes = np.array(outputTimes)
        self.NumberOfOutputs = self.OutputTimes.shape[0]

    def GetOutputTimes(self):
        return self.OutputTimes

    def GetOutputTimesBetween(self, t1, t2):
        if self.OutputTimes is None:
            return np.array([])
        indices = (self.OutputTimes > t1)*(self.OutputTimes <= t2)
        return self.OutputTimes[indices]

    def SetNumberOfOutputs(self, N):
        self.NumberOfOutputs = N
        self.NextOutput = 1

    def GetNumberOfOutputs(self):
        return self.NumberOfOutputs

    def IncreaseOutputCounter(self):
        self.NextOutput += 1

    def GetOutputCounter(self):
        return self.NextOutput

    def GetNextOutputTime(self):
        return self.OutputTimes[self.NextOutput]

    def GetFinalOutputTime(self):
        return self.OutputTimes[-1]

    def ResetOutputCounter(self):
        self.NextOutput = 0

    def ShowTimestepStats(self):
        pass
