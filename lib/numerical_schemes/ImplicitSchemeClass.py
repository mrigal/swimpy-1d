from .SchemeClass import SchemeClass

class ImplicitSchemeClass(SchemeClass):

    def __init__(self, ti=None):
        #No generic time integrator should be used with this scheme
        super(ImplicitSchemeClass, self).__init__(ti=ti)
        self.AccountForGravityWaves = True

    def SetResolveGravityTimescale(self, boolean):
        """
        Indicate if the CFL condition should account for surface gravity waves.
        """
        self.AccountForGravityWaves = boolean
        
    def ShouldReseolveGravityTimescale(self):
        """
        Indicate if the CFL condition should account for surface gravity waves.
        """
        return self.AccountForGravityWaves
