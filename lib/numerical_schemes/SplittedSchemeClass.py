import numpy as np
from .SchemeClass import *

class SplittedSchemeClass(SchemeClass):

    def __init__(self, ti=None):
        super(SplittedSchemeClass, self).__init__(ti=ti)

        self.discrSlowOperator = None
        self.discrFastOperator = None

        self.discrSlowOperatorName = None
        self.discrFastOperatorName = None

    def __str__(self):
        string = \
            "SplittedSchemeClass instance:\n" \
            "    Name: {}\n" \
            "    Discr. of slow operator: {}\n" \
            "    Discr. of fast operator: {}\n" \
            "    CFL number: {}\n" \
            "    Boundary conditions: {}\n" \
            "    Reconstruction operator: {}\n" \
            "    Limiter: {}".format(
                self.SchemeName, self.discrSlowOperatorName,
                self.discrFastOperatorName, #self.ti.GetTimeIntegratorName(),
                self.CFL_number, self.BoundaryConditionsName,
                self.ReconstructionOperatorName, self.LimiterName
            )
        return string

    def SetSlowOperatorDiscretization(self, scheme, scheme_name=None):
        self.discrSlowOperator = scheme
        self.discrSlowOperatorName = scheme_name

    def SetFastOperatorDiscretization(self, scheme, scheme_name=None):
        self.discrFastOperator = scheme
        self.discrFastOperatorName = scheme_name

    def GetSlowOperatorDiscretization(self):
        return self.discrSlowOperator

    def GetFastOperatorDiscretization(self):
        return self.discrFastOperator

    def ComputeSlowCellVariation(self, cl, mesh, U, time):
        cl.SwitchToSlowFlux()
        return self.GetSlowOperatorDiscretization().ComputeCellVariation(
            cl, mesh, U, time
        )
        # a, b, c =  self.GetSlowOperatorDiscretization().ComputeCellVariation(
        #     cl, mesh, U, time
        # )
        # return 0*a, 0*b, 0*c

    def ComputeFastCellVariation(self, cl, mesh, U, time):
        cl.SwitchToFastFlux()
        return self.GetFastOperatorDiscretization().ComputeCellVariation(
            cl, mesh, U, time
        )

    # def EstimateWaveVelocities__(self, cl, mesh, WL_, WR_):
    #     iv = mesh.GetInteriorVerticesRange()

    #     z = mesh.GetBathymetryArray()
    #     zL = z[iv-1]
    #     zR = z[iv]

    #     minL, maxL = cl.ComputeOuterSlowEigenvalues(WL_, zL)
    #     minR, maxR = cl.ComputeOuterSlowEigenvalues(WR_, zR)

    #     lambdaL = np.minimum(minL, minR)
    #     lambdaR = np.maximum(maxL, maxR)

    #     average = .5*(lambdaL + lambdaR)
    #     var = .5*(lambdaR - lambdaL)

    #     lambdaL = average - self.NumericalViscosity*var
    #     lambdaR = average + self.NumericalViscosity*var

    #     if np.all(0 == lambdaL) and np.all(0 != lambdaR):
    #         minL, maxL = cl.ComputeOuterFastEigenvalues(WL_, zL)
    #         minR, maxR = cl.ComputeOuterFastEigenvalues(WR_, zR)
            
    #         lambdaL = np.minimum(minL, minR)
    #         lambdaR = np.maximum(maxL, maxR)
            
    #         average = .5*(lambdaL + lambdaR)
    #         var = .5*(lambdaR - lambdaL)
            
    #         lambdaL = average - self.NumericalViscosity*var
    #         lambdaR = average + self.NumericalViscosity*var

    #     return lambdaL, lambdaR

    def AssembleMassMatrix(self, cl, mesh, i, dt):
        aii = self.ti.ButcherTableau_AI[i,i]
        return self.GetFastOperatorDiscretization().AssembleMassMatrix(
            cl, mesh, aii, dt
        )

    def AssembleRHS(self, cl, mesh, U, i, time, dt):
        aii = self.ti.ButcherTableau_AI[i,i]
        return self.GetFastOperatorDiscretization().AssembleRHS(
            cl, mesh, U, aii, time, dt
        )
