import numpy as np
from .SchemeClass import *

class Rusanov(SchemeClass):

    def __init__(self, ti=None):
        super(Rusanov, self).__init__(ti=ti)
        self.SchemeName = "Rusanov"

    def ComputeCellVariation(self, cl, mesh, W, time):
        iv = mesh.GetInteriorVerticesRange()
        
        z = mesh.GetBathymetryArray()
        zL = z[iv-1]
        zR = z[iv]

        WL_, WR_, CorrectionL_, CorrectionR_, zL_, zR_ = \
            self.ApplyReconstructionOperator(cl, mesh, W, time)

        FL = cl.ComputeFlux(WL_, zL_)
        FR = cl.ComputeFlux(WR_, zR_)

        lambdaL, lambdaR = self.EstimateWaveVelocities__(cl, mesh, WL_, WR_)

        lambdaMax = np.maximum(np.abs(lambdaL), np.abs(lambdaR))

        F_Rusanov = .5*(FL + FR - lambdaMax[:,np.newaxis]*(WR_ - WL_))
        
        return F_Rusanov, CorrectionL_, CorrectionR_

