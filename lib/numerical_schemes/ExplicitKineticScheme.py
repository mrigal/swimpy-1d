import numpy as np
from .SchemeClass import *

class ExplicitKineticScheme(SchemeClass):

    def __init__(self, ti=None):
        super(ExplicitKineticScheme, self).__init__(ti=ti)
        self.SchemeName = "Explicit kinetic scheme"
        self.ComputeFlux_PositiveXi = self.__UndefinedIntegral
        self.ComputeFlux_NegativeXi = self.__UndefinedIntegral
        self.MaxwellianName = None

    def __str__(self):
        string = super(ExplicitKineticScheme, self).__str__()
        string += \
            "\n    Maxwellian: {}".format(self.MaxwellianName)
        return string

    def SetMaxwellian_Index(self):
        self.ComputeFlux_PositiveXi = self.__Integral_PositiveXi_Index
        self.ComputeFlux_NegativeXi = self.__Integral_NegativeXi_Index
        self.MaxwellianName = "Index"

    def SetMaxwellian_HalfDisk(self):
        self.ComputeFlux_PositiveXi = self.__Integral_PositiveXi_HalfDisk
        self.ComputeFlux_NegativeXi = self.__Integral_NegativeXi_HalfDisk
        self.MaxwellianName = "Half disk"

    def __UndefinedIntegral(self, cl, mesh, W):
        raise ValueError(
            "ExplicitKineticScheme requires a Maxwellian to be chosen first. "
            "Current choices include the index Maxwellian "
            "[SetMaxwellian_Index(self)], and the half disk Maxwellian "
            "[SetMaxwellian_HalfDisk(self)]."
        )

    def __Integral_PositiveXi_Index(self, cl, mesh, W):
        iv = mesh.GetInteriorVerticesRange()
        nVertices = len(iv)
        N = cl.GetSystemSize()
        
        g = cl.GetGravityConstant()
        h = W[:,0]
        q = W[:,1]
        v = cl.ComputeVelocity(W)

        K = np.sqrt(h/6./g)
        C3 = np.sqrt(1.5*g*h)
        a = np.maximum(0., v - C3)
        b = np.maximum(0., v + C3)
        a2 = a*a
        b2 = b*b

        leftComingFlux = np.empty((nVertices, N))
        leftComingFlux[:,0] = K*.5*(b2 - a2)
        leftComingFlux[:,1] = K*(b2*b - a2*a)/3.

        return leftComingFlux

    def __Integral_NegativeXi_Index(self, cl, mesh, W):
        iv = mesh.GetInteriorVerticesRange()
        nVertices = len(iv)
        N = cl.GetSystemSize()
        
        g = cl.GetGravityConstant()
        h = W[:,0]
        q = W[:,1]
        v = cl.ComputeVelocity(W)

        K = np.sqrt(h/6./g)
        C3 = np.sqrt(1.5*g*h)
        a = np.minimum(0., v - C3)
        b = np.minimum(0., v + C3)
        a2 = a*a
        b2 = b*b

        rightComingFlux = np.empty((nVertices, N))
        rightComingFlux[:,0] = K*.5*(b2 - a2)
        rightComingFlux[:,1] = K*(b2*b - a2*a)/3.

        return rightComingFlux

    def __Integral_PositiveXi_HalfDisk(self, cl, mesh, W):
        iv = mesh.GetInteriorVerticesRange()
        nVertices = len(iv)
        N = cl.GetSystemSize()
        
        g = cl.GetGravityConstant()
        h = W[:,0]
        q = W[:,1]
        v = cl.ComputeVelocity(W)

        leftComingFlux = np.empty((nVertices, N))

        C = np.sqrt(2*g*h)
        # dryArea = (C <= np.spacing(1))
        zeroComponents = np.maximum((v + C <= 0), (np.abs(C) < np.spacing(1)))
        K = 2*h/np.pi
        a = np.arcsin(np.maximum(-v/(C+zeroComponents), -1)*(1 - zeroComponents))
        b = np.arcsin(np.maximum(-v/(C+zeroComponents), 1)*(1 - zeroComponents))
        H = b - a + .5*np.sin(2*b) - .5*np.sin(2*a)

        leftComingFlux[:,0] = .5*K*v*H - K*C*((np.cos(b)**3) - (np.cos(a)**3))/3.

        leftComingFlux[:,1] = K*(g*h + .5*v**2)*H - \
            .125*K*C*C*(3*b + .25*np.sin(4*b) + 2*np.sin(2*b) - 3*a - \
                        .25*np.sin(4*a) - 2*np.sin(2*a)) - \
                        2*v*K*C*((np.cos(b)**3) - (np.cos(a)**3))/3.

        leftComingFlux = leftComingFlux*(1 - zeroComponents[:,np.newaxis])

        return leftComingFlux

    def __Integral_NegativeXi_HalfDisk(self, cl, mesh, W):
        iv = mesh.GetInteriorVerticesRange()
        nVertices = len(iv)
        N = cl.GetSystemSize()
        
        g = cl.GetGravityConstant()
        h = W[:,0]
        q = W[:,1]
        v = cl.ComputeVelocity(W)

        rightComingFlux = np.empty((nVertices, N))

        C = np.sqrt(2*g*h)
        zeroComponents = np.maximum((v - C >= 0), (np.abs(C) < np.spacing(1)))
        K = 2*h/np.pi
        a = np.arcsin(np.minimum(-v/(C+zeroComponents), -1)*(1 - zeroComponents))
        b = np.arcsin(np.minimum(-v/(C+zeroComponents), 1)*(1 - zeroComponents))
        H = b - a + .5*np.sin(2*b) - .5*np.sin(2*a)
        
        rightComingFlux[:,0] = \
            .5*K*v*H - K*C*((np.cos(b)**3) - (np.cos(a)**3))/3.
        
        rightComingFlux[:,1] = \
            K*(g*h + .5*v**2)*H - \
            .125*K*C*C*(3*b + .25*np.sin(4*b) + 2*np.sin(2*b) - 3*a - .25*np.sin(4*a) - 2*np.sin(2*a)) - \
            2*v*K*C*((np.cos(b)**3) - (np.cos(a)**3))/3.
        
        rightComingFlux = rightComingFlux*(1 - zeroComponents[:,np.newaxis])

        return rightComingFlux

    def ComputeCellVariation(self, cl, mesh, W, time):
        iv = mesh.GetInteriorVerticesRange()
        nVertices = len(iv)
        N = cl.GetSystemSize()
        
        z = mesh.GetBathymetryArray()
        zL = z[iv-1]
        zR = z[iv]

        WL_, WR_, CorrectionL_, CorrectionR_, zL_, zR_ = \
            self.ApplyReconstructionOperator(cl, mesh, W, time)

        #Compute left coming flux
        leftComingFlux = self.ComputeFlux_PositiveXi(cl, mesh, WL_)
        rightComingFlux = self.ComputeFlux_NegativeXi(cl, mesh, WR_)
        
        F_kinetic = leftComingFlux + rightComingFlux

        return F_kinetic, CorrectionL_, CorrectionR_
