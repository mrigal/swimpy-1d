import numpy as np
from .SchemeClass import *

class HLL_hConst(SchemeClass):

    def __init__(self, ti=None):
        super(HLL_hConst, self).__init__(ti=ti)
        self.SchemeName = "HLL_hConst"

    def ComputeCellVariation(self, cl, mesh, W, time):
        iv = mesh.GetInteriorVerticesRange()
        
        z = mesh.GetBathymetryArray()
        zL = z[iv-1]
        zR = z[iv]

        WL_, WR_, CorrectionL_, CorrectionR_, zL_, zR_ = \
            self.ApplyReconstructionOperator(cl, mesh, W, time)

        FL = cl.ComputeFlux(WL_, zL_)
        FR = cl.ComputeFlux(WR_, zR_)

        lambdaL_, lambdaR_ = self.EstimateWaveVelocities__(cl, mesh, WL_, WR_)
        lambdaL = lambdaL_[:,np.newaxis]
        lambdaR = lambdaR_[:,np.newaxis]

        rightGoingWaves = (lambdaL >= 0)
        leftGoingWaves  = (lambdaR < 0)
        sameSpeed = (np.abs(lambdaL - lambdaR) <= np.spacing(1))

        # F_HLL_hConst = np.zeros(FL.shape)

        F_HLL_hConst = \
            (lambdaR*FL - lambdaL*FR + lambdaL*lambdaR*(WR_ - WL_)) \
            /(lambdaR - lambdaL + sameSpeed)*(1-rightGoingWaves-leftGoingWaves) \
            + FR*leftGoingWaves + FL*rightGoingWaves

        F_HLL_hConst[:,0] = 0
        
        return F_HLL_hConst, CorrectionL_, CorrectionR_

