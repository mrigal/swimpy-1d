import numpy as np
from .SchemeClass import *

class RoeScheme(SchemeClass):

    def __init__(self, ti=None):
        super(RoeScheme, self).__init__(ti=ti)
        self.SchemeName = "Roe"

    def ComputeCellVariation(self, cl, mesh, W, time):
        iv = mesh.GetInteriorVerticesRange()
        nVertices = len(iv)
        N = cl.GetSystemSize()
        
        z = mesh.GetBathymetryArray()
        zL = z[iv-1]
        zR = z[iv]

        WL_, WR_, CorrectionL_, CorrectionR_, zL_, zR_ = \
            self.ApplyReconstructionOperator(cl, mesh, W, time)

        FL = cl.ComputeFlux(WL_, zL_)
        FR = cl.ComputeFlux(WR_, zR_)

        #Collect quantities of interest
        g = cl.GetGravityConstant()
        hL = WL_[:,0]
        hR = WR_[:,0]
        qL = WL_[:,1]
        qR = WR_[:,1]
        vL = cl.ComputeVelocity(WL_)
        vR = cl.ComputeVelocity(WR_)

        hRoe = np.sqrt(hL*hR)
        #TODO: account for wet areas

        dryArea = (np.spacing(1) >= hL + hR)
        vRoe = (np.sqrt(hL)*vL + np.sqrt(hR)*vR) \
            /(np.sqrt(hL) + np.sqrt(hR) + dryArea)*(1 - dryArea)

        cRoe = np.sqrt(g*hRoe)
        dryArea = (np.spacing(1) >= cRoe)
        
        #Eigenvalues
        E1 = vRoe - cRoe
        E2 = vRoe + cRoe

        E1_abs = np.abs(E1)
        E2_abs = np.abs(E2)

        #Assembling Roe matrix
        A11 = (E1_abs*E2 - E2_abs*E1)*.5/(cRoe + dryArea)*(1 - dryArea)
        A12 = (E2_abs - E1_abs)*.5/(cRoe + dryArea)*(1 - dryArea)
        A21 = E1*E2*(E1_abs - E2_abs)*.5/(cRoe + dryArea)*(1 - dryArea)
        A22 = (E2_abs*E2 - E1_abs*E1)*.5/(cRoe + dryArea)*(1 - dryArea)

        F_Roe = np.empty((nVertices, N))
        F_Roe[:,0] = .5*(FL[:,0] + FR[:,0] - A11*(hR - hL) - A12*(qR - qL))
        F_Roe[:,1] = .5*(FL[:,1] + FR[:,1] - A21*(hR - hL) - A22*(qR - qL))
        
        return F_Roe, CorrectionL_, CorrectionR_

