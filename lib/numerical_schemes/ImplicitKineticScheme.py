import numpy as np
from .ImplicitSchemeClass import ImplicitSchemeClass
from ..time_integrators import TimeIntegratorClass


class ImplicitKineticScheme(ImplicitSchemeClass):

    def __init__(self, ti=None):
        #No generic time integrator should be used with this scheme
        super(ImplicitKineticScheme, self).__init__(ti=ti)
        self.SchemeName = "Implicit kinetic scheme O1"

        if ti is not None:
            self.SetTimeIntegrator(ti)


    def __str__(self):
        string = super(ImplicitKineticScheme, self).__str__()
        string += \
            "\n    Maxwellian: {}".format("index function")
        return string


    def SetTimeIntegrator(self, timeIntegrator):
        self.ti = timeIntegrator #Should make use of the CustomTimeIntegrator
        self.ti.SetCustomUpdate(self.KineticUpdate) #Set custom update routine

    
    def EstimateWaveVelocities(self, cl, mesh, data, time):

        WL = data[:-1,:]
        WR = data[1:,:]

        if self.ShouldReseolveGravityTimescale() is True:
            #Use eigenvalues to estimate timestep (as in explicit schemes)
            # cellCenters = mesh.GetCellCentersArray()
            # xL = cellCenters[:-1]
            # xR = cellCenters[1:]

            # minL, maxL = cl.ComputeOuterEigenvalues(WL, xL)
            # minR, maxR = cl.ComputeOuterEigenvalues(WR, xR)

            z = mesh.GetBathymetryArray()
            zL = z[:-1]
            zR = z[1:]
            
            minL, maxL = cl.ComputeOuterEigenvalues(WL, zL)
            minR, maxR = cl.ComputeOuterEigenvalues(WR, zR)
            
            lambdaL = np.minimum(minL, minR)
            lambdaR = np.maximum(maxL, maxR)
            
        else:
            #Only use material particles velocity to estimate timestep
            vL = cl.ComputeVelocity(WL)
            vR = cl.ComputeVelocity(WR)

            #If no material convection, use water height to determine timestep
            if np.all(np.abs(vL*vR) <= np.spacing(1.)):
                hL = WL[:,0]
                hR = WR[:,0]
                lambdaL = np.sqrt(hL)
                lambdaR = np.sqrt(hR)
            else:
                lambdaL = np.abs(vL)
                lambdaR = np.abs(vR)

        average = .5*(lambdaL + lambdaR)
        var = .5*(lambdaR - lambdaL)

        lambdaL = average - self.NumericalViscosity*var
        lambdaR = average + self.NumericalViscosity*var

        return lambdaL, lambdaR


    def KineticUpdate(timeIntegrator, scheme, cl, mesh, time, dt):
        """
        Return the full implicit-kinetic update.
        """

        data = mesh.GetCellData()

        #If time step is zero, nothing to do
        if dt <= np.spacing(1):
            return data

        U_interior = scheme.ComputeInteriorContribution(data, cl, mesh, time, dt)

        U_boundary = scheme.ComputeBoundaryConditions(data, cl, mesh, time, dt)

        return U_interior + U_boundary


    def ComputeInteriorContribution(self, data, cl, mesh, time, dt):
        """
        Return the array nextData storing the implicit-kinetic update of the
        macroscopic cell values without accounting for ghost cells.
        """

        nextData = np.empty(data.shape)

        g = cl.GetGravityConstant()
        N = mesh.GetNumberOfCells(ghosts=False)
        ic = mesh.GetInteriorCellsRange()
        dx = mesh.GetDx()

        W = data[ic,:]
        H = W[:,0]
        U = cl.ComputeVelocity(W)
        C = np.sqrt(.5*g*H)

        sqrt3 = np.sqrt(3.)

        #### Vectorized water height update

        sigma = dt/dx[ic]
        coeff = .5/sqrt3/sigma
        X = coeff*np.sqrt(2*H/g)

        a = U - sqrt3*C
        b = U + sqrt3*C
        alpha = -np.minimum(0, a)*sigma
        beta  = -np.minimum(0, b)*sigma
        Ah = self._AssembleMatrixAh(alpha) - self._AssembleMatrixAh(beta)

        # nextData[ic,0] = Ah @ X
        nextData[ic,0] = np.matmul(Ah, X)

        delta = np.maximum(0, a)*sigma
        gamma  = np.maximum(0, b)*sigma
        Bh = self._AssembleMatrixBh(gamma) - self._AssembleMatrixBh(delta)

        # nextData[ic,0] += Bh @ X
        nextData[ic,0] += np.matmul(Bh, X)

        #### Old version, not vectorized

        # for j in range(1, N+1):
        #     sigma_j = dt/dx[ic[j-1]]
        #     coeff = .5/(sqrt3*sigma_j)
        #     aj = U[j-1] - sqrt3*C[j-1]
        #     bj = U[j-1] + sqrt3*C[j-1]
            
            # alpha = -np.minimum(0, aj)*sigma_j
            # beta  = -np.minimum(0, bj)*sigma_j
            # y1 = beta/(1+beta)
            # y2 = alpha/(1+alpha)
            # Ah = np.log(np.abs(1+alpha)) - np.log(np.abs(1+beta))
            # nextData[ic[j-1],0] += coeff*Ah*np.sqrt(2*H[j-1]/g)
            # for i in range(j-1, 0, -1):
            #     Ah -= (y2**(j-i) - y1**(j-i))/(j-i)
            #     nextData[ic[i-1],0] += coeff*Ah*np.sqrt(2*H[j-1]/g)
                
            # alpha = np.maximum(0, aj)*sigma_j
            # beta  = np.maximum(0, bj)*sigma_j
            # y1 = alpha/(1+alpha)
            # y2 = beta/(1+beta)
            # Bh = np.log(np.abs(1+beta)) - np.log(np.abs(1+alpha))
            # nextData[ic[j-1],0] += coeff*Bh*np.sqrt(2*H[j-1]/g)
            # for i in range(j+1, N+1):
            #     Bh -= (y2**(i-j) - y1**(i-j))/(i-j)
            #     nextData[ic[i-1],0] += coeff*Bh*np.sqrt(2*H[j-1]/g)

        is_positive_water_height = np.all(nextData[ic,0] >= 0.)
        assert(is_positive_water_height)

        #### Vectorized discharge update

        sigma = dt/dx[ic]
        coeff = .5/(sqrt3*sigma*sigma)
        X = coeff*np.sqrt(2*H/g)

        a = U - sqrt3*C
        b = U + sqrt3*C
        alpha = -np.minimum(0, a)*sigma
        beta  = -np.minimum(0, b)*sigma
        Ahu = self._AssembleMatrixAhu(alpha) - self._AssembleMatrixAhu(beta)
        nextData[ic,1] = -np.matmul(Ahu, X)

        delta = np.maximum(0, a)*sigma
        gamma = np.maximum(0, b)*sigma
        Bhu = self._AssembleMatrixBhu(gamma) - self._AssembleMatrixBhu(delta)
        nextData[ic,1] += np.matmul(Bhu, X)

        #### Old version, not vectorized
        
        # for j in range(1, N+1):

        #     sigma_j = dt/dx[ic[j-1]]
        #     coeff = .5/(sqrt3*sigma_j*sigma_j)
        #     aj = U[j-1] - sqrt3*C[j-1]
        #     bj = U[j-1] + sqrt3*C[j-1]
            
        #     alpha = -np.minimum(0, aj)*sigma_j
        #     beta  = -np.minimum(0, bj)*sigma_j
        #     y1 = beta/(1+beta)
        #     y2 = alpha/(1+alpha)
        #     UA = 0.
        #     VA = 0.
        #     Ahu = alpha - np.log(np.abs(1+alpha)) - (beta - np.log(np.abs(1+beta)))
        #     nextData[ic[j-1],1] -= coeff*Ahu*np.sqrt(2*H[j-1]/g)
        #     for i in range(j-1, 0, -1):
        #         l_ji = j-i+1
        #         VA = VA - (y2**(j-i) - y1**(j-i))
        #         UA = UA*l_ji/(j-i) + (y2**(j-i) - y1**(j-i))*l_ji/(j-i)
        #         Ahu = UA + VA - \
        #             l_ji*(np.log(np.abs(1+alpha)) - np.log(np.abs(1+beta))) \
        #             + alpha - beta
        #         nextData[ic[i-1],1] -= coeff*Ahu*np.sqrt(2*H[j-1]/g)

            # alpha = np.maximum(0, aj)*sigma_j
            # beta  = np.maximum(0, bj)*sigma_j
            # y1 = alpha/(1+alpha)
            # y2 = beta/(1+beta)
            # UB = 0.
            # VB = 0.
            # Bhu = beta - np.log(np.abs(1+beta)) - (alpha - np.log(np.abs(1+alpha)))
            # nextData[ic[j-1],1] += coeff*Bhu*np.sqrt(2*H[j-1]/g)
            # for i in range(j+1, N+1):
            #     l_ij = i-j+1
            #     VB = VB - (y2**(i-j) - y1**(i-j))
            #     UB = UB*l_ij/(i-j) + (y2**(i-j) - y1**(i-j))*l_ij/(i-j)
            #     Bhu = UB + VB - \
            #         l_ij*(np.log(np.abs(1+beta)) - np.log(np.abs(1+alpha))) \
            #         + beta - alpha
            #     nextData[ic[i-1],1] += coeff*Bhu*np.sqrt(2*H[j-1]/g)

        return nextData


    def ComputeBoundaryConditions(self, data, cl, mesh, time, dt):
        """
        Return the array nextData storing the implicit-kinetic update of the
        macroscopic cell values only accounting for ghost cells influence.
        """

        nextData = np.empty(data.shape)
        
        g = cl.GetGravityConstant()

        N = mesh.GetNumberOfCells(ghosts=False)
        ic = mesh.GetInteriorCellsRange()
        dx = mesh.GetDx()

        #TODO: add possibility to enforce discharge instead of water height
        #TODO: add possibility to apply periodic boundary conditions?
        ########################################################################
        WL_ghost = data[ic[0]-1,:]
        hL_ghost = WL_ghost[0]
        qL_ghost = WL_ghost[1]
        uL_ghost = cl.ComputeVelocity(WL_ghost[np.newaxis,:])

        WR_ghost = data[ic[-1]+1,:]
        hR_ghost = WR_ghost[0]
        qR_ghost = WR_ghost[1]
        uR_ghost = cl.ComputeVelocity(WR_ghost[np.newaxis,:])

        #Compute integral bounds
        sqrt3 = np.sqrt(3)

        cL = np.sqrt(.5*g*hL_ghost)
        aL = uL_ghost - sqrt3*cL
        bL = uL_ghost + sqrt3*cL

        cR = np.sqrt(.5*g*hR_ghost)
        aR = uR_ghost - sqrt3*cR
        bR = uR_ghost + sqrt3*cR
        ########################################################################
        # domainBorder = np.array(mesh.GetDomainBounds())
        # xL = domainBorder[0]
        # xR = domainBorder[1]

        # #Left boundary
        # WL = data[ic[0],:]
        # hL = WL[0]
        # uL = cl.ComputeVelocity(WL[np.newaxis,:])
        # hL_ghost = self.BoundaryConditionValue(xL, time)
        # uL_ghost = uL - 2*(np.sqrt(g*hL) - np.sqrt(g*hL_ghost))
        # qL_ghost = hL_ghost*uL_ghost[0]

        # #Right boundary
        # WR = data[ic[-1],:]
        # hR = WR[0]
        # uR = cl.ComputeVelocity(WR[np.newaxis,:])
        # hR_ghost = self.BoundaryConditionValue(xR, time)
        # uR_ghost = uR + 2*(np.sqrt(g*hR) - np.sqrt(g*hR_ghost))
        # qR_ghost = hR_ghost*uR_ghost[0]

        # #Compute integral bounds
        # sqrt3 = np.sqrt(3)

        # cL = np.sqrt(.5*g*hL_ghost)
        # aL = uL_ghost - sqrt3*cL
        # bL = uL_ghost + sqrt3*cL

        # cR = np.sqrt(.5*g*hR_ghost)
        # aR = uR_ghost - sqrt3*cR
        # bR = uR_ghost + sqrt3*cR
        ########################################################################

        sigma = dt/dx[ic]
        delta =  max(0, aL)*sigma
        gamma =  max(0, bL)*sigma
        alpha = -min(0, aR)*sigma
        beta  = -min(0, bR)*sigma

        coeff = .5/sqrt3/sigma
        nextData[ic,0] = coeff*np.sqrt(2*hL_ghost/g) * \
            (self._ComputeGhostHL(gamma) - self._ComputeGhostHL(delta))
        nextData[ic,0] += coeff*np.sqrt(2*hR_ghost/g) * \
            (self._ComputeGhostHR(alpha) - self._ComputeGhostHR(beta))

        coeff = coeff/sigma
        nextData[ic,1] = coeff * np.sqrt(2*hL_ghost/g) * \
            (self._ComputeGhostQL(gamma) - self._ComputeGhostQL(delta))
        nextData[ic,1] += coeff * np.sqrt(2*hR_ghost/g) * \
            (self._ComputeGhostQR(beta) - self._ComputeGhostQR(alpha))

        # #Water height boundary conditions
        # for i in range(1, N+1):
        #     sigma_i = dt/dx[ic[i-1]]
        #     coeff = .5/sqrt3/sigma_i
        #     alpha = -min(0, aR)*sigma_i
        #     beta  = -min(0, bR)*sigma_i
        #     delta =  max(0, aL)*sigma_i
        #     gamma =  max(0, bL)*sigma_i
        #     k = N - i + 1
        #     P = self._ComputeI2(beta, k) - self._ComputeI2(alpha, k)
            # k = i
            # Q = self._ComputeI2(gamma, k) - self._ComputeI2(delta, k)
            # nextData[ic[i-1],0] = \
            #     coeff*(np.sqrt(2.*hL_ghost/g)*Q - np.sqrt(2.*hR_ghost/g)*P)

            # nextData[ic[i-1],0] -= \
            #     coeff*np.sqrt(2.*hR_ghost/g)*P

        # #Discharge boundary conditions
        # for i in range(1, N+1):
        #     sigma_i = dt/dx[ic[i-1]]
        #     coeff = .5/sqrt3/sigma_i/sigma_i
        #     alpha = -min(0, aR)*sigma_i
        #     beta  = -min(0, bR)*sigma_i
        #     delta =  max(0, aL)*sigma_i
        #     gamma =  max(0, bL)*sigma_i
        #     k = N - i + 1
        #     P = self._ComputeI3(beta, k) - self._ComputeI3(alpha, k)
        #     k = i
        #     Q = self._ComputeI3(gamma, k) - self._ComputeI3(delta, k)
            # nextData[ic[i-1],1] = \
            #     coeff*(np.sqrt(2*hR_ghost/g)*P + np.sqrt(2*hL_ghost/g)*Q)
            # nextData[ic[i-1],1] += \
            #     coeff*np.sqrt(2*hR_ghost/g)*P
            # nextData[ic[i-1],1] += \
            #     coeff*np.sqrt(2*hL_ghost/g)*Q

        return nextData

    def _AssembleMatrixAh(self, X):
        N = X.shape[0]
        Y = X/(1+X)

        Ah = np.zeros((N, N))
        I, J = np.triu_indices(N, 0)
        Ah[I,J] = np.log(np.abs(1 + X))[J]

        S = np.zeros((N, N))
        I, J = np.triu_indices(N, 1)
        S[I,J] = (Y[J]**(J-I))/(J-I)
        Ah[I,J] -= np.cumsum(S[::-1,:], axis=0)[N-I-1,J]

        return Ah

    # def _AssembleMatrixAh(self, X):
    #     N = X.shape[0]
    #     P = np.arange(1, N+1, dtype=int)[:,np.newaxis]
    #     X = X[np.newaxis,:]
    #     Y = X/(1+X)
    #     Z = np.log(np.abs(1 + X)) - np.cumsum((Y**P)/P, axis=0)[::-1,:]
        
    #     I, J = np.triu_indices(N, 1)

    #     Ah = np.zeros((N, N))
    #     Ah[(I, J)] = Z[(N-J+I, J)]
    #     diagId = np.arange(0, N)
    #     Ah[(diagId, diagId)] = np.log(np.abs(1 + X))

    #     return Ah

    def _AssembleMatrixBh(self, X):
        N = X.shape[0]
        Y = X/(1+X)

        Bh = np.zeros((N, N))
        I, J = np.tril_indices(N, 0)
        Bh[I,J] = np.log(np.abs(1 + X))[J]

        S = np.zeros((N, N))
        I, J = np.tril_indices(N, -1)
        S[I,J] = (Y[J]**(I-J))/(I-J)
        Bh[I,J] -= np.cumsum(S, axis=0)[I,J]

        return Bh

    # def _AssembleMatrixBh(self, X):
    #     N = X.shape[0]
    #     P = np.arange(1, N+1, dtype=int)[:,np.newaxis]
    #     X = X[np.newaxis,:]
    #     Y = X/(1+X)
    #     Z = np.log(np.abs(1 + X)) - np.cumsum((Y**P)/P, axis=0)
        
    #     I, J = np.tril_indices(N, -1)

    #     Bh = np.zeros((N, N))
    #     Bh[(I, J)] = Z[(I-J-1, J)]
    #     diagId = np.arange(0, N)
    #     Bh[(diagId, diagId)] = np.log(np.abs(1 + X))

    #     return Bh

    def _AssembleMatrixAhu(self, X):
        N = X.shape[0]
        Y = X/(1+X)
        
        I, J = np.triu_indices(N, 0)
        K1 = np.zeros((N, N))
        K1[I, J] = np.log(np.abs(1+X))[J]
        K1[I, J] *= I-J-1
        K1[I, J] += X[J]

        I, J = np.triu_indices(N, 1)
        K2 = np.zeros((N, N))
        K2[I,J] = Y[J]**(J-I)
        K2 = np.cumsum(K2[::-1,:], axis=0)[::-1,:]
        
        K3 = np.zeros((N, N))
        K3[I,J] = (Y[J]**(J-I))/(J-I)
        K3 = np.cumsum(K3[::-1,:], axis=0)[::-1,:]
        K3[I,J] *= J-I+1
        
        Ahu = K1 - K2 + K3
        return Ahu

    def _AssembleMatrixBhu(self, X):
        N = X.shape[0]
        Y = X/(1+X)
        
        I, J = np.tril_indices(N, 0)
        K1 = np.zeros((N, N))
        K1[I, J] = (J-I-1)*np.log(np.abs(1+X))[J] + X[J]
        
        I, J = np.tril_indices(N, -1)
        K2 = np.zeros((N, N))
        K2[I,J] = Y[J]**(I-J)
        K2 = np.cumsum(K2, axis=0)

        K3 = np.zeros((N, N))
        K3[I,J] = (Y[J]**(I-J))/(I-J)
        K3 = np.cumsum(K3, axis=0)
        K3[I,J] = (I-J+1)*K3[I,J]
        
        Bhu = K1 - K2 + K3
        return Bhu

    def _ComputeGhostHL(self, X):
        N = X.shape[0]
        Y = X/(1+X)

        K = np.arange(1, N+1)

        U = X - K*np.log(np.abs(1+X))

        S = np.zeros((N, N))
        I, J = np.tril_indices(N, -1)
        S[I,J] = (J + 1)*(Y[I]**(I - J))/(I - J)
        
        U += np.sum(S, axis=1)

        return U

    def _ComputeGhostHR(self, X):
        N = X.shape[0]
        Y = X/(1+X)

        K = np.arange(N, 0, -1)

        U = X - K*np.log(np.abs(1+X))

        S = np.zeros((N, N))
        I, J = np.triu_indices(N, 1)
        S[I,J] = (N - J)*(Y[I]**(J - I))/(J - I)
        
        U += np.sum(S, axis=1)

        return U

    def _ComputeGhostQL(self, X):
        N = X.shape[0]
        Y = X/(1+X)

        K = np.arange(1, N+1)

        U = .5*(1+X)*(1+X) - (K+1)*(1+X) + K*np.log(np.abs(1+X)) \
            + .5*K*(K-1)*np.log(np.abs(1+X))

        S = np.zeros((N, N))
        I, J = np.tril_indices(N, -2)
        S[I,J] = (I-J-1)*.5*(I-J)*(Y[I]**(J+1))/(J+1)
        
        U -= np.sum(S, axis=1)

        I, J = np.tril_indices(N, -1)
        S[I,J] = (I-J)*(Y[I]**(J+1))/(J+1)

        U -= np.sum(S, axis=1)

        return U

    def _ComputeGhostQR(self, X):
        N = X.shape[0]
        Y = X/(1+X)

        K = np.arange(N, 0, -1)

        U = .5*(1+X)*(1+X) - (K+1)*(1+X) + K*np.log(np.abs(1+X)) \
            + .5*K*(K-1)*np.log(np.abs(1+X))

        S = np.zeros((N, N))
        I, J = np.triu_indices(N, 2)
        S[I,J] = (J-I-1)*.5*(J-I)*(Y[N-J-1]**(N-J))/(N-J)

        U -= np.sum(S, axis=1)

        I, J = np.triu_indices(N, 1)
        S[I,J] = (J-I)*(Y[N-J-1]**(N-J))/(N-J)

        U -= np.sum(S, axis=1)

        return U

    # def _ComputeI1(self, x, k):
    #     y = x/(1+x)
    #     res = np.log(np.abs(1 + x))
    #     for l in range(1, k+1):
    #         res -= (y**l)/l
    #     return res


    # def _ComputeI2(self, x, k):
    #     y = x/(1+x)
    #     res = -k*np.log(np.abs(1 + x)) + x
    #     for l in range(1, k):
    #         res += l*(y**(k-l))/(k-l)
    #     return res


    # def _ComputeI3(self, x, k):
    #     y = x/(1+x)
    #     res = -(k+1)/(1-y) + .5/(1-y)/(1-y) - k*np.log(np.abs(1-y))
    #     if 2 <= k:
    #         res -= .5*k*(k-1)*np.log(np.abs(1-y))
    #         for q in range(1, k):
    #             res -= (k-q)*(y**q)/q
    #     if 3 <= k:
    #         for r in range(1, k-1):
    #             res -= .5*(k-r-1)*(k-r)*(y**r)/r
    #     return res
