import numpy as np
from .ExplicitKineticScheme import ExplicitKineticScheme
from .ImplicitSchemeClass import ImplicitSchemeClass

class IterativeKineticScheme(ExplicitKineticScheme, ImplicitSchemeClass):

    def __init__(self, ti=None):
        super(ExplicitKineticScheme, self).__init__(ti=ti)
        self.SchemeName = "Iterative kinetic scheme"
        self.alpha = 1 #Relaxation parameter
        self.eps = 10**(-9)
        self.maxIter = 10000
        self.TOLERANCE             = 0
        self.ENTROPY               = 1
        self.TOLERANCE_AND_ENTROPY = 2
        self.STOPPING_CRITERIA = self.TOLERANCE_AND_ENTROPY
        self.NextIter__ = self.NextIter__ToleranceAndEntropy
        # self.ComputeFlux_PositiveXi = self.__UndefinedIntegral
        # self.ComputeFlux_NegativeXi = self.__UndefinedIntegral
        # self.MaxwellianName = None

    def __str__(self):
        string = super(IterativeKineticScheme, self).__str__()
        # string += \
        #     "\n    Maxwellian: {}".format(self.MaxwellianName)
        string += \
            "\n    Tolerance: {}\n    MaxIter: {}".format(self.eps, self.maxIter)
        return string

    def SetTimeIntegrator(self, timeIntegrator):
        self.ti = timeIntegrator #Should make use of the CustomTimeIntegrator
        self.ti.SetCustomUpdate(self.IterativeUpdate) #Set custom update routine

    def SetStoppingCriteria(self, criteria):
        if self.TOLERANCE == criteria:
            self.STOPPING_CRITERIA = self.TOLERANCE
            self.NextIter__ = self.NextIter__Tolerance
        elif self.ENTROPY == criteria:
            self.STOPPING_CRITERIA = self.ENTROPY
            self.NextIter__ = self.NextIter__Entropy
        elif self.TOLERANCE_AND_ENTROPY == criteria:
            self.STOPPING_CRITERIA = self.TOLERANCE_AND_ENTROPY
            self.NextIter__ = self.NextIter__ToleranceAndEntropy
        else:
            raise ValueError('Unknown stopping criteria.')

    def SetAlpha(self, alpha):
        self.alpha = alpha

    def GetAlpha(self):
        return self.alpha

    def SetTolerance(self, eps):
        self.eps = eps

    def GetTolerance(self):
        return self.eps

    def SetMaxIter(self, maxIter):
        self.maxIter = maxIter

    def GetMaxIter(self):
        return self.maxIter

    def IterativeUpdate(timeIntegrator, scheme, cl, mesh, time, dt):
        data = mesh.GetCellData()

        if abs(dt) < np.spacing(1):
            return data

        ic = mesh.GetInteriorCellsRange()

        dx_ = mesh.GetDx()
        dx = dx_[ic]

        # time = scheme.ti.GetCurrentTime()
        # print(timeIntegrator)
        # time = timeIntegrator.GetCurrentTime()

        nIter = 0
        data_k = np.copy(data)

        Etot_0 = scheme.ComputeTotalEnergy(data_k, cl, mesh)
        
        while True:

            data_k = scheme.ApplyBNDC(cl, mesh, data_k, time)

            numericalFlux, correctionL, correctionR = \
                scheme.ComputeCellVariation(cl, mesh, data_k, time)
            
            FL = numericalFlux[:-1,:]
            FR = numericalFlux[1:,:]

            alpha = scheme.GetAlpha()

            nextData = data[ic,:] + alpha*data_k[ic,:] - \
                dt/dx[:,np.newaxis]*(FR + correctionR[:-1,:] - FL - correctionL[1:,:])
            # + dt*source
            nextData = nextData/(1 + alpha)

            # nextData = .5*(data_k[ic,:] + data[ic,:]) - \
            #     .5*dt/dx[:,np.newaxis]*(FR + correctionR[:-1,:] - FL - correctionL[1:,:]) # + dt*source

            diff = nextData - data_k[ic,:]

            err = np.sqrt(np.sum(dx[:,np.newaxis]*diff*diff, axis=None))

            data_k[ic,:] = nextData
            
            # if scheme.GetTolerance() >= err:
            #     break

            Etot = scheme.ComputeTotalEnergy(data_k, cl, mesh)

            DeltaE = Etot - Etot_0

            if scheme.NextIter__(err, nIter, DeltaE):
                nIter += 1
                continue

            if scheme.GetTolerance() < err and scheme.STOPPING_CRITERIA != scheme.ENTROPY:
                print(
                    "Warning: maximum number of iterations reached without convergence."
                )

            if 0 < DeltaE and scheme.STOPPING_CRITERIA != scheme.TOLERANCE:
                print(
                    "Warning: maximum number of iterations reached with "
                    "total energy increase: DE = {}.".format(DeltaE)
                )

            break

        return data_k

    def NextIter__Tolerance(self, err, nIter, DeltaE):
        return (self.GetTolerance() < err and self.GetMaxIter() > nIter)

    def NextIter__Entropy(self, err, nIter, DeltaE):
        return (0 < DeltaE and self.GetMaxIter() > nIter)

    def NextIter__ToleranceAndEntropy(self, err, nIter, DeltaE):
        return ((0 < DeltaE or self.GetTolerance() < err) and self.GetMaxIter() > nIter)

    def ComputeTotalEnergy(self, W, cl, mesh):
        ic = mesh.GetInteriorCellsRange()
        E = self.ComputeEnergy(W, cl, mesh)
        dx = mesh.GetDx()[ic]
        return np.sum(dx*E)

    def ComputeEnergy(self, W, cl, mesh):
        ic = mesh.GetInteriorCellsRange()
        W_ = W[ic,:]
        h = W_[:,0]
        v = cl.ComputeVelocity(W_)
        v2 = v*v
        z = mesh.GetBathymetryArray()[ic]
        g = cl.GetGravityConstant()
        return .5*h*v2 + .5*g*h*h + g*h*(z - np.min(z))
