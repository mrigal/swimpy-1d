import numpy as np
from ..reconstruction_operators.NoReconstruction import *

class SchemeClass(object):

    DIRICHLET_CONDITION = 0
    NEUMANN_CONDITION = 1
    PERIODIC_CONDITION = 2

    #For weakly prescribed Riemann boundary condition
    ENFORCE_WATER_HEIGHT = 0
    ENFORCE_DISCHARGE = 1

    # PERIODIC_CONDITION__ = 0
    # MIXED_DIRICHLET_NEUMANN_CONDITION = 1
    # WEAKLY_PRESCRIBED_RIEMANN_CONDITION = 2

    def __init__(self, ti=None):
        self.BoundaryConditionType = None
        self.BoundaryConditionValue = None
        self.CFL_number = .5
        self.ReconstructionOperator = NoReconstruction()
        self.ReconstructionOperatorName = "None"
        self.Limiter = None
        self.LimiterName = None
        self.ti = ti #Time integrator
        self.NumericalViscosity = 1.125
        self.SchemeName = "None"
        self.BoundaryConditionsName = None

        # self.BoundaryConditionCategory = None
        # self.test = self.ApplyBNDC

        self.ApplyBNDC = self.ApplyPeriodicBNDC

    def __str__(self):
        string = \
            "SchemeClass instance:\n" \
            "    Name: {}\n" \
            "    Time integrator: {}\n" \
            "    CFL number: {}\n" \
            "    Boundary conditions: {}\n" \
            "    Reconstruction operator: {}\n" \
            "    Limiter: {}".format(
                self.SchemeName, self.ti.GetTimeIntegratorName(),
                self.CFL_number, self.BoundaryConditionsName,
                self.ReconstructionOperatorName, self.LimiterName
            )
        return string

    def SetPeriodicBNDC(self):
        self.ApplyBNDC = self.ApplyPeriodicBNDC
        self.BoundaryConditionsName = "periodic"

    def SetMixedBNDC(self, bndConditionType, bndConditionValue):
        self.BoundaryConditionType = bndConditionType
        self.BoundaryConditionValue = bndConditionValue
        self.ApplyBNDC = self.ApplyMixedBNDC
        self.BoundaryConditionsName = "mixed"

    def SetWeaklyPrescribedSubsonicRiemannBNDC_H(
            self, bndConditionType, bndConditionValue
    ):
        self.BoundaryConditionType = bndConditionType
        self.BoundaryConditionValue = bndConditionValue
        self.ApplyBNDC = \
            self.ApplyWeaklyPrescribedSubsonicRiemannBNDC_H
        self.BoundaryConditionsName = "subsonic weakly prescribed"

    def SetWeaklyPrescribedRiemannBNDC_H(self):
        raise NotImplementedError

    def SetTimeIntegrator(self, timeIntegrator):
        self.ti = timeIntegrator

    def SetCFLNumber(self, CFL):
        self.CFL_number = CFL

    def GetCFLNumer(self):
        return self.CFL_number

    def SetNumericalViscosity(self, numViscosity):
        self.NumericalViscosity = numViscosity

    def GetNumericalViscosity(self):
        return self.NumericalViscosity

    def SetBoudaryConditions(self, bndConditionType, bndConditionValue):
        self.BoundaryConditionType = bndConditionType
        self.BoundaryConditionValue = bndConditionValue

    def SetSchemeName(self, name):
        self.SchemeName = name

    def GetSchemeName(self):
        return self.SchemeName

    def Solve(self, cl, mesh, initialCondition, om, silent=True):
        #cl -> ConservationLaw
        #ti -> TimeIntegrator
        #om -> OutputManager
        self.ti.InitializeTimer() #Simulation timer
        om.ResetCPUTime() #Measure how much time the solver takes
        om.ResetOutputCounter()

        self.InitializeScheme(cl, mesh, initialCondition, om)

        if silent is False:
            ic = mesh.GetInteriorCellsRange()
            data = mesh.GetCellData()
            h0 = data[ic,0]
            totalH0 = np.sum(h0)

        while self.ti.EndingTimeHasBeenReached() is False:
            self.UpdateSolution(cl, mesh, om)

        if silent is False:
            data = mesh.GetCellData()
            h1 = data[ic,0]
            totalH1 = np.sum(h1)

            print("Total height variation: {}".format(totalH1-totalH0))

            print("CPU time: {}".format(om.GetCPUTime()))

    def SetReconstructionOperator(self, reconstructionOperator):
        self.ReconstructionOperator = reconstructionOperator
        self.ReconstructionOperatorName = reconstructionOperator.name

    def SetLimiter(self, limiter, name=None):
        self.Limiter = limiter
        self.LimiterName = name

    def GetLimiter(self):
        return self.Limiter

    def InitializeScheme(self, cl, mesh, initialCondition, om):
        N = cl.GetSystemSize()
        mesh.SetCellDataSize(N)
        mesh.AllocateCellData()

        vertices = mesh.GetVerticesArray()
        z_ = cl.ComputeBathymetryAtPoints(vertices)
        z = .5*(z_[:-1] + z_[1:])
        mesh.SetBathymetryArray(z)

        data = mesh.GetCellData()
        
        ic = mesh.GetInteriorCellsRange()

        i = ic[0]
        j = ic[-1] + 1

        for k in range(1,N): #Water height treated appart
            WL = initialCondition[k](vertices[i:j])
            WR = initialCondition[k](vertices[i+1:j+1])
            data[ic,k] = .5*(WL + WR)

        zetaL = initialCondition[0](vertices[i:j]) + z_[i:j]
        zetaR = initialCondition[0](vertices[i+1:j+1]) + z_[i+1:j+1]
        zeta = .5*(zetaL + zetaR)
        data[ic,0] = np.maximum(0., zeta - z[ic])
        # data[ic,0] = zeta - z[ic]

        dry = (data[ic,0] <= np.spacing(1.))
        data[ic[dry],:] = 0.

        mesh.SetCellData(data)

        #TODO: plot initial condition
        if om.NextOutputTimeHasBeenReached(self.ti.GetCurrentTime()):
            om.DoPlotRealTime(cl, data, mesh, self)
            om.DoDumpDataToFile(cl, data, mesh, self)
            om.IncreaseOutputCounter()

    def UpdateSolution(self, cl, mesh, om):
        om.StartMeasuringCPUTime()

        time = self.ti.GetCurrentTime()
        data = mesh.GetCellData()
        data = self.ApplyBNDC(cl, mesh, data, time)
        mesh.SetCellData(data)

        # WL = data[:-1,:]
        # WR = data[1:]

        #Compute eigenvalues to determine time step
        lambdaL, lambdaR = self.EstimateWaveVelocities(cl, mesh, data, time)
        lambdaMax = np.maximum(np.abs(lambdaL), np.abs(lambdaR))

        # print(max(lambdaMax))

        dx = mesh.GetDx()
        dt_ = self.ComputeTimeStep(dx, lambdaMax)

        om.StopMeasuringCPUTime()
        outputTimesArray = om.GetOutputTimesBetween(time, time + dt_)

        if outputTimesArray.shape[0] > 0:
            for t in np.nditer(outputTimesArray):
                dt = t - time
                nextData = self.ti.Update(self, cl, mesh, time, dt)
                om.DoPlotRealTime(cl, nextData, mesh, self)
                om.DoDumpDataToFile(cl, nextData, mesh, self)
                om.IncreaseOutputCounter()

        om.StartMeasuringCPUTime()
        
        nextData = self.ti.Update(self, cl, mesh, time, dt_)
        
        mesh.SetCellData(nextData, ghosts=True)
        self.ti.AdvanceCurrentTime(dt_)
    
        om.StopMeasuringCPUTime()

    def EstimateWaveVelocities(self, cl, mesh, data, time):
        # time = self.ti.GetCurrentTime()

        #TODO: should we reconstruct the bathymetry?

        if self.ReconstructionOperator is not None:
            WL_, WR_, CorrectionL, CorrectionR, _, _ = \
                self.ApplyReconstructionOperator(cl, mesh, data, time)
        else:
            iv = mesh.GetInteriorVerticesRange()
            WL_ = data[iv-1,:]
            WR_ = data[iv,:]

        return self.EstimateWaveVelocities__(cl, mesh, WL_, WR_)

    def EstimateWaveVelocities__(self, cl, mesh, WL_, WR_):
        iv = mesh.GetInteriorVerticesRange()

        z = mesh.GetBathymetryArray()
        zL = z[iv-1]
        zR = z[iv]

        # cellCenters = mesh.GetCellCentersArray()
        # xL = cellCenters[iv-1]
        # xR = cellCenters[iv]

        # minL, maxL = cl.ComputeOuterEigenvalues(WL_, xL)
        # minR, maxR = cl.ComputeOuterEigenvalues(WR_, xR)

        minL, maxL = cl.ComputeOuterEigenvalues(WL_, zL)
        minR, maxR = cl.ComputeOuterEigenvalues(WR_, zR)

        lambdaL = np.minimum(minL, minR)
        lambdaR = np.maximum(maxL, maxR)

        average = .5*(lambdaL + lambdaR)
        var = .5*(lambdaR - lambdaL)

        lambdaL = average - self.NumericalViscosity*var
        lambdaR = average + self.NumericalViscosity*var

        # if np.all(0 == lambdaL) and np.all(0 == lambdaR):
        #     minL, maxL = cl.ComputeOuterFastEigenvalues(WL_, zL)
        #     minR, maxR = cl.ComputeOuterFastEigenvalues(WR_, zR)

        #     lambdaL = np.minimum(minL, minR)
        #     lambdaR = np.maximum(maxL, maxR)

        #     average = .5*(lambdaL + lambdaR)
        #     var = .5*(lambdaR - lambdaL)

        #     lambdaL = average - self.NumericalViscosity*var
        #     lambdaR = average + self.NumericalViscosity*var

        return lambdaL, lambdaR

    def ComputeTimeStep(self, dx, maxWaveVelocity):
        zeros = np.abs(maxWaveVelocity) <= np.spacing(1)
        # dt = np.minimum(np.divide(dx/(maxWaveVelocity+zeros)))
        dt = self.CFL_number*np.amin(np.divide(dx, np.amax(maxWaveVelocity+zeros)))
        #TODO: change me to cell wise maxWave evaluation
        time = self.ti.GetCurrentTime()
        endingTime = self.ti.GetEndingTime()
        dt = min(dt, endingTime - time)
        return dt

    # def ApplyBNDC(self, cl, mesh, W):

    #     time = self.ti.GetCurrentTime()
        
    #     bounds = np.array(mesh.GetDomainBounds())
    #     bndCondType = self.BoundaryConditionType(bounds, time) #TODO: vectorize!
    #     bndCondVal = self.BoundaryConditionValue(bounds, time)

    #     N = cl.GetSystemSize()
    #     nCells = mesh.GetNumberOfCells(ghosts=False)
    #     # ic = mesh.GetInteriorCellsRange()
    #     # i = ic[0]
    #     # j = ic[-1]

    #     i, j = mesh.GetBorderCellsID()

    #     borderID = [i, j]
    #     ghostsID = [i-1, j+1]
    #     torusID = borderID[::-1]

    #     centerCellsArray = mesh.GetCellCentersArray()
    #     borderCenters = centerCellsArray[borderID]
    #     ghostCenters = centerCellsArray[ghostsID]
    #     distBorder = np.abs(borderCenters - bounds)
    #     distGhost = np.abs(ghostCenters - bounds)

    #     W_border = W[borderID,:]
    #     W_torus = W[torusID,:]
    #     W_ghosts = W[ghostsID,:]

    #     data = W[:,:]
    #     for k in range(N):
    #         data[ghostsID,k] = \
    #             (bndCondType[:,k] == self.PERIODIC_CONDITION)*W_torus[:,k] + \
    #             (bndCondType[:,k] == self.DIRICHLET_CONDITION)* \
    #             ((1+np.divide(distGhost, distBorder))*bndCondVal[:,k] - \
    #              np.divide(distGhost, distBorder)*W_border[:,k])
    #             # np.divide(distBorder*W_ghosts[k] + distGhost*W_border[k],
    #             #           distBorder + distGhost)

    #     #TODO: implement Neumann boundary conditions

    #     return data

    # def ApplyBNDC(self, cl, mesh, W):
    #     return self.ApplyMixedBNDC(cl, mesh, W)

    def ApplyPeriodicBNDC(self, cl, mesh, W, time):
        
        nCells = mesh.GetNumberOfCells(ghosts=True)
        i, j = mesh.GetBorderCellsID()
        leftGhosts = range(0, i)
        rightGhosts = range(j+1, nCells)

        data = W[:,:]

        for l in leftGhosts:
            data[l,:] = W[j-i+1+l,:]

        for l in rightGhosts:
            data[l,:] = W[i+l-j-1,:]

        return data

    def ApplyMixedBNDC(self, cl, mesh, W, time):

        bounds = np.array(mesh.GetDomainBounds())
        # bndCondType = self.BoundaryConditionType(bounds, time) #TODO: vectorize!
        # bndCondVal = self.BoundaryConditionValue(bounds, time)
        X = mesh.GetCellCentersArray()

        N = cl.GetSystemSize()
        nCells = mesh.GetNumberOfCells(ghosts=True)
        # ic = mesh.GetInteriorCellsRange()
        # i = ic[0]
        # j = ic[-1]

        i, j = mesh.GetBorderCellsID()
        leftGhosts = range(0, i)
        rightGhosts = range(j+1, nCells)

        data = W[:,:]

        #Dealing with left boundary
        bndCondType = self.BoundaryConditionType(bounds[0], time)
        bndCondVal = self.BoundaryConditionValue(bounds[0], time)
        for k in range(N):
            if bndCondType[k] == self.DIRICHLET_CONDITION:
                for l in leftGhosts:
                    x0 = X[l] - bounds[0]
                    x1 = X[i] - bounds[0]
                    x2 = X[i+1] - bounds[0]
                    alpha = x1*x2/(x2-x0)/(x1-x0)
                    beta = -x0*x2/(x1-x0)/(x2-x1)
                    gamma = x0*x1/(x2-x0)/(x2-x1)
                    data[l,k] = (bndCondVal[k] - beta*W[i,k] -
                                 gamma*W[i+1,k])/alpha
            elif bndCondType[k] == self.NEUMANN_CONDITION:
                for l in leftGhosts:
                    x0 = X[l] - bounds[0]
                    x1 = X[i] - bounds[0]
                    x2 = X[i+1] - bounds[0]
                    gamma = -(x0 + x1)/(x2 - x0)/(x2 - x1)
                    beta = (x0 + x2)/(x1 - x0)/(x2 - x1)
                    alpha = -(x1 + x2)/(x2 - x0)/(x1 - x0)
                    data[l,k] = (bndCondVal[k] - beta*W[i,k] -
                                 gamma*W[i+1,k])/alpha
            elif bndCondType[k] == self.PERIODIC_CONDITION:
                for l in leftGhosts:
                    data[l,k] = W[j-i+1+l,k]
            else:
                raise ValueError("Unknown left boundary condition type for "
                                 "component {}.".format(k))

        #Dealing with right boundary
        bndCondType = self.BoundaryConditionType(bounds[1], time)
        bndCondVal = self.BoundaryConditionValue(bounds[1], time)
        for k in range(N):
            if bndCondType[k] == self.DIRICHLET_CONDITION:
                for l in rightGhosts:
                    x0 = X[l] - bounds[1]
                    x1 = X[j] - bounds[1]
                    x2 = X[j-1] - bounds[1]
                    alpha = x1*x2/(x2-x0)/(x1-x0)
                    beta = -x0*x2/(x1-x0)/(x2-x1)
                    gamma = x0*x1/(x2-x0)/(x2-x1)
                    data[l,k] = (bndCondVal[k] - beta*W[j,k] -
                                 gamma*W[j-1,k])/alpha
            elif bndCondType[k] == self.NEUMANN_CONDITION:
                for l in rightGhosts:
                    x0 = X[l] - bounds[1]
                    x1 = X[j] - bounds[1]
                    x2 = X[j-1] - bounds[1]
                    gamma = -(x0 + x1)/(x2 - x0)/(x2 - x1)
                    beta = (x0 + x2)/(x1 - x0)/(x2 - x1)
                    alpha = -(x1 + x2)/(x2 - x0)/(x1 - x0)
                    data[l,k] = (bndCondVal[k] - beta*W[j,k] -
                                 gamma*W[j-1,k])/alpha
            elif bndCondType[k] == self.PERIODIC_CONDITION:
                for l in rightGhosts:
                    data[l,k] = W[i+l-j-1,k]
            else:
                raise ValueError("Unknown right boundary condition type for "
                                 "component {}.".format(k))

        return data

    def ApplyWeaklyPrescribedSubsonicRiemannBNDC_H(
            self, cl, mesh, W, time
    ):
        bounds = np.array(mesh.GetDomainBounds())
        # bndCondType = self.BoundaryConditionType(bounds, time) #TODO: vectorize!
        # bndCondVal = self.BoundaryConditionValue(bounds, time)
        X = mesh.GetCellCentersArray()

        N = cl.GetSystemSize()
        nCells = mesh.GetNumberOfCells(ghosts=True)
        # ic = mesh.GetInteriorCellsRange()
        # i = ic[0]
        # j = ic[-1]

        i, j = mesh.GetBorderCellsID()
        leftGhosts = range(0, i)
        rightGhosts = range(j+1, nCells)

        data = W[:,:]

        g = cl.GetGravityConstant()

        #EXPERIMENTAL, APPLIES ONLY TO THE 1D SAINT VENANT CLASS,
        #APPLIES THE WEAKLY PRESCRIBED SUBSONIC RIEMANN BOUDARY CONDITION WITH
        #ENFORCED WATER HEIGHT

        # alpha = 1
        # beta = 0
        # gamma = 0

        #Left boundary
        Wi = W[i,:]
        hi = Wi[0]
        ci = np.sqrt(g*hi)
        vi = cl.ComputeVelocity(Wi[np.newaxis,:])
        if (vi-ci)*(vi+ci)>0.:
            print("Warning: non subsonic flow at left boundary")
        
        #Enforcing left border water height
        hStar = self.BoundaryConditionValue(bounds[0], time) #How many quantities should this function return?
        for l in leftGhosts:
            # x0 = X[l] - bounds[0]
            # x1 = X[i] - bounds[0]
            # x2 = X[i+1] - bounds[0]
            # alpha = x1*x2/(x2-x0)/(x1-x0)
            # beta = -x0*x2/(x1-x0)/(x2-x1)
            # gamma = x0*x1/(x2-x0)/(x2-x1)
            # data[l,0] = (hStar - beta*W[i,0] -
            #              gamma*W[i+1,0])/alpha
            data[l,0] = hStar

        #Left border discharge given by the left going Riemann invariant (subsonic case)
        # n = -1 (outgoing normal)
        # v_n = vi * n
        #uStar_n = v_n + 2*(sqrt(ghi) - sqrt(ghStar))
        #uStar = uStar_n * n
        uStar = vi - 2*(np.sqrt(g*hi) - np.sqrt(g*hStar))
        qStar = hStar*uStar
        for l in leftGhosts:
            # x0 = X[l] - bounds[0]
            # x1 = X[i] - bounds[0]
            # x2 = X[i+1] - bounds[0]
            # alpha = x1*x2/(x2-x0)/(x1-x0)
            # beta = -x0*x2/(x1-x0)/(x2-x1)
            # gamma = x0*x1/(x2-x0)/(x2-x1)
            # data[l,1] = (qStar - beta*W[i,1] -
            #              gamma*W[i+1,1])/alpha
            data[l,1] = qStar

        # if (uStar-np.sqrt(g*hStar))*(uStar+np.sqrt(g*hStar))>0.:
        #     print("Warning: non subsonic flow at left ghost")

        #Right boundary
        Wj = W[j,:]
        hj = Wj[0]
        cj = np.sqrt(g*hj)
        vj = cl.ComputeVelocity(Wj[np.newaxis,:])
        if (vj-cj)*(vj+cj)>0.:
            print("Warning: non subsonic flow at right boundary")

        #Enforcing right border water height
        hStar = self.BoundaryConditionValue(bounds[1], time) #How many quantities should this function return?
        for l in rightGhosts:
            # x0 = X[l] - bounds[1]
            # x1 = X[j] - bounds[1]
            # x2 = X[j-1] - bounds[1]
            # alpha = x1*x2/(x2-x0)/(x1-x0)
            # beta = -x0*x2/(x1-x0)/(x2-x1)
            # gamma = x0*x1/(x2-x0)/(x2-x1)
            # data[l,0] = (hStar - beta*W[j,0] -
            #              gamma*W[j-1,0])/alpha
            data[l,0] = hStar

        #Right border discharge given by the right going Riemann invariant (subsonic case)
        uStar = vj + 2*(np.sqrt(g*hj) - np.sqrt(g*hStar))
        qStar = hStar*uStar
        for l in rightGhosts:
            # x0 = X[l] - bounds[1]
            # x1 = X[j] - bounds[1]
            # x2 = X[j-1] - bounds[1]
            # alpha = x1*x2/(x2-x0)/(x1-x0)
            # beta = -x0*x2/(x1-x0)/(x2-x1)
            # gamma = x0*x1/(x2-x0)/(x2-x1)
            # data[l,1] = (qStar - beta*W[j,1] -
            #              gamma*W[j-1,1])/alpha
            data[l,1] = qStar

        # if (uStar-np.sqrt(g*hStar))*(uStar+np.sqrt(g*hStar))>0.:
        #     print("Warning: non subsonic flow at right ghost")

        return data

    def ApplyReconstructionOperator(self, cl, mesh, W, time):
        #TODO: treat the case of periodic boundary conditions appart
        return self.ReconstructionOperator.ReconstructData(cl, mesh, self, W)
