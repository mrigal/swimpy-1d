import numpy as np
from scipy import sparse
from .SchemeClass import *

class FD_centered_gravity_waves_O2(SchemeClass):

    def __init__(self, ti=None):
        super(FD_centered_gravity_waves_O2, self).__init__(ti=ti)
        self.SchemeName = "FD_centered_gravity_waves_O2"

    def ComputeCellVariation(self, cl, mesh, W, time):
        iv = mesh.GetInteriorVerticesRange()
        ic = mesh.GetInteriorCellsRange()

        dx_ = mesh.GetDx()
        dx = dx_[ic]

        z = mesh.GetBathymetryArray()

        nVertices = len(iv)
        N = cl.GetSystemSize()

        g = cl.GetGravityConstant()

        # G = cl.ComputeFastFlux(W, z) #- cl.ComputeFastSourceTermContribution(W, z)
        # DG = .5*(G[ic-1,:] - G[ic+1,:])/dx[:,np.newaxis]

        ########################################################################

        WL = W[ic-1,:]
        WC = W[ic,:]
        WR = W[ic+1,:]

        zL = z[ic-1]
        zC = z[ic]
        zR = z[ic+1]

        # GL = cl.ComputeFastFlux(WL, zL)
        # GR = cl.ComputeFastFlux(WR, zR)

        # SL = cl.ComputeFastSourceTermContribution(WC, zL)
        # SR = cl.ComputeFastSourceTermContribution(WC, zR)

        # DG = .5*(GL - GR + SR - SL)/dx[:,np.newaxis]

        DG_h = -.5*(WR[:,1] - WL[:,1])/dx
        DG_q = .5*g*zC*(WR[:,0] + zR - WL[:,0] - zL)/dx#[:,np.newaxis]

        DG = np.empty(WC.shape)
        DG[:,0] = DG_h
        DG[:,1] = DG_q

        return DG

    def AssembleMassMatrix(self, cl, mesh, aii, dt):

        systemSize = cl.GetSystemSize()
        Nc = mesh.GetNumberOfCells(ghosts=False)
        ic = mesh.GetInteriorCellsRange()

        #Left neighbors (periodic only!)
        ln = np.arange(0, Nc) - 1
        ln[0] = Nc - 1

        #Right neighbor
        rn = np.arange(0, Nc) + 1
        rn[-1] = 0

        dx = mesh.GetDx()
        sigma = dt/dx[ic]

        z = mesh.GetBathymetryArray()
        g = cl.GetGravityConstant()
        c2 = -g*z[ic]

        nnz_cell = 6
        nnz = Nc*nnz_cell

        I = np.zeros(nnz)
        J = np.zeros(nnz)
        V = np.zeros(nnz)

        j = np.arange(0, nnz, nnz_cell)
        iD = np.arange(0, Nc)

        #Height coefficients
        #Left discharge contribution
        I[j] = 2*iD
        J[j] = 2*ln + 1
        V[j] = -.5*aii*sigma
        j += 1
        #Right discharge contribution
        I[j] = 2*iD
        J[j] = 2*rn + 1
        V[j] = .5*aii*sigma
        j += 1
        #Self contribution
        I[j] = 2*iD
        J[j] = 2*iD
        V[j] = 1
        j += 1

        #Discharge coefficients
        #Left height contribution
        I[j] = 2*iD + 1
        J[j] = 2*ln
        V[j] = -.5*c2*aii*sigma
        j += 1
        #Right height contribution
        I[j] = 2*iD + 1
        J[j] = 2*rn
        V[j] = .5*c2*aii*sigma
        j += 1
        #Self contribution
        I[j] = 2*iD + 1
        J[j] = 2*iD + 1
        V[j] = 1

        # A = sparse.coo_matrix((V, (I, J)), shape=(2*Nc, 2*Nc))
        A = sparse.csr_matrix((V, (I, J)), shape=(2*Nc, 2*Nc))

        return A

    def AssembleRHS(self, cl, mesh, U, aii, time, dt):

        systemSize = cl.GetSystemSize()
        Nc = mesh.GetNumberOfCells(ghosts=False)
        ic = mesh.GetInteriorCellsRange()

        #Left neighbors (periodic only!)
        ln = np.arange(0, Nc) - 1
        ln[0] = Nc - 1

        #Right neighbor
        rn = np.arange(0, Nc) + 1
        rn[-1] = 0

        dx = mesh.GetDx()
        sigma = dt/dx[ic]

        z = mesh.GetBathymetryArray()
        g = cl.GetGravityConstant()
        c2 = -g*z[ic]

        RHS = np.empty((2*Nc,))
        iD = np.arange(0, Nc)

        h = U[:,0]
        q = U[:,1]

        RHS[2*iD] = h[ic]
        RHS[2*iD+1] = q[ic] - .5*c2*aii*sigma*(z[ic[rn]] - z[ic[ln]])

        return RHS
