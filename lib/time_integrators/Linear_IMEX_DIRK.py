import numpy as np
from scipy.sparse.linalg import spsolve
from .TimeIntegratorClass import *

class Linear_IMEX_DIRK(TimeIntegratorClass):

    def __init__(self):
        super(Linear_IMEX_DIRK, self).__init__()
        self.TimeIntegratorName = "Explicit Runge-Kutta"
        self.ButcherTableau_AE = None
        self.ButcherTableau_bE = None
        self.ButcherTableau_cE = None
        self.ButcherTableau_AI = None
        self.ButcherTableau_bI = None
        self.ButcherTableau_cI = None
        self.ButcherTableauName = None

    def __str__(self):
        string = super(Linear_IMEX_DIRK, self).__str__()
        string += \
            "\n    Butcher tableau: {}".format(self.ButcherTableauName)
        return string

    def SetButcherTableau(self, AE, bE, cE, AI, bI, cI, name=None):
        self.ButcherTableau_AE = AE
        self.ButcherTableau_bE = bE
        self.ButcherTableau_cE = cE

        assert(AE.shape[0] == AE.shape[1])
        assert(AE.shape[1] == bE.shape[0])
        assert(AE.shape[0] == cE.shape[0])

        self.ButcherTableau_AI = AI
        self.ButcherTableau_bI = bI
        self.ButcherTableau_cI = cI

        assert(AI.shape[0] == AI.shape[1])
        assert(AI.shape[1] == bI.shape[0])
        assert(AI.shape[0] == cI.shape[0])

        self.ButcherTableauName = name

    def GetButcherTableau(self):
        return \
            self.ButcherTableau_AE, \
            self.ButcherTableau_bE, \
            self.ButcherTableau_cE, \
            self.ButcherTableau_AI, \
            self.ButcherTableau_bI, \
            self.ButcherTableau_cI

    def Update(self, scheme, cl, mesh, time, dt):
        
        data = mesh.GetCellData()

        if abs(dt) < np.spacing(1):
            return data

        systemSize = cl.GetSystemSize()
        nCellsIncludingGhosts = mesh.GetNumberOfCells(ghosts=True)
        nCells = mesh.GetNumberOfCells(ghosts=False)

        ic = mesh.GetInteriorCellsRange()

        dx_ = mesh.GetDx()
        dx = dx_[ic]

        AE, bE, cE, AI, bI, cI = self.GetButcherTableau()
        nStages = AE.shape[0]
        partialUpdates = np.zeros((nStages, nCellsIncludingGhosts, systemSize))
        cellVar_i_j = np.empty((nCells, systemSize))
        Vbar = np.empty((nCellsIncludingGhosts, systemSize))

        time = self.GetCurrentTime()

        #Compute partial updates
        for i in range(nStages):
            #Slow operator contribution
            cellVariationRate = np.zeros((nCells, systemSize))

            for j in range(i):
                Uj = scheme.ApplyBNDC(
                    cl, mesh, partialUpdates[j,:,:], time #+ cE[j]*dt
                )
                numericalFlux, correctionL, correctionR = \
                    scheme.ComputeSlowCellVariation(
                        cl, mesh, Uj, time + cE[j]*dt
                    )
                FL = numericalFlux[:-1,:]
                FR = numericalFlux[1:,:]
                cellVar_i_j[:,:] = \
                    (FL + correctionL[1:,:] - FR - correctionR[:-1,:]) \
                    / dx[:,np.newaxis]
                cellVariationRate = cellVariationRate + AE[i,j]*cellVar_i_j

                #Fast operator contribution
                Uj = scheme.ApplyBNDC(
                    cl, mesh, partialUpdates[j,:,:], time #+ cI[j]*dt
                )
                cellVar_i_j[:,:] = scheme.ComputeFastCellVariation(cl, mesh, Uj, time)
                cellVariationRate = cellVariationRate + AI[i,j]*cellVar_i_j

            Vbar[ic,:] = data[ic,:] + dt*cellVariationRate

            # partialUpdates[i,ic,:] = data[ic,:] + dt*cellVariationRate

            if np.spacing(1) <= np.abs(AI[i,i]):
                partialUpdates[i,ic,:] = \
                    self.SolveImplicitPart(scheme, cl, mesh, Vbar, i, time, dt);
            else:
                partialUpdates[i,:,:] = Vbar
                

        #Final update
        cellVariationRate = np.zeros((nCells, systemSize))
        for j in range(nStages):
            #Slow operator contribution
            Uj = scheme.ApplyBNDC(
                cl, mesh, partialUpdates[j,:,:], time #+ cE[j]*dt
            )
            numericalFlux, correctionL, correctionR = \
                scheme.ComputeSlowCellVariation(cl, mesh, Uj, time + cE[j]*dt) #TODO: do we need to add cE[j]*dt?
            FL = numericalFlux[:-1,:]
            FR = numericalFlux[1:,:]
            cellVar_i_j[:,:] = \
                (FL + correctionL[1:,:] - FR - correctionR[:-1,:]) \
                / dx[:,np.newaxis]
            cellVariationRate = cellVariationRate + bE[j]*cellVar_i_j

            #Fast operator contribution
            Uj = scheme.ApplyBNDC(
                cl, mesh, partialUpdates[j,:,:], time #+ cI[j]*dt
            )
            cellVar_i_j[:,:] = scheme.ComputeFastCellVariation(cl, mesh, Uj, time)
            cellVariationRate = cellVariationRate + bI[j]*cellVar_i_j

        nextData = np.empty((nCellsIncludingGhosts, systemSize))
        nextData[ic,:] = data[ic,:] + dt*cellVariationRate

        return nextData

    def SolveImplicitPart(self, scheme, cl, mesh, Vbar, i, time, dt):
        massMatrix = scheme.AssembleMassMatrix(cl, mesh, i, dt)
        RHS = scheme.AssembleRHS(cl, mesh, Vbar, i, time, dt)

        # print(massMatrix.shape)
        # print(RHS.shape)

        # Ui_ = np.linalg.solve(massMatrix, RHS)
        Ui_ = spsolve(massMatrix, RHS)

        h = Ui_[0::2]
        q = Ui_[1::2]

        Ui = np.stack((h, q), axis=1)

        return Ui
