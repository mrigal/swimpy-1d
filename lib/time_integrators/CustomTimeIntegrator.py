import numpy as np
from .TimeIntegratorClass import *

class CustomTimeIntegrator(TimeIntegratorClass):

    def Update(self, scheme, cl, mesh, time, dt):
        raise NotImplementedError()

    def SetCustomUpdate(self, update):
        self.Update = update
