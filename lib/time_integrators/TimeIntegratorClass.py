class TimeIntegratorClass(object):

    def __init__(self):
        self.StartingTime = 0
        self.EndingTime = None
        self.CurrentTime = None
        self.TimeIntegratorName = "None"

    def __str__(self):
        if self.CurrentTime is None:
            currentTime = "not yet initialized"
        else:
            currentTime = self.CurrentTime
        string = \
            "TimeIntegratorClass instance:\n" \
            "    Name: {}\n" \
            "    Starting time: {}\n" \
            "    Ending time: {}\n" \
            "    Current time: {}".format(
                self.TimeIntegratorName,
                self.StartingTime,
                self.EndingTime,
                currentTime
            )
        return string

    def SetStartingTime(self, startingTime):
        self.StartingTime = startingTime

    def GetStartingTime(self):
        return self.StartingTime

    def SetEndingTime(self, endingTime):
        self.EndingTime = endingTime

    def GetEndingTime(self):
        return self.EndingTime

    def SetTimeIntegratorName(self, name):
        self.TimeIntegratorName = name

    def GetTimeIntegratorName(self):
        return self.TimeIntegratorName

    def InitializeTimer(self, value=None):
        if value is None:
            self.CurrentTime = self.StartingTime
        else:
            self.CurrentTime = value

    def GetCurrentTime(self):
        return self.CurrentTime

    def AdvanceCurrentTime(self, dt):
        self.CurrentTime = self.CurrentTime + dt

    def EndingTimeHasBeenReached(self):
        if self.CurrentTime >= self.EndingTime:
            return True
        return False
