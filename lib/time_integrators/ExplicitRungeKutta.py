import numpy as np
from .TimeIntegratorClass import *

class ExplicitRungeKutta(TimeIntegratorClass):

    def __init__(self):
        super(ExplicitRungeKutta, self).__init__()
        self.TimeIntegratorName = "Explicit Runge-Kutta"
        self.ButcherTableau_A = None
        self.ButcherTableau_b = None
        self.ButcherTableau_c = None
        self.ButcherTableauName = None

    def __str__(self):
        string = super(ExplicitRungeKutta, self).__str__()
        string += \
            "\n    Butcher tableau: {}".format(self.ButcherTableauName)
        return string

    def SetButcherTableau(self, A, b, c, name=None):
        self.ButcherTableau_A = A
        self.ButcherTableau_b = b
        self.ButcherTableau_c = c

        assert(A.shape[0] == A.shape[1])
        assert(A.shape[1] == b.shape[0])
        assert(A.shape[0] == c.shape[0])

        self.ButcherTableauName = name

    def GetButcherTableau(self):
        return \
            self.ButcherTableau_A, \
            self.ButcherTableau_b, \
            self.ButcherTableau_c

    def Update(self, scheme, cl, mesh, time, dt):
        
        data = mesh.GetCellData()

        if abs(dt) < np.spacing(1):
            return data

        systemSize = cl.GetSystemSize()
        nCellsIncludingGhosts = mesh.GetNumberOfCells(ghosts=True)
        nCells = mesh.GetNumberOfCells(ghosts=False)

        ic = mesh.GetInteriorCellsRange()

        dx_ = mesh.GetDx()
        dx = dx_[ic]

        A, b, c = self.GetButcherTableau()
        nStages = A.shape[0]
        partialUpdates = np.zeros((nStages, nCellsIncludingGhosts, systemSize))
        cellVar_i_j = np.empty((nCells, systemSize))

        time = self.GetCurrentTime()

        #Compute partial updates
        for i in range(nStages):
            cellVariationRate = np.zeros((nCells, systemSize))

            for j in range(i):
                Uj = scheme.ApplyBNDC(
                    cl, mesh, partialUpdates[j,:,:], time + c[j]*dt
                )
                numericalFlux, correctionL, correctionR = \
                    scheme.ComputeCellVariation(cl, mesh, Uj, time + c[j]*dt)
                FL = numericalFlux[:-1,:]
                FR = numericalFlux[1:,:]
                cellVar_i_j[:,:] = \
                    (FL + correctionL[1:,:] - FR - correctionR[:-1,:])/dx[:,np.newaxis]
                cellVariationRate = cellVariationRate + A[i,j]*cellVar_i_j
                
            partialUpdates[i,ic,:] = data[ic,:] + dt*cellVariationRate

        #Final update
        cellVariationRate = np.zeros((nCells, systemSize))
        for j in range(nStages):
            Uj = scheme.ApplyBNDC(
                cl, mesh, partialUpdates[j,:,:], time + c[j]*dt
            )
            numericalFlux, correctionL, correctionR = \
                scheme.ComputeCellVariation(cl, mesh, Uj, time + c[j]*dt)
            FL = numericalFlux[:-1,:]
            FR = numericalFlux[1:,:]
            cellVar_i_j[:,:] = \
                (FL + correctionL[1:,:] - FR - correctionR[:-1,:])/dx[:,np.newaxis]
            cellVariationRate = cellVariationRate + b[j]*cellVar_i_j

        nextData = np.empty((nCellsIncludingGhosts, systemSize))
        nextData[ic,:] = data[ic,:] + dt*cellVariationRate
            

        # # time = scheme.ti.GetCurrentTime()
        # time = self.GetCurrentTime()

        # numericalFlux, correctionL, correctionR = \
        #     scheme.ComputeNumericalFluxes(cl, mesh, data, time)

        # FL = numericalFlux[:-1,:]
        # FR = numericalFlux[1:,:]

        # nextData = np.empty(data.shape)

        # nextData[ic,:] = data[ic,:] - \
        #     dt/dx[:,np.newaxis]*(FR + correctionR[:-1,:] - FL - correctionL[1:,:]) # + dt*source

        return nextData
