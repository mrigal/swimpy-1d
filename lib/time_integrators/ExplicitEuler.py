import numpy as np
from .TimeIntegratorClass import *

class ExplicitEuler(TimeIntegratorClass):

    def __init__(self):
        super(ExplicitEuler, self).__init__()
        self.TimeIntegratorName = "Explicit Euler"

    def Update(self, scheme, cl, mesh, time, dt):
        data = mesh.GetCellData()

        if abs(dt) < np.spacing(1):
            return data

        ic = mesh.GetInteriorCellsRange()

        dx_ = mesh.GetDx()
        dx = dx_[ic]

        # time = scheme.ti.GetCurrentTime()
        time = self.GetCurrentTime()

        numericalFlux, correctionL, correctionR = \
            scheme.ComputeCellVariation(cl, mesh, data, time)

        FL = numericalFlux[:-1,:]
        FR = numericalFlux[1:,:]

        nextData = np.empty(data.shape)

        nextData[ic,:] = data[ic,:] - \
            dt/dx[:,np.newaxis]*(FR + correctionR[:-1,:] - FL - correctionL[1:,:]) # + dt*source

        return nextData
