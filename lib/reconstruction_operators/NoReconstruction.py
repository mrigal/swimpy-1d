import numpy as np
from .ReconstructionOperator import *

class NoReconstruction(ReconstructionOperator):

    def __init__(self):
        self.limiter = None
        self.name = "deactivated"
        self.limiterName = "none"

    def __str__(self):
        return super(NoReconstruction, self).__str__()

    def ReconstructData(self, cl, mesh, scheme, W):

        iv = mesh.GetInteriorVerticesRange()
        
        z = mesh.GetBathymetryArray()
        zL = z[iv-1]
        zR = z[iv]

        nVertices = mesh.GetNumberOfVertices(ghosts=False)
        # nVertices = len(iv)
        systemSize = cl.GetSystemSize()
        WL_ = W[iv-1,:]
        WR_ = W[iv,:]
        
        zL_ = zL
        zR_ = zR
        
        CorrectionL_ = np.zeros((nVertices, systemSize))
        CorrectionR_ = np.zeros((nVertices, systemSize))
        
        g = cl.GetGravityConstant()
        
        zE = .5*(zL + zR)
        
        #Centered source term
        # CorrectionL_[:,1] = -g*WL_[:,0]*zE
        # CorrectionR_[:,1] = -g*WR_[:,0]*zE
        CorrectionL_ = cl.ComputeSourceTermContribution(WL_, zE)
        CorrectionR_ = cl.ComputeSourceTermContribution(WR_, zE)

        return WL_, WR_, CorrectionL_, CorrectionR_, zL_, zR_
