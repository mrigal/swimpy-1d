import numpy as np
from .ReconstructionOperator import *

class HydroRec(ReconstructionOperator):

    def __init__(self):
        super(HydroRec, self).__init__()
        self.name = "HydroRec"

    def __str__(self):
        return super(HydroRec, self).__str__()

    def ReconstructData(self, cl, mesh, scheme, W):

        #WL, WR number of elements = num of cells - 1
        #Return Rec,Correction, number of elements = number of interior vertices
        
        iv = mesh.GetInteriorVerticesRange()
        
        data = mesh.GetCellData()
        
        z = mesh.GetBathymetryArray()
        zL = z[iv-1]
        zR = z[iv]
        z_ = np.maximum(zL, zR)
        
        WL = W[iv-1,:]
        WR = W[iv,:]
        
        hL = WL[:,0]
        hR = WR[:,0]
        
        hL_ = np.maximum(hL + zL - z_, 0.)
        hR_ = np.maximum(hR + zR - z_, 0.)
        
        vL = cl.ComputeVelocity(WL)
        vR = cl.ComputeVelocity(WR)
        
        qL_ = hL_*vL
        qR_ = hR_*vR
        
        RecL = np.stack((hL_, qL_), axis=1)
        RecR = np.stack((hR_, qR_), axis=1)
        
        # nVertices = len(mesh.GetInteriorCellsRange()) + 1
        nVertices = len(iv)
        N = cl.GetSystemSize()
        
        CorrectionL = np.zeros((nVertices, N));
        CorrectionR = np.zeros((nVertices, N));
        
        g = cl.GetGravityConstant()
        
        CorrectionL[:,1] = .5*g*(hL_**2 - hL**2)
        CorrectionR[:,1] = .5*g*(hR_**2 - hR**2)
        
        return RecL, RecR, CorrectionL, CorrectionR, zL, zR
