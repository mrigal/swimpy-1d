import numpy as np
from .ReconstructionOperator import *

class HydroRec_O2(ReconstructionOperator):

    def __init__(self):
        super(HydroRec_O2, self).__init__()
        self.name = "HydroRec_O2"

    def __str__(self):
        return super(HydroRec_O2, self).__str__()

    def ReconstructData(self, cl, mesh, scheme, W):

        assert(4 <= mesh.GetNumberOfGhosts())

        limiter = self.GetLimiter()
        
        nCellsIncludingGhosts = mesh.GetNumberOfCells(ghosts=True)
        nCells = mesh.GetNumberOfCells(ghosts=False)
        systemSize = cl.GetSystemSize()
        
        cL, cR = mesh.GetBorderCellsID()
        cID_L = np.arange(cL-2, cR+2, dtype=np.int64)
        cID_R = np.arange(cL-1, cR+3, dtype=np.int64)

        dx = mesh.GetDx()
        dx_L = dx[cID_L]
        dx_R = dx[cID_R]
        
        z = mesh.GetBathymetryArray()
        zL = z[cID_L]
        zR = z[cID_R]
        
        cc = mesh.GetCellCentersArray()
        ccL = cc[cID_L]
        ccR = cc[cID_R]
        dist = np.abs(ccR - ccL)
        
        WL = W[cID_L,:]
        WR = W[cID_R,:]

        # #Passing in (Zeta,Q) coordinates
        # WL[:,0] += zL
        # WR[:,0] += zR

        dryNeighborhood = (WL[:,0] > np.spacing(1))*(WR[:,0] > np.spacing(1))*0
        # dryCell = (np.spacing(1) >= W[cL:cR+3,0])
        dryCell = (np.spacing(1) >= W[cL-1:cR+2,0])#*0

        #2nd order reconstruction of (Zeta,Q)
        varVertW = (WR - WL)/dist[:,np.newaxis]*(1-dryNeighborhood[:,np.newaxis])
        # varVertW[:,0] *= dryNeighborhood
        varCellW = .5*(varVertW[:-1,:] + varVertW[1:,:])*(1 - dryCell[:,np.newaxis])
        dW_L = limiter(varCellW[:-1,:], varVertW[1:-1,:])
        dW_R = limiter(varCellW[1:,:], varVertW[1:-1,:])
        RecL_ = WL[1:-1,:] + .5*dx_L[1:-1,np.newaxis]*dW_L
        RecR_ = WR[1:-1,:] - .5*dx_R[1:-1,np.newaxis]*dW_R
        # testL = (0. < WL[1:-1,0])
        # testR = (0. < WR[1:-1,0])
        # dW_L_ = np.maximum(dW_L[:,0], -2.*WL[1:-1,0]/dx_L[1:-1])*testL - dZ_L*(1-testL)#*(0. < WL[1:-1,0])
        # dW_R_ = np.minimum(dW_R[:,0], 2.*WR[1:-1,0]/dx_R[1:-1])*testR - dZ_R*(1-testR)#*(0. < WR[1:-1,0])
        # RecL_[:,0] = WL[1:-1,0] + .5*dx_L[1:-1]*dW_L_
        # RecR_[:,0] = WR[1:-1,0] - .5*dx_R[1:-1]*dW_R_

        negativeL = (RecL_[:,0] <= np.spacing(1))#*(WL[1:-1,0] <= np.spacing(1))
        negativeR = (RecR_[:,0] <= np.spacing(1))#*(WR[1:-1,0] <= np.spacing(1))
        delta_L = np.copy(RecL_[:,0])
        delta_R = np.copy(RecR_[:,0])
        RecL_[:,0] *= (WL[1:-1,0] > np.spacing(1))
        RecR_[:,0] *= (WR[1:-1,0] > np.spacing(1))
        RecL_[:,0] = np.maximum(0., RecL_[:,0])
        RecR_[:,0] = np.maximum(0., RecR_[:,0])
        RecL_[:,1] *= (1 - negativeL)
        RecR_[:,1] *= (1 - negativeR)
        # RecL_[:,:] = RecL_[:,:]*(1-negativeL[:,np.newaxis]) + WL[1:-1,:]*negativeL[:,np.newaxis]
        # RecR_[:,:] = RecR_[:,:]*(1-negativeR[:,np.newaxis]) + WR[1:-1,:]*negativeR[:,np.newaxis]
        delta_L -= RecL_[:,0]
        delta_R -= RecR_[:,0]

        # #Passing in (H,Q) coordinates
        # RecL_[:,0] = np.maximum(0., RecL_[:,0] - zL_)
        # RecR_[:,0] = np.maximum(0., RecR_[:,0] - zR_)

        #2nd order reconstruction of topography
        varVertZ = (zR - zL)/dist*(1-dryNeighborhood)
        # varVertZ *= dryNeighborhood
        varCellZ = .5*(varVertZ[:-1] + varVertZ[1:])*(1 - dryCell)
        dZ_L = limiter(varCellZ[:-1], varVertZ[1:-1])
        dZ_R = limiter(varCellZ[1:], varVertZ[1:-1])
        # dZ_L = -dW_L[:,0]
        # dZ_R = -dW_R[:,0]
        #Preserve LAR when water height reconstruction canceled due to nonpositivity
        # delta_L = dW_L[:,0] - dW_L_
        # delta_R = dW_R[:,0] - dW_R_
        zL_ = zL[1:-1] + .5*dx_L[1:-1]*dZ_L*(1-negativeL) + delta_L*negativeL
        zR_ = zR[1:-1] - .5*dx_R[1:-1]*dZ_R*(1-negativeR) + delta_R*negativeR
        # zL_ = zL[1:-1] + .5*dx_L[1:-1]*(dZ_L + 1*delta_L)
        # zR_ = zR[1:-1] - .5*dx_R[1:-1]*(dZ_R + 1*delta_R)
        # zL_ = zL[1:-1] - .5*dx_L[1:-1]*dW_L_
        # zR_ = zR[1:-1] + .5*dx_R[1:-1]*dW_R_

        ##############################################################

        #Applying hydrostatic reconstruction
        hL = RecL_[:,0]
        hR = RecR_[:,0]
        z_ = np.maximum(zL_, zR_)
        hL_ = np.maximum(0., hL + zL_ - z_)
        hR_ = np.maximum(0., hR + zR_ - z_)

        vL = cl.ComputeVelocity(RecL_)
        vR = cl.ComputeVelocity(RecR_)

        qL_ = hL_*vL
        qR_ = hR_*vR

        CorrectionL = np.zeros((nCells+1, systemSize))
        CorrectionR = np.zeros((nCells+1, systemSize))

        g = cl.GetGravityConstant()
        CorrectionL[1:,1] = .5*g*(hL_[1:]**2 - hL[1:]**2 - (hL[1:] + hR[:-1])*(zL_[1:]))
        CorrectionR[:-1,1] = .5*g*(hR_[:-1]**2 - hR[:-1]**2 - (hL[1:] + hR[:-1])*(zR_[:-1]))

        RecL_[:,0] = hL_
        RecL_[:,1] = qL_
        
        RecR_[:,0] = hR_
        RecR_[:,1] = qR_
        
        return RecL_, RecR_, CorrectionL, CorrectionR, zL_[1:-1], zR_[1:-1]
        # return RecL_, RecR_, CorrectionL, CorrectionR, z_[0:-2], z_[1:-1]


        ##############################################################

        


    def OldOldReconstructData(self, cl, mesh, scheme, W):

        #WL, WR number of elements = num of cells - 1
        #Return Rec,Correction, number of elements = number of interior vertices
        
        assert(4 <= mesh.GetNumberOfGhosts())

        limiter = self.GetLimiter()
        
        nCellsIncludingGhosts = mesh.GetNumberOfCells(ghosts=True)
        nCells = mesh.GetNumberOfCells(ghosts=False)
        systemSize = cl.GetSystemSize()
        
        cL, cR = mesh.GetBorderCellsID()
        cID_L = np.arange(cL-2, cR+2, dtype=np.int64)
        cID_R = np.arange(cL-1, cR+3, dtype=np.int64)
        
        dx = mesh.GetDx()
        dx_L = dx[cID_L]
        dx_R = dx[cID_R]
        
        # iL, iR = mesh.GetBorderVerticesID()
        # vID_L = np.arange(iL-1, iR+1, dtype=int64)
        # vID_R = np.arange(iL, iR+2, dtype=int64)
        
        z = mesh.GetBathymetryArray()
        zL = z[cID_L]
        zR = z[cID_R]
        
        cc = mesh.GetCellCentersArray()
        ccL = cc[cID_L]
        ccR = cc[cID_R]
        dist = np.abs(ccR - ccL)
        
        WL = W[cID_L,:]
        WR = W[cID_R,:]

        # vL = cl.ComputeVelocity(WL)
        # vR = cl.ComputeVelocity(WR)

        # WL[:,1] = vL
        # WR[:,1] = vR
        
        varVertW = (WR - WL)/dist[:,np.newaxis]
        varVertZ = (zR - zL)/dist
        
        #Approximate space derivative
        # ic = mesh.GetInteriorCellsRange()
        # varCellW = np.empty((nCells + 2, systemSize))
        #TODO: take into account non uniform meshes!
        varCellW = .5*(varVertW[:-1,:] + varVertW[1:,:])
        varCellZ = .5*(varVertZ[:-1] + varVertZ[1:])
        
        # for i in range(systemSize):
        dW_L = limiter(varCellW[:-1,:], varVertW[1:-1,:])
        dW_R = limiter(varCellW[1:,:], varVertW[1:-1,:])
        
        RecL_ = WL[1:-1,:] + .5*dx_L[1:-1,np.newaxis]*dW_L
        RecR_ = WR[1:-1,:] - .5*dx_R[1:-1,np.newaxis]*dW_R

        dW_L_ = np.maximum(dW_L[:,0], -2*WL[1:-1,0]/dx_L[1:-1])
        dW_R_ = np.minimum(dW_R[:,0], 2*WR[1:-1,0]/dx_R[1:-1])

        RecL_[:,0] = WL[1:-1,0] + .5*dx_L[1:-1]*dW_L_
        RecR_[:,0] = WR[1:-1,0] - .5*dx_R[1:-1]*dW_R_

        delta_L = dW_L[:,0] - dW_L_
        delta_R = dW_R[:,0] - dW_R_

        ############################################################################
        #TODO: is it necessary to have a positive reconstruction for the water height?
        # RecL_[:,0] = np.maximum(0, RecL_[:,0])
        # RecR_[:,0] = np.maximum(0, RecR_[:,0])
        # assert(np.all(0. <= RecL_[:,0]))
        # assert(np.all(0. <= RecR_[:,0]))
        # zerosL_ = (np.spacing(1) >= RecL_[:,0])
        # zerosR_ = (np.spacing(1) >= RecR_[:,0])
        # RecL_[:,1] *= (1 - zerosL_)
        # RecR_[:,1] *= (1 - zerosR_)
        ############################################################################

        # if not np.all(RecL_[:,0]>=0) or not np.all(RecR_[:,0]>=0):# or (np.any(RecR_[:,0]<0.)) is True:
        #     print("Warning: negative reconstructed water height.")
        
        # assert(np.all(RecL_[:,0]>=-np.spacing(1)))
        # assert(np.all(RecR_[:,0]>=-np.spacing(1)))

        dZ_L = limiter(varCellZ[:-1], varVertZ[1:-1])
        dZ_R = limiter(varCellZ[1:], varVertZ[1:-1])

        zL_ = zL[1:-1] + .5*dx_L[1:-1]*(dZ_L + 0.*delta_L)
        zR_ = zR[1:-1] - .5*dx_R[1:-1]*(dZ_R + 0.*delta_R)
        # zL_ = zL[1:-1] - .5*dx_L[1:-1]*dW_L[:,0]
        # zR_ = zR[1:-1] + .5*dx_R[1:-1]*dW_R[:,0]

        hL = np.maximum(0, RecL_[:,0])
        hR = np.maximum(0, RecR_[:,0])

        # hL = RecL_[:,0]
        # hR = RecR_[:,0]

        # zL_ = zL[1:-1] - hL + WL[1:-1,0]
        # zR_ = zR[1:-1] - hR + WR[1:-1,0]

        z_ = np.maximum(zL_, zR_)
        
        hL_ = np.maximum(hL + zL_ - z_, 0.)
        hR_ = np.maximum(hR + zR_ - z_, 0.)

        vL = cl.ComputeVelocity(RecL_)
        vR = cl.ComputeVelocity(RecR_)

        # vL = cl.ComputeVelocity(WL[1:-1,:])
        # vR = cl.ComputeVelocity(WR[1:-1,:])

        # vL = WL[1:-1,1]
        # vR = WR[1:-1,1]

        qL_ = hL_*vL
        qR_ = hR_*vR
        
        CorrectionL = np.zeros((nCells+1, systemSize))
        CorrectionR = np.zeros((nCells+1, systemSize))
        
        g = cl.GetGravityConstant()
        # CorrectionL[:,1] = -.5*g*(hL*hL - hL_*hL_ + (hL + WL[1:-1,0])*(zL_ - zL[1:-1]))
        # CorrectionR[:,1] = -.5*g*(hR*hR - hR_*hR_ + (hR + WR[1:-1,0])*(zR_ - zR[1:-1]))

        # CorrectionL[:,1] = -.5*g*(hL_*hL_ - hL*hL + 0*(hL + hR)*(zL_))
        # CorrectionR[:,1] = -.5*g*(hR_*hR_ - hR*hR + 0*(hL + hR)*(-zR_))

        CorrectionL[1:,1] = .5*g*(hL_[1:]**2 - hL[1:]**2 - (hL[1:] + hR[:-1])*(zL_[1:]))
        CorrectionR[:-1,1] = .5*g*(hR_[:-1]**2 - hR[:-1]**2 - (hL[1:] + hR[:-1])*(zR_[:-1]))

        RecL_[:,0] = hL_
        RecL_[:,1] = qL_
        
        RecR_[:,0] = hR_
        RecR_[:,1] = qR_
        
        return RecL_, RecR_, CorrectionL, CorrectionR, zL_[1:-1], zR_[1:-1]
