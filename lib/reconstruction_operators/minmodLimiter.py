import numpy as np

def minmodLimiter(a, b):
    #Minmod limiter
    res = .5*(np.sign(a)+np.sign(b))*np.minimum(np.abs(a), np.abs(b))
    return res
