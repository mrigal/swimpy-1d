import numpy as np
from .ReconstructionOperator import *

class MUSCL_O2(ReconstructionOperator):

    def __init__(self):
        super(MUSCL_O2, self).__init__()
        self.name = "MUSCL_O2"

    def __str__(self):
        return super(MUSCL_O2, self).__str__()

    def ReconstructData(self, cl, mesh, scheme, W):

        #WL, WR number of elements = num of cells - 1
        #Return Rec,Correction, number of elements = number of interior vertices
        
        assert(4 <= mesh.GetNumberOfGhosts())
        
        limiter = self.GetLimiter()
        
        nCellsIncludingGhosts = mesh.GetNumberOfCells(ghosts=True)
        nCells = mesh.GetNumberOfCells(ghosts=False)
        systemSize = cl.GetSystemSize()
        
        cL, cR = mesh.GetBorderCellsID()
        cID_L = np.arange(cL-2, cR+2, dtype=np.int64)
        cID_R = np.arange(cL-1, cR+3, dtype=np.int64)
        
        dx = mesh.GetDx()
        dx_L = dx[cID_L]
        dx_R = dx[cID_R]
        
        # iL, iR = mesh.GetBorderVerticesID()
        # vID_L = np.arange(iL-1, iR+1, dtype=int64)
        # vID_R = np.arange(iL, iR+2, dtype=int64)
        
        z = mesh.GetBathymetryArray()
        zL = z[cID_L]
        zR = z[cID_R]
        
        cc = mesh.GetCellCentersArray()
        ccL = cc[cID_L]
        ccR = cc[cID_R]
        dist = np.abs(ccR - ccL)
        
        WL = W[cID_L,:]
        WR = W[cID_R,:]
        
        varVertW = (WR - WL)/dist[:,np.newaxis]
        varVertZ = (zR - zL)/dist
        
        #Approximate space derivative
        # ic = mesh.GetInteriorCellsRange()
        # varCellW = np.empty((nCells + 2, systemSize))
        #TODO: take into account non uniform meshes!
        varCellW = .5*(varVertW[:-1,:] + varVertW[1:,:])
        varCellZ = .5*(varVertZ[:-1] + varVertZ[1:])
        
        # for i in range(systemSize):
        dW_L = limiter(varCellW[:-1,:], varVertW[1:-1,:])
        dW_R = limiter(varCellW[1:,:], varVertW[1:-1,:])
        
        RecL_ = WL[1:-1,:] + .5*dx_L[1:-1,np.newaxis]*dW_L
        RecR_ = WR[1:-1,:] - .5*dx_R[1:-1,np.newaxis]*dW_R
        
        ########################################################################
        #TODO: is it necessary to have a positive reconstruction for the water
        #height?
        RecL_[:,0] = np.maximum(0, RecL_[:,0])
        RecR_[:,0] = np.maximum(0, RecR_[:,0])
        ########################################################################
        
        dZ_L = limiter(varCellZ[:-1], varVertZ[1:-1])
        dZ_R = limiter(varCellZ[1:], varVertZ[1:-1])
        
        zL_ = zL[1:-1] + .5*dx_L[1:-1]*dZ_L
        zR_ = zR[1:-1] - .5*dx_R[1:-1]*dZ_R
        
        nVertices = nCells + 1
        CorrectionL_ = np.zeros((nVertices, systemSize))
        CorrectionR_ = np.zeros((nVertices, systemSize))
        
        g = cl.GetGravityConstant()
        
        zE = .5*(zL + zR)[1:-1]
        
        #Centered source term
        # CorrectionL_[:,1] = -g*RecL_[:,0]*zE
        # CorrectionR_[:,1] = -g*RecR_[:,0]*zE
        CorrectionL_ = cl.ComputeSourceTermContribution(RecL_, zL_)
        CorrectionR_ = cl.ComputeSourceTermContribution(RecR_, zR_)
        
        return RecL_, RecR_, CorrectionL_, CorrectionR_, zL_, zR_
      # return RecL_, RecR_, CorrectionL_, CorrectionR_, zL[1:-1], zR[1:-1]
