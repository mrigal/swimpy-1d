import numpy as np
from scipy.optimize import fsolve

class SaintVenant1D(object):

    SYSTEM_SIZE = 2

    def __init__(self):
        self.GravityConstant = None
        self.Froude = None
        self.Bathymetry = None

        #Riemann solver
        self.RS_RAREFACTION = 0
        self.RS_SHOCK       = 1
        self.RS_WAVE_1 = None
        self.RS_WAVE_2 = None
        self.RS_I1 = None
        self.RS_I2 = None
        self.RS_UL = None
        self.RS_UI = None
        self.RS_UR = None
        self.RS_evalulate_solution = None

    def GetSystemSize(self):
        return self.SYSTEM_SIZE

    def SetGravityConstant(self, g):
        self.GravityConstant = g
        self.Froude = 1/np.sqrt(g)

    def GetGravityConstant(self):
        return self.GravityConstant

    def SetFroudeCaracteristicNumber(self, Fr):
        self.Froude = Fr
        self.GravityConstant = Fr**(-2)

    def GetFroudeCaracteristicNumber(self):
        return self.Froude

    def SetBathymetry(self, bathymetryFunction):
        self.Bathymetry = bathymetryFunction

    def GetBathymetry(self):
        return self.Bathymetry

    def ComputeBathymetryAtPoints(self, points):
        return self.Bathymetry(points)

    def ComputeSourceTermContribution(self, U, z):
        g = self.GetGravityConstant()
        res = np.zeros(U.shape)
        res[:,1] = -g*U[:,0]*z
        return res

    def ComputeVelocity(self, W):
        #TODO: field should be chosen according to scheme
        h = W[:,0]
        test = (h >= 0.)
        assert(test.all)
        q = W[:,1]
        dryArea = (np.spacing(1) >= np.abs(h))
        v = q/(h+dryArea)*(1-dryArea)
        return v

    def ComputeFlux(self, W, _):
        g = self.GetGravityConstant()
        h = W[:,0]
        q = W[:,1]
        v = self.ComputeVelocity(W)

        Fh = q
        Fq = h*v*v + .5*g*h*h

        return np.stack((Fh, Fq), axis=1)

    def ComputeEigenvalues(self, W, _): #z?
        h = W[:,0]
        v = self.ComputeVelocity(W)
        c = np.sqrt(self.GravityConstant*h)

        E1 = v - c
        E2 = v + c
        
        return np.stack((E1, E2), axis=1)

    def ComputeOuterEigenvalues(self, W, _):
        eigenvalues = self.ComputeEigenvalues(W, _)
        return np.amin(eigenvalues, axis=1), np.amax(eigenvalues, axis=1)

    ############################################################################
    # Riemann solver over flat bottom
    ############################################################################

    def RS_ComputeSolution(self, UL, UR):
        hL = UL[0]
        hR = UR[0]
        h0 = .5*(hL + hR)
        UI = self.GetIntermediateState(UL, UR, h0)[:,0]
        
        self.RS_UL = UL
        self.RS_UI = UI
        self.RS_UR = UR
        
        if self.Is_R1(UL, UI):
            self.RS_WAVE_1 = self.RS_RAREFACTION
            self.RS_I1 = self.Compute_R1_Interval(UL, UI)
        else:
            self.RS_WAVE_1 = self.RS_SHOCK
            self.RS_I1 = self.Compute_S1_Speed(UL, UI)

        if self.Is_R2(UI, UR):
            self.RS_WAVE_2 = self.RS_RAREFACTION
            self.RS_I2 = self.Compute_R2_Interval(UI, UR)
        else:
            self.RS_WAVE_2 = self.RS_SHOCK
            self.RS_I2 = self.Compute_S2_Speed(UI, UR)

    def RS_GetSolution(self):
        
        if self.RS_WAVE_1 == self.RS_RAREFACTION and \
           self.RS_WAVE_2 == self.RS_RAREFACTION:
            
            return self.RS_EvaluateSolution__R1R2
        
        if self.RS_WAVE_1 == self.RS_SHOCK and \
           self.RS_WAVE_2 == self.RS_RAREFACTION:
            
            return self.RS_EvaluateSolution__S1R2
        
        if self.RS_WAVE_1 == self.RS_RAREFACTION and \
           self.RS_WAVE_2 == self.RS_SHOCK:
            
            return self.RS_EvaluateSolution__R1S2

        return self.RS_EvaluateSolution__S1S2

    def RS_EvaluateSolution__R1R2(self, xi):
        R1_profile = self.Compute_R1_Profile(self.RS_UL, xi)
        R2_profile = self.Compute_R2_Profile(self.RS_UR, xi)
        
        UL_domain = (xi <= self.RS_I1[0])
        R1_domain = (xi > self.RS_I1[0])*(xi <= self.RS_I1[1])
        UI_domain = (xi > self.RS_I1[1])*(xi <= self.RS_I2[0])
        R2_domain = (xi > self.RS_I2[0])*(xi <= self.RS_I2[1])
        UR_domain = (xi > self.RS_I2[1])
        
        res = np.zeros((len(xi), 2))
        res = res + UL_domain[:,np.newaxis]*self.RS_UL[np.newaxis]
        res = res + R1_domain[:,np.newaxis]*R1_profile
        res = res + UI_domain[:,np.newaxis]*self.RS_UI[np.newaxis]
        res = res + R2_domain[:,np.newaxis]*R2_profile
        res = res + UR_domain[:,np.newaxis]*self.RS_UR[np.newaxis]
        

        return res

    def RS_EvaluateSolution__R1S2(self, xi):
        R1_profile = self.Compute_R1_Profile(self.RS_UL, xi)
        
        UL_domain = (xi <= self.RS_I1[0])
        R1_domain = (xi > self.RS_I1[0])*(xi <= self.RS_I1[1])
        UI_domain = (xi > self.RS_I1[1])*(xi <= self.RS_I2)
        UR_domain = (xi > self.RS_I2)
        
        res = np.zeros((len(xi), 2))
        res = res + UL_domain[:,np.newaxis]*self.RS_UL[np.newaxis]
        res = res + R1_domain[:,np.newaxis]*R1_profile
        res = res + UI_domain[:,np.newaxis]*self.RS_UI[np.newaxis]
        res = res + UR_domain[:,np.newaxis]*self.RS_UR[np.newaxis]

        return res

    def RS_EvaluateSolution__S1R2(self, xi):
        R2_profile = self.Compute_R2_Profile(self.RS_UR, xi)
        
        UL_domain = (xi <= self.RS_I1)
        UI_domain = (xi > self.RS_I1)*(xi <= self.RS_I2[0])
        R2_domain = (xi > self.RS_I2[0])*(xi <= self.RS_I2[1])
        UR_domain = (xi > self.RS_I2[1])
        
        res = np.zeros((len(xi), 2))
        res = res + UL_domain[:,np.newaxis]*self.RS_UL[np.newaxis]
        res = res + UI_domain[:,np.newaxis]*self.RS_UI[np.newaxis]
        res = res + R2_domain[:,np.newaxis]*R2_profile
        res = res + UR_domain[:,np.newaxis]*self.RS_UR[np.newaxis]

        return res

    def RS_EvaluateSolution__S1S2(self, xi):
        UL_domain = (xi <= self.RS_I1)
        UI_domain = (xi > self.RS_I1)*(xi <= self.RS_I2)
        UR_domain = (xi > self.RS_I2)
        
        res = np.zeros((len(xi), 2))
        res = res + UL_domain[:,np.newaxis]*self.RS_UL[np.newaxis]
        res = res + UI_domain[:,np.newaxis]*self.RS_UI[np.newaxis]
        res = res + UR_domain[:,np.newaxis]*self.RS_UR[np.newaxis]

        return res
                       
    def rarefaction_1(self, hI, hL, vL):
        g = self.GetGravityConstant()
        return vL + 2*(np.sqrt(g*hL) - np.sqrt(g*hI))

    def rarefaction_2(self, hI, hR, vR):
        g = self.GetGravityConstant()
        return vR - 2*(np.sqrt(g*hR) - np.sqrt(g*hI))

    def shock_1(self, hI, hL, vL):
        g = self.GetGravityConstant()
        return vL - np.sqrt(.5*g*(hL/hI - hI/hL)*(hL - hI))

    def shock_2(self, hI, hR, vR):
        g = self.GetGravityConstant()
        return vR + np.sqrt(.5*g*(hR/hI - hI/hR)*(hR - hI))

    def wave_1(self, hI, hL, vL):
        if hI <= hL:
            return self.rarefaction_1(hI, hL, vL)
        return self.shock_1(hI, hL, vL)

    def wave_2(self, hI, hR, vR):
        if hI <= hR:
            return self.rarefaction_2(hI, hR, vR)
        return self.shock_2(hI, hR, vR)
    
    def LocusIntersection(self, hI, hL, vL, hR, vR):
        return self.wave_1(hI, hL, vL) - self.wave_2(hI, hR, vR)

    def GetIntermediateHeight(self, UL, UR, h0):
        hL = UL[0]
        vL = self.ComputeVelocity(UL[np.newaxis])
        hR = UR[0]
        vR = self.ComputeVelocity(UR[np.newaxis])
        def func__(h):
            return self.LocusIntersection(h, hL, vL, hR, vR)
        return fsolve(func__, h0)

    def GetIntermediateState(self, UL, UR, h0):
        hI = self.GetIntermediateHeight(UL, UR, h0)
        hL = UL[0]
        vL = self.ComputeVelocity(UL[np.newaxis])
        qI = hI*self.wave_1(hI, hL, vL)
        return np.array([hI, qI])

    def Is_R1(self, UL, UI):
        return (UI[0] <= UL[0])

    def Is_R2(self, UI, UR):
        return (UI[0] <= UR[0])

    def Compute_S1_Speed(self, UL, UI):
        return (UL[1] - UI[1])/(UL[0] - UI[0])

    def Compute_S2_Speed(self, UI, UR):
        return (UI[1] - UR[1])/(UI[0] - UR[0])

    def Compute_R1_Interval(self, UL, UI):
        eigL = self.ComputeEigenvalues(UL[np.newaxis,:], 0)
        eigI = self.ComputeEigenvalues(UI[np.newaxis,:], 0)
        return np.array([eigL[0,0], eigI[0,0]])

    def Compute_R2_Interval(self, UI, UR):
        eigI = self.ComputeEigenvalues(UI[np.newaxis,:], 0)
        eigR = self.ComputeEigenvalues(UR[np.newaxis,:], 0)
        return np.array([eigI[0,1], eigR[0,1]])
    
    def Compute_R1_Profile(self, UL, xi):
        hL = UL[0]
        vL = self.ComputeVelocity(UL[np.newaxis])
        g = self.GetGravityConstant()
        hStar = \
                (-1./(3*np.sqrt(g))*(xi - vL) + 2./3*np.sqrt(hL)) * \
                (-1./(3*np.sqrt(g))*(xi - vL) + 2./3*np.sqrt(hL))
        qStar = hStar*(2./3*(xi + np.sqrt(g*hL)) + 1./3*vL)

        return np.stack((hStar, qStar), 1) #Stack?

    def Compute_R2_Profile(self, UR, xi):
        hR = UR[0]
        vR = self.ComputeVelocity(UR[np.newaxis])
        g = self.GetGravityConstant()
        hStar = \
                (1./(3*np.sqrt(g))*(xi - vR) + 2./3*np.sqrt(hR)) * \
                (1./(3*np.sqrt(g))*(xi - vR) + 2./3*np.sqrt(hR))
        qStar = hStar*(2./3*(xi - np.sqrt(g*hR)) + 1./3*vR)

        return np.stack((hStar, qStar), 1) #Stack?
