import numpy as np

class Mesh1D(object):
    """Handles uniform and non-uniform 1 dimensionnal meshes"""

    UNIFORM_MESH     = 0
    NONUNIFORM_MESH  = 1

    def __init__(self):
        #Public attributes
        self.DomainBounds = None
        self.NumberOfGhosts = 2
        self.NumberOfCells = None
        self.NumberOfVertices = None
        self.VerticesArray = None
        self.CellCentersArray = None
        self.CellDxArray = None
        self.CellDataArray = None
        self.CellDataSize = 1
        self.BathymetryArray = None

        #Private attributes
        self._MeshType = None
        self._NumberOfCells = None
        self._ListOfVertices = None
        self._GhostsIncluded = False

    def __str__(self):
        string = \
        "Mesh1D instance:\n" \
        "    Bounds: [{}, {}]\n" \
        "    Number of cells: {}\n" \
        "    Number of interior cells: {}\n" \
        "    Number of ghosts: {}\n" \
        "    Data array size: {}".format(
            self.DomainBounds[0],
            self.DomainBounds[1],
            self.NumberOfCells,
            self.NumberOfCells - self.NumberOfGhosts,
            self.NumberOfGhosts,
            self.CellDataArray.shape
        )
        return string

    def SetDomainBounds(self, xMin, xMax):
        self.DomainBounds = [xMin, xMax]

    def GetDomainBounds(self):
        return self.DomainBounds[:]

    def GetNumberOfCells(self, ghosts=True):
        if ghosts is True:
            return self.NumberOfCells
        else:
            return self._NumberOfCells

    def GetNumberOfVertices(self, ghosts=True):
        if ghosts is True:
            return self.NumberOfVertices
        else:
            return self._NumberOfCells + 1

    def GetDx(self):
        return self.CellDxArray

    def GetVerticesArray(self):
        return self.VerticesArray

    def GetCellCentersArray(self):
        return self.CellCentersArray

    def SetNumberOfGhosts(self, numberOfGhosts):
        if 0 >= numberOfGhosts:
            raise ValueError("Number of ghosts should be a strictly positive integer.")
        if 1 == numberOfGhosts%2:
            raise ValueError("Number of ghost cells should be even.")
        self.NumberOfGhosts = numberOfGhosts

    def GetNumberOfGhosts(self):
        return self.NumberOfGhosts

    def SetUniformMesh(self, numberOfCells):
        self._MeshType = self.UNIFORM_MESH
        self._NumberOfCells = numberOfCells

    def SetNonuniformMesh(self, listOfVertices, ghosts=False):
        """
        Set the mesh to a nonuniform grid whose vertices are specified in
        left to right order
        """
        self._MeshType = self.NONUNIFORM_MESH
        self._ListOfVertices = np.array(listOfVertices)
        self._GhostsIncluded = ghosts

    def SetCellDataSize(self, cellDataSize):
        """Set the number of scalars to store per cell"""
        self.CellDataSize = cellDataSize

    def GetCellDataSize(self):
        return self.CellDataSize

    def AllocateCellData(self):
        self.CellDataArray = np.zeros((self.NumberOfCells, self.CellDataSize))

    def SetCellData(self, data, ghosts=True):
        if ghosts is True:
            self.CellDataArray[:,:] = data
        else:
            i = self.NumberOfGhosts//2
            j = self.NumberOfCells - i
            self.CellDataArray[i:j,:] = data

    def GetCellData(self, ghosts=True, copy=False):
        if ghosts is True:
            if copy is False:
                return self.CellDataArray
            else:
                return self.CellDataArray[:,:]
        else:
            i = self.NumberOfGhosts//2
            j = self.NumberOfCells - i
            return self.CellDataArray[i:j,:]

    def GetInteriorCellsRange(self):
        i = self.NumberOfGhosts//2
        j = self.NumberOfCells - i
        return np.arange(i, j, dtype=np.int64)

    def GetInteriorVerticesRange(self):
        i = self.NumberOfGhosts//2
        j = self.NumberOfCells - i + 1
        return np.arange(i, j, dtype=np.int64)

    def GetBorderCellsID(self):
        i = self.NumberOfGhosts//2
        j = self.NumberOfCells - i - 1
        return i, j

    def GetBorderVerticesID(self):
        i = self.NumberOfGhosts//2
        j = self.NumberOfCells - i
        return i, j

    def GenerateMesh(self):
        if self.UNIFORM_MESH == self._MeshType:
            self.__GenerateUniformMesh()
        elif self.NONUNIFORM_MESH == self._MeshType:
            self.__GenerateNonuniformMesh()
        else:
            raise AttributeError("Bad initialization for Mesh1D.")

    def __GenerateUniformMesh(self):
        self.NumberOfCells = self._NumberOfCells + self.NumberOfGhosts
        self.NumberOfVertices = self.NumberOfCells + 1
        
        xMin = self.DomainBounds[0]
        xMax = self.DomainBounds[1]
        dx = float(xMax - xMin)/self._NumberOfCells
        # self.VerticesArray = np.arange(
        #     xMin - .5*dx*self.NumberOfGhosts,
        #     xMax + .5*dx*self.NumberOfGhosts, #TODO: + dx ???
        #     dx
        # )
        self.VerticesArray = np.linspace(
            xMin - .5*dx*self.NumberOfGhosts,
            xMax + .5*dx*self.NumberOfGhosts,
            self.NumberOfVertices
        )
        
        self.CellCentersArray = \
            .5*(self.VerticesArray[0:-1] + self.VerticesArray[1:])
        self.CellDxArray = \
            self.VerticesArray[1:] - self.VerticesArray[0:-1]

    def __GenerateNonuniformMesh(self):
        if self._GhostsIncluded is True:
            nVert = self._ListOfVertices.shape[0]
            self.NumberOfVertices = nVert
            self.NumberOfCells = nVert - 1
            self.VerticesArray = np.empty(nVert)
            self.VerticesArray[:] = self._ListOfVertices[:]
            
            self.CellCentersArray = \
                .5*(self.VerticesArray[0:-1] + self.VerticesArray[1:])
            self.CellDxArray = \
                self.VerticesArray[1:] - self.VerticesArray[0:-1]
            
            i = int(self.NumberOfGhosts/2)
            j = nVert - i - 1
            self.DomainBounds = [
                self.VerticesArray[i],
                self.VerticesArray[j]
            ]
            
        else:
            nVert = self._ListOfVertices.shape[0] + self.NumberOfGhosts
            self.NumberOfVertices = nVert
            self.NumberOfCells = nVert - 1
            self.VerticesArray = np.empty(nVert)
            i = int(self.NumberOfGhosts/2)
            j = nVert - i
            self.VerticesArray[i:j] = self._ListOfVertices[:]
            
            x0 = self.VerticesArray[i]
            x1 = self.VerticesArray[i+1]
            dx = x1 - x0
            self.VerticesArray[:i] = np.arange(x0 - i*dx, x0, dx)
            
            x0 = self.VerticesArray[j-1]
            x1 = self.VerticesArray[j]
            dx = x1 - x0
            self.VerticesArray[j:] = np.arange(x1, x1 + i*dx, dx)

            self.CellCentersArray = \
                .5*(self.VerticesArray[0:-1] + self.VerticesArray[1:])
            self.CellDxArray = \
                self.VerticesArray[1:] - self.VerticesArray[0:-1]

            self.DomainBounds = [
                self._ListOfVertices[0],
                self._ListOfVertices[-1]
            ]

    def SetBathymetryArray(self, bathymetryArray):
        self.BathymetryArray = bathymetryArray

    def GetBathymetryArray(self):
        return self.BathymetryArray
