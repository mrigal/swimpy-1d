import numpy as np
from .SaintVenant1D import *

class LAR_splitting(SaintVenant1D):

    def ComputeFastFlux(self, U, z): #_
        g = self.GetGravityConstant()
        h = U[:,0]
        q = U[:,1]
        
        fH = q
        fQ = -g*z*(h + .5*z) #+ .5*g*h*z
        
        return np.stack((fH, fQ), axis=1)

    def ComputeSlowFlux(self, U, z): #_
        g = self.GetGravityConstant()
        h = U[:,0]
        q = U[:,1]
        v = self.ComputeVelocity(U)

        fH = np.zeros(q.shape)
        # fQ = h*v*v + .5*g*h*h + g*z*(h + .5*z)
        fQ = h*v*v + .5*g*(h+z)*(h+z)
        # fQ = h*v*v

        return np.stack((fH, fQ), axis=1)

    def ComputeFastEigenvalues(self, _, z): # U, z
        g = self.GetGravityConstant()
        E1 = -np.sqrt(-g*z)
        E2 =  np.sqrt(-g*z)
        return np.stack((E1, E2), axis=1)

    def ComputeOuterFastEigenvalues(self, U, z):
        fastEigenvalues = self.ComputeFastEigenvalues(U, z)
        return np.amin(fastEigenvalues, axis=1), np.amax(fastEigenvalues, axis=1)

    def ComputeSlowEigenvalues(self, U, _): #z
        v = self.ComputeVelocity(U)
        nullVect = np.zeros(v.shape)
        return np.stack((nullVect, 2*v), axis=1)

    def ComputeOuterSlowEigenvalues(self, U, z):
        slowEigenvalues = self.ComputeSlowEigenvalues(U, z)
        return np.amin(slowEigenvalues, axis=1), np.amax(slowEigenvalues, axis=1)

    def ComputeSlowSourceTermContribution(self, U, z):
        return np.zeros(U.shape)
        # g = self.GetGravityConstant()
        # h = U[:,0]
        # res = np.zeros(U.shape)
        # res[:,1] = -(.5*g*z*z + .5*g*h*h + g*z*h)
        # return res

    def ComputeFastSourceTermContribution(self, U, z):
        g = self.GetGravityConstant()
        res = np.zeros(U.shape)
        res[:,1] = -g*U[:,0]*z
        return res

    def SwitchToSlowFlux(self):
        self.ComputeFlux = self.ComputeSlowFlux
        self.ComputeEigenvalues = self.ComputeSlowEigenvalues
        self.ComputeOuterEigenvalues = self.ComputeOuterSlowEigenvalues
        self.ComputeSourceTermContribution = \
            self.ComputeSlowSourceTermContribution

    def SwitchToFastFlux(self):
        self.ComputeFlux = self.ComputeFastFlux
        self.ComputeEigenvalues = self.ComputeFastEigenvalues
        self.ComputeOuterEigenvalues = self.ComputeOuterFastEigenvalues
        self.ComputeSourceTermContribution = \
            self.ComputeFastSourceTermContribution
