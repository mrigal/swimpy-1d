#! /usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np

# Load data
ImpKinetic_array    = np.loadtxt("Kinetic_convergence.txt")
ImpKinetic_N_sizes  = ImpKinetic_array[:,0] #.astype(int)
ImpKinetic_L2_err_H = ImpKinetic_array[:,1]
ImpKinetic_L2_err_Q = ImpKinetic_array[:,2]
ImpKinetic_times    = ImpKinetic_array[:,3]

ImpKinetic_10xDt_array    = np.loadtxt("Kinetic_10xDt_convergence.txt")
ImpKinetic_10xDt_N_sizes  = ImpKinetic_10xDt_array[:,0] #.astype(int)
ImpKinetic_10xDt_L2_err_H = ImpKinetic_10xDt_array[:,1]
ImpKinetic_10xDt_L2_err_Q = ImpKinetic_10xDt_array[:,2]
ImpKinetic_10xDt_times    = ImpKinetic_10xDt_array[:,3]

IterKinetic_array    = np.loadtxt("IterKinetic_convergence.txt")
IterKinetic_N_sizes  = IterKinetic_array[:,0] #.astype(int)
IterKinetic_L2_err_H = IterKinetic_array[:,1]
IterKinetic_L2_err_Q = IterKinetic_array[:,2]
IterKinetic_times    = IterKinetic_array[:,3]

HLL_array    = np.loadtxt("HLL_convergence.txt")
HLL_N_sizes  = HLL_array[:,0] #.astype(int)
HLL_L2_err_H = HLL_array[:,1]
HLL_L2_err_Q = HLL_array[:,2]
HLL_times    = HLL_array[:,3]

KinExp_array    = np.loadtxt("KinExp_convergence.txt")
KinExp_N_sizes  = KinExp_array[:,0] #.astype(int)
KinExp_L2_err_H = KinExp_array[:,1]
KinExp_L2_err_Q = KinExp_array[:,2]
KinExp_times    = KinExp_array[:,3]


Op25 = ImpKinetic_N_sizes**(-.25)
Op5  = ImpKinetic_N_sizes**(-.5)
# halfOrderRef = ImpKinetic_N_sizes**(-.5)
# firstOrderRef = ImpKinetic_N_sizes**(-1)/2
# secondOrderRef = ImpKinetic_N_sizes**(-2)*6

errorBounds = np.array([np.nanmin(ImpKinetic_L2_err_H),
                        np.nanmax(ImpKinetic_L2_err_H)])

figure = plt.figure(figsize=(9, 3.5))
plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.15,
                    wspace=0.25, hspace=0.)
axis = []
lines = []

#Free surface and bathymetry on the same subfigure
axis.append(figure.add_subplot(121))
axis[-1].axis([6, 300, 2*10e-3, 3*10e-2])
axis[-1].set_title("Experimental order of convergence")
lines.append(axis[-1].loglog([], [], 'k--', [], [], 'k-', [], [], 'go-', [], [], 'ys-', [], [], 'cd-', [], [], 'bv-', [], [], 'v--'))
# lines[-1][5].set_label("Iterative kinetic $ 10\Delta t $")
lines[-1][3].set_label("Explicit kinetic")
lines[-1][4].set_label("Iterative kinetic")
lines[-1][5].set_label("Implicit kinetic")
lines[-1][2].set_label("HLL")
lines[-1][0].set_label("O1/4")
lines[-1][1].set_label("O1/2")
lines[-1][5].set_color("#a6cee3") # #fb9a99
lines[-1][4].set_color("#1f78b4")
lines[-1][3].set_color('#b2df8a')
lines[-1][2].set_color("#33a02c")
axis[-1].set_xlabel("Number of cells")
axis[-1].set_ylabel("$ |\!| h - h_{exact} |\!|_{L^2} $")
axis[-1].legend(ncol=1, loc="best")
axis[-1].grid()

#L2 error per CPU time in a separate subfigure
axis.append(figure.add_subplot(122))
axis[-1].axis([2.2*10e-2, 3.5*10e-3, 10e-4, .5*10e1])
# axis[-1].set_title("CPU time = f(L2 error)")
axis[-1].set_title("CPU time = $f\, (|\!| h - h_{exact} |\!|_{L^2})$")
lines.append(axis[-1].loglog([], [], 'go-', [], [], 'ys-', [], [], 'cd-', [], [], 'bv-', [], [], 'ro-'))
# lines[-1][4].set_label("Iterative kinetic $ 10\Delta t $")
lines[-1][1].set_label("Explicit kinetic")
lines[-1][2].set_label("Iterative kinetic")
lines[-1][3].set_label("Implicit kinetic")
lines[-1][0].set_label("HLL")
lines[-1][3].set_color("#a6cee3") # #fb9a99
lines[-1][2].set_color("#1f78b4")
lines[-1][1].set_color('#b2df8a')
lines[-1][0].set_color("#33a02c")
# lines[-1][3].set_color("#386cb0") ##ffff99
# lines[-1][2].set_color("#fdc086")
# lines[-1][1].set_color('#beaed4')
# lines[-1][0].set_color("#7fc97f")
# lines[-1][4].set_label("X^2")
# lines[-1][5].set_label("X^1")
axis[-1].set_xlabel("$ |\!| h - h_{exact} |\!|_{L^2} $")
axis[-1].set_ylabel("CPU time (s)")
axis[-1].legend(ncol=1, loc="upper left")
axis[-1].grid()

#Plot data
# lines[0][6].set_data(ImpKinetic_10xDt_N_sizes, ImpKinetic_10xDt_L2_err_H)
lines[0][3].set_data(KinExp_N_sizes, KinExp_L2_err_H)
lines[0][4].set_data(IterKinetic_N_sizes, IterKinetic_L2_err_H)
lines[0][5].set_data(ImpKinetic_N_sizes, ImpKinetic_L2_err_H)
lines[0][2].set_data(HLL_N_sizes, HLL_L2_err_H)
lines[0][0].set_data(ImpKinetic_N_sizes, Op25/2.5)
lines[0][1].set_data(ImpKinetic_N_sizes, Op5/2.5)

# lines[1][4].set_data(ImpKinetic_10xDt_L2_err_H, ImpKinetic_10xDt_times)
lines[1][1].set_data(KinExp_L2_err_H, KinExp_times)
lines[1][2].set_data(IterKinetic_L2_err_H, IterKinetic_times)
lines[1][3].set_data(ImpKinetic_L2_err_H, ImpKinetic_times)
lines[1][0].set_data(HLL_L2_err_H, HLL_times)
# lines[1][1].set_data(errorBounds, 10e-2*errorBounds**(-2))
# lines[1][2].set_data(errorBounds, 10e-4*errorBounds**(-1))
# figure.canvas.draw()
figure.canvas.flush_events()
# figure.canvas.draw()
# plt.draw()
figure.savefig("RiemannPB_comparison_nobat.pdf", format="pdf", dpi=300)
plt.show()

