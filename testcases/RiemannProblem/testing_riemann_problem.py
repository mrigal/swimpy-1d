#! /usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
from lib import SaintVenant1D, Mesh1D, OutputManager
from lib.numerical_schemes import *
from lib.time_integrators import CustomTimeIntegrator, ExplicitRungeKutta
from lib.reconstruction_operators import *

UL = np.array([2, 0.5])
UR = np.array([1, 0.5])

def bathymetry(x):
    return -1*np.ones(x.shape)

def initCond_h(x):
    return UL[0]*(x < 0) + UR[0]*(x >= 0)

def initCond_q(x):
    return UL[1]*(x < 0) + UR[1]*(x >= 0)

g = 100
cl = SaintVenant1D()
cl.SetGravityConstant(g)
cl.SetBathymetry(bathymetry)

cl.RS_ComputeSolution(UL, UR)
RS_solution = cl.RS_GetSolution()

def boundaryConditionsType(X, time):
    raise NotImplementedError

def boundaryConditionsValue(X, time):
    # nComponents = SaintVenant1D.SYSTEM_SIZE
    # values = np.empty(nComponents)
    #Enforce water height given by initial condition to preserve lake at rest
    # value = initCond_h(X)
    # return value
    
    if time > 0:
        Xi = X/time
    else:
        Xi = 1E9*(X >= 0).astype(float) - 1E9*(X < 0).astype(float)
    Xi = np.array([Xi])
    W_exact = RS_solution(Xi)
    return W_exact[:,0]

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    # z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    X_ = X[ic]
    # z_ = z[ic]
    time = om.GetNextOutputTime()
    if time > 0:
        Xi = X_/time
    else:
        Xi = 1E9*(X_ >= 0).astype(float) - 1E9*(X_ < 0).astype(float)
    W_exact = RS_solution(Xi)
    line[0][0].set_data(X_, W_exact[:,0])
    line[0][1].set_data(X_, W[ic,0])
    line[1][0].set_data(X_, W_exact[:,1])
    line[1][1].set_data(X_, W[ic,1])
    # figure.canvas.draw()
    figure.canvas.flush_events()
    # plt.draw()
    plt.savefig('testfig_small_Dt')

def SetExplicitHLLScheme(initialTime, finalTime, CFL_number):
    ti = ExplicitRungeKutta()
    A = np.array([[0.]]) #Partial update weights
    b = np.array([1.])         #Final update weights
    c = np.array([0.])           #Partial time stepping
    ti.SetButcherTableau(A, b, c, name="ForwardEuler")
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    scheme = HLL()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    ro = NoReconstruction()
    # ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)

    # scheme.SetPeriodicBNDC()
    scheme.SetWeaklyPrescribedSubsonicRiemannBNDC_H(
        boundaryConditionsType,
        boundaryConditionsValue
    )

    return scheme

def SetExplicitKineticScheme(initialTime, finalTime, CFL_number):
    ti = ExplicitRungeKutta()
    A = np.array([[0.]]) #Partial update weights
    b = np.array([1.])         #Final update weights
    c = np.array([0.])           #Partial time stepping
    ti.SetButcherTableau(A, b, c, name="ForwardEuler")
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    scheme = ExplicitKineticScheme()
    scheme.SetMaxwellian_HalfDisk()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    ro = NoReconstruction()
    # ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)

    # scheme.SetPeriodicBNDC()
    scheme.SetWeaklyPrescribedSubsonicRiemannBNDC_H(
        boundaryConditionsType,
        boundaryConditionsValue
    )

    return scheme

def SetImplicitKineticScheme(initialTime, finalTime, CFL_number):
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    scheme = ImplicitKineticScheme()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    
    # scheme.SetPeriodicBNDC()
    scheme.SetWeaklyPrescribedSubsonicRiemannBNDC_H(
        boundaryConditionsType,
        boundaryConditionsValue
    )

    return scheme

def SetImplicitKineticScheme_10xDt(initialTime, finalTime, CFL_number):
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    scheme = ImplicitKineticScheme()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(10*CFL_number)
    scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    
    # scheme.SetPeriodicBNDC()
    scheme.SetWeaklyPrescribedSubsonicRiemannBNDC_H(
        boundaryConditionsType,
        boundaryConditionsValue
    )

    return scheme

def SetIterKineticScheme(initialTime, finalTime, CFL_number):
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    scheme = IterativeKineticScheme()
    # scheme.SetMaxwellian_Index()
    scheme.SetMaxwellian_HalfDisk()
    scheme.SetAlpha(1)
    scheme.SetStoppingCriteria(scheme.TOLERANCE)
    # scheme.SetStoppingCriteria(scheme.ENTROPY); scheme.SetAlpha(0)
    # scheme.SetStoppingCriteria(scheme.TOLERANCE_AND_ENTROPY)
    scheme.SetMaxIter(200)
    scheme.SetTolerance(10**(-9))
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    ro = NoReconstruction()
    # ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)

    # scheme.SetPeriodicBNDC()
    scheme.SetWeaklyPrescribedSubsonicRiemannBNDC_H(
        boundaryConditionsType,
        boundaryConditionsValue
    )

    return scheme


if "__main__" == __name__:

    #Setting the mesh
    xMin = -1
    xMax = 1
    numberOfCells = 50 #30 #50 #100
    numberOfGhosts = 4 #Should be even, required for boundary conditions

    #Setting the time integrator
    initialTime = 0
    finalTime = .025

    #Setting the output manager
    om = OutputManager()
    # numberOutputs = 20
    # outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    # om.SetOutputTimes(outputTimes)
    om.SetOutputTimes([finalTime])

    plt.ion()
    figure = plt.figure(figsize=(9, 3.5))
    plt.subplots_adjust(left=0.075, right=0.925, top=0.9, bottom=0.15)
    axis = []
    lines = []
    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(121))
    axis[-1].axis([-1, 1, 0.75, 2.25])
    axis[-1].set_title("Free surface")
    lines.append(axis[-1].plot([], [], 'g--', [], [], 'b:'))
    lines[-1][0].set_label("Analytic")
    lines[-1][1].set_label("Kinetic")
    axis[-1].legend()
    plt.grid()
    #Discharge in a separate subfigure
    axis.append(figure.add_subplot(122))
    axis[-1].axis([-1, 1, cl.RS_UL[1]*0.5, cl.RS_UI[1]*1.25])
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'g--', [], [], 'b:'))
    lines[-1][0].set_label("Analytic")
    lines[-1][1].set_label("Kinetic")
    axis[-1].legend()
    plt.grid()
    #Uploading features into output manager
    om.SetRealTimePlotFigure(figure)
    om.SetRealTimePlotAxis(axis)
    om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    #Setting the scheme with boundary conditions
    CFL_number = .45

    N_sizes = 6
    N_start = 8

    # N_sizes = 1
    # N_start = 256
    
    for i in range(N_sizes):
        numberOfCells = N_start * 2**(i)
        mesh = Mesh1D()
        mesh.SetDomainBounds(xMin, xMax)
        mesh.SetUniformMesh(numberOfCells)
        mesh.SetNumberOfGhosts(numberOfGhosts)
        mesh.GenerateMesh()

        scheme = SetExplicitHLLScheme(initialTime, finalTime, CFL_number)
        file_name = "HLL_convergence.txt"

        # scheme = SetImplicitKineticScheme(initialTime, finalTime, CFL_number)
        # file_name = "Kinetic_convergence.txt"

        # scheme = SetImplicitKineticScheme_10xDt(initialTime, finalTime, CFL_number)
        # file_name = "Kinetic_10xDt_convergence.txt"

        # scheme = SetIterKineticScheme(initialTime, finalTime, CFL_number)
        # file_name = "IterKinetic_convergence.txt"

        # scheme = SetExplicitKineticScheme(initialTime, finalTime, CFL_number)
        # file_name = "KinExp_convergence.txt"


        #om.ActivatePlotRealTime()
        scheme.Solve(cl, mesh, initialCondition, om, silent=True)
        #om.DeactivatePlotRealTime()

        CPU_time = om.GetCPUTime()

        Dx = mesh.GetDx()
        time = scheme.ti.GetCurrentTime()
        X = mesh.GetCellCentersArray()
        ic = mesh.GetInteriorCellsRange()
        X_ = X[ic]
        Xi = X_/time

        W = mesh.GetCellData()
        W_exact = RS_solution(Xi)

        L2_error_H = np.sqrt(np.sum(Dx[ic]*(W[ic,0] - W_exact[:,0])**2))
        L2_error_Q = np.sqrt(np.sum(Dx[ic]*(W[ic,1] - W_exact[:,1])**2))

        with open(file_name, "a") as output_file:
            output_file.write("{} {} {} {}\n".format(
                numberOfCells, L2_error_H, L2_error_Q, CPU_time)
            )
