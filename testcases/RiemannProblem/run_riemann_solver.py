#! /usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
from lib import SaintVenant1D

def bathymetry(x):
    return -1*np.ones(x.shape)

UL = np.array([1, -0.5])
UR = np.array([1, 0.5])

UL = np.array([2, 4])
UR = np.array([1, 2])

g = 9.81
cl = SaintVenant1D()
cl.SetGravityConstant(g)
cl.SetBathymetry(bathymetry)

cl.RS_ComputeSolution(UL, UR)
sol = cl.RS_GetSolution()

X = np.linspace(-1, 1, 200)
time = .125
Xi = X/time

Y = sol(Xi)

plt.plot(X, Y)
plt.show()
