#! /usr/bin/python2

# THACKLER TESTCASE IN 1D

import numpy as np
import matplotlib.pyplot as plt
from lib import SaintVenant1D, Mesh1D, OutputManager
from lib.numerical_schemes import *
from lib.time_integrators import CustomTimeIntegrator, ExplicitRungeKutta
from lib.reconstruction_operators import *


g = 10
h0 = 0.5
a = 1
L = 4
B = .5*np.sqrt(2*g*h0)/a

def X_1(time):
    return -.5*np.cos(2*B*time) - a + .5*L

def X_2(time):
    return -.5*np.cos(2*B*time) + a + .5*L

def solution_h(X, time):
    wetArea = (X >= X_1(time))*(X <= X_2(time))
    return -h0*(((X - .5*L)/a + .5/a*np.cos(2*B*time))**2 - 1)*wetArea

def solution_u(X, time):
    wetArea = (X >= X_1(time))*(X <= X_2(time))
    return B*np.sin(2*B*time)*wetArea

def solution_q(X, time):
    return solution_h(X, time)*solution_u(X, time)

def initCond_h(X):
    return solution_h(X, 0)

def initCond_q(X):
    return solution_q(X, 0)

def bathymetry(X):
    return h0*((X - .5*L)*(X - .5*L)/a/a - 1)

def mixedBoundaryConditionsType(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    types = np.empty(nComponents)
    if X == 0:
        types[0] = Rusanov.NEUMANN_CONDITION
        types[1] = Rusanov.DIRICHLET_CONDITION#*(time < 0.1) + Rusanov.NEUMANN_CONDITION*(time > 0.2)
    elif X == 1:
        types[0] = Rusanov.DIRICHLET_CONDITION
        types[1] = Rusanov.NEUMANN_CONDITION
    else:
        raise ValueError("Unexpected boundary position.")
    return types

def mixedBoundaryConditionsValue(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    values = np.empty(nComponents)
    if X == 0:
        values[0] = 0.
        values[1] = 0*5*(1 - (time>0.01)*(1-np.exp(-20*(time-0.01))))
    elif X == 1:
        values[0] = 0.
        values[1] = 0.
    else:
        raise ValueError("Unexpected boundary position.")
    return values

def FileDumper(om, cl, W, mesh, scheme):
    ic = mesh.GetInteriorCellsRange()
    z = mesh.GetBathymetryArray()[ic]
    X = mesh.GetCellCentersArray()[ic]
    h = W[ic,0]
    q = W[ic,1]
    array = np.stack((X, h + z, q, z), axis=1)
    fileID = om.OpenDataFile(binary=False, mode="w", ext="txt")
    # fileID.write("# Header\n")
    # fileID.write("# Comment: Position, Free surface, Discharge, Bathymetry\n")
    np.savetxt(fileID, array, fmt="%g")
    fileID.close()

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    time = om.GetNextOutputTime()
    H_exact = solution_h(X, time)
    Q_exact = solution_q(X, time)
    line[0][0].set_data(X[ic], H_exact[ic] + z[ic])
    line[0][1].set_data(X[ic], W[ic,0] + z[ic])
    line[0][2].set_data(X[ic], z[ic])
    line[1][0].set_data(X[ic], Q_exact[ic])
    line[1][1].set_data(X[ic], W[ic,1])
    time = om.GetNextOutputTime()
    finalTime = om.GetFinalOutputTime()
    figure.suptitle("Thacker testcase\nTime={:.5f} / {}".format(time, finalTime)) # vs Heun-Roe-HydroRec-O2
    # figure.canvas.draw()
    figure.canvas.flush_events()
    # plt.draw()
    # plt.savefig("Thacker_1D_comparison.png", format="png")
    plt.show(block=True)

def SetExplicitHLLScheme(initialTime, finalTime, CFL_number):
    ti = ExplicitRungeKutta()
    A = np.array([[0.]]) #Partial update weights
    b = np.array([1.])         #Final update weights
    c = np.array([0.])           #Partial time stepping
    ti.SetButcherTableau(A, b, c, name="ForwardEuler")
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    # ti = ExplicitRungeKutta()
    # A = np.array([[0., 0.], [1., 0.]]) #Partial update weights
    # b = np.array([.5, .5])         #Final update weights
    # c = np.array([[0.], [1.]])           #Partial time stepping
    # ti.SetButcherTableau(A, b, c, name="Heun")
    # ti.SetStartingTime(initialTime)
    # ti.SetEndingTime(finalTime)
    
    scheme = HLL()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    # ro = NoReconstruction()
    ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)

    scheme.SetPeriodicBNDC()

    return scheme

def SetExplicitKineticScheme(initialTime, finalTime, CFL_number):
    ti = ExplicitRungeKutta()
    A = np.array([[0.]]) #Partial update weights
    b = np.array([1.])         #Final update weights
    c = np.array([0.])           #Partial time stepping
    ti.SetButcherTableau(A, b, c, name="ForwardEuler")
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    scheme = ExplicitKineticScheme()
    scheme.SetMaxwellian_HalfDisk()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    # ro = NoReconstruction()
    ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)

    scheme.SetPeriodicBNDC()

    return scheme

def SetIterKineticScheme_TOLERANCE_ENTROPY(initialTime, finalTime, CFL_number):
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    scheme = IterativeKineticScheme()
    # scheme.SetMaxwellian_Index()
    scheme.SetMaxwellian_HalfDisk()
    scheme.SetAlpha(1)
    # scheme.SetStoppingCriteria(scheme.TOLERANCE)
    # scheme.SetStoppingCriteria(scheme.ENTROPY); scheme.SetAlpha(0)
    scheme.SetStoppingCriteria(scheme.TOLERANCE_AND_ENTROPY)
    scheme.SetMaxIter(200)
    scheme.SetTolerance(10**(-9))
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    # ro = NoReconstruction()
    ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)

    scheme.SetPeriodicBNDC()

    return scheme

def SetIterKineticScheme_ENTROPY(initialTime, finalTime, CFL_number):
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    scheme = IterativeKineticScheme()
    # scheme.SetMaxwellian_Index()
    scheme.SetMaxwellian_HalfDisk()
    # scheme.SetAlpha(1)
    # scheme.SetStoppingCriteria(scheme.TOLERANCE)
    scheme.SetStoppingCriteria(scheme.ENTROPY); scheme.SetAlpha(0)
    # scheme.SetStoppingCriteria(scheme.TOLERANCE_AND_ENTROPY)
    scheme.SetMaxIter(200)
    scheme.SetTolerance(10**(-9))
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    # ro = NoReconstruction()
    ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)

    scheme.SetPeriodicBNDC()

    return scheme


if "__main__" == __name__:

    #Setting the conservation law and bathymetry
    cl = SaintVenant1D()
    cl.SetGravityConstant(g)
    cl.SetBathymetry(bathymetry)

    #Setting the mesh
    xMin = 0
    xMax = L
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)

    #Setting the time integrator
    initialTime = 0
    finalTime = 1

    #Setting the output manager
    om = OutputManager()
    # numberOutputs = 10*30 #200
    # numberOutputs = 20
    # outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    # om.SetOutputTimes(outputTimes)
    om.SetOutputTimes([finalTime])

    # plt.ion()
    # figure = plt.figure(figsize=(9, 4.5))
    # # figure = plt.figure(figsize=(18, 9))
    # plt.subplots_adjust(left=0.075, right=0.925, top=0.775, bottom=0.15)
    # axis = []
    # lines = []
    # #Free surface and bathymetry on the same subfigure
    # axis.append(figure.add_subplot(121))
    # axis[-1].axis([xMin, xMax, -1, 2])
    # # axis[-1].axis([0, 1, -10e-11, 10e-11])
    # axis[-1].set_title("Free surface")
    # lines.append(axis[-1].plot([], [], 'r-', [], [], 'g--', [], [], 'k-'))
    # lines[-1][0].set_label("Analytic")
    # lines[-1][1].set_label("Iterative kinetic")
    # lines[-1][2].set_label("Bathymetry")
    # axis[-1].legend()
    # plt.xlabel("X position ({} cells)".format(150))
    # plt.grid()
    # #Discharge in a separate subfigure
    # axis.append(figure.add_subplot(122))
    # axis[-1].axis([xMin, xMax, -1, 1])
    # # axis[-1].axis([0, 1, -5*10e-10, 5*10e-10])
    # axis[-1].set_title("Discharge")
    # lines.append(axis[-1].plot([], [], 'r-', [], [], 'g--'))
    # lines[-1][0].set_label("Analytic")
    # lines[-1][1].set_label("Iterative kinetic")
    # axis[-1].legend()
    # plt.xlabel("X position ({} cells)".format(150))
    # plt.grid()
    # #Uploading features into output manager
    # om.SetRealTimePlotFigure(figure)
    # om.SetRealTimePlotAxis(axis)
    # om.SetRealTimePlotLines(lines)
    # om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    ############################################################################
    #Setting the scheme with boundary conditions
    numberOfCells = 50 #500 #30 #50 #100
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()

    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    X_ = X[ic]
    # z = mesh.GetBathymetryArray()[ic]
    
    CFL_number = 1#.45

    # om.ActivatePlotRealTime()

    scheme = SetExplicitHLLScheme(initialTime, finalTime, CFL_number)
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    W_HLL = mesh.GetCellData()

    scheme = SetExplicitKineticScheme(initialTime, finalTime, CFL_number)
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    W_KinExp = mesh.GetCellData()

    scheme = SetIterKineticScheme_TOLERANCE_ENTROPY(initialTime, finalTime, CFL_number)
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    W_KinIter_TE = mesh.GetCellData()

    scheme = SetIterKineticScheme_ENTROPY(initialTime, finalTime, CFL_number)
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    W_KinIter_E = mesh.GetCellData()

    # assert(False)

    z = mesh.GetBathymetryArray()[ic]

    ############################################################################
    
    #Analytic solution
    N_exact = 256
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)
    mesh.SetUniformMesh(N_exact)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    Dx = mesh.GetDx()
    time = scheme.ti.GetCurrentTime()
    X = mesh.GetCellCentersArray()
    ic__ = mesh.GetInteriorCellsRange()
    X_exact = X[ic__]
    hE = solution_h(X_exact, time)
    qE = solution_q(X_exact, time)
    # W_exact = np.array([hE, qE])
    W_exact = np.stack((hE, qE), axis=1)

    scheme.InitializeScheme(cl, mesh, initialCondition, om)
    zE = mesh.GetBathymetryArray()[ic__]

    ############################################################################

    
    figure = plt.figure(figsize=(9, 3.5))
    plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.15,
                        wspace=0.25, hspace=0.)
    axis = []
    lines = []

    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(121))
    # axis[-1].axis([0.5, L-.5, -.55, .75])
    axis[-1].axis([0.25, L-.25, -.55, 1])
    axis[-1].set_title("Free surface elevation")
    lines.append(axis[-1].plot([], [], 'k-', [], [], 'g-', [], [], 'y:', [], [], 'c-', [], [], 'b:', [], [], 'k-'))
    # lines.append(axis[-1].plot([], [], 'k-', [], [], 'gs', [], [], 'yo', [], [], 'c.', [], [], 'bx'))
    # lines[-1][5].set_label("Iterative kinetic $ 10\Delta t $")

    lines[-1][4].set_label("IterKin (Entropy)")
    lines[-1][3].set_label("IterKin (Tol+Entr.)")
    lines[-1][2].set_label("Explicit kinetic")
    lines[-1][1].set_label("HLL")
    lines[-1][0].set_label("Analytic")
    lines[-1][5].set_linewidth("2")
    lines[-1][4].set_linewidth("2")
    lines[-1][3].set_linewidth("2")
    lines[-1][2].set_linewidth("2")
    lines[-1][1].set_linewidth("2")
    lines[-1][0].set_linewidth("2")
    lines[-1][0].set_color("#fb9a99")
    lines[-1][4].set_color("#a6cee3") # #fb9a99
    lines[-1][3].set_color("#1f78b4")
    lines[-1][2].set_color('#b2df8a')
    lines[-1][1].set_color("#33a02c")
    axis[-1].set_xlabel("X position ({} cells)".format(numberOfCells))
    axis[-1].set_ylabel("Free surface")
    axis[-1].legend(ncol=1, loc="upper left")
    axis[-1].grid()

    #Plot data
    lines[0][5].set_data(X_exact, zE)
    lines[0][4].set_data(X_, W_KinIter_E[ic,0] + z)
    lines[0][3].set_data(X_, W_KinIter_TE[ic,0] + z)
    lines[0][2].set_data(X_, W_KinExp[ic,0] + z)
    lines[0][1].set_data(X_, W_HLL[ic,0] + z)
    lines[0][0].set_data(X_exact, W_exact[:,0] + zE)

    #L2 error per CPU time in a separate subfigure
    axis.append(figure.add_subplot(122))
    # axis[-1].axis([0.5, L-0.5, -0.1, 0.9])
    axis[-1].axis([0.25, L-0.25, -0.05, 0.2])
    # axis[-1].set_title("CPU time = f(L2 error)")
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'k-', [], [], 'g-', [], [], 'y:', [], [], 'c-', [], [], 'b:'))
    # lines[-1][4].set_label("Iterative kinetic $ 10\Delta t $")
    lines[-1][3].set_label("IterKin (Tol+Entr.)")
    lines[-1][4].set_label("IterKin (Entropy)")
    lines[-1][2].set_label("Explicit kinetic")
    lines[-1][1].set_label("HLL")
    lines[-1][0].set_label("Analytic")
    lines[-1][4].set_linewidth("2")
    lines[-1][3].set_linewidth("2")
    lines[-1][2].set_linewidth("2")
    lines[-1][1].set_linewidth("2")
    lines[-1][0].set_linewidth("2")
    lines[-1][4].set_color("#a6cee3") # #fb9a99
    lines[-1][3].set_color("#1f78b4")
    lines[-1][2].set_color('#b2df8a')
    lines[-1][1].set_color("#33a02c")
    lines[-1][0].set_color("#fb9a99")
    # lines[-1][3].set_color("#386cb0") ##ffff99
    # lines[-1][2].set_color("#fdc086")
    # lines[-1][1].set_color('#beaed4')
    # lines[-1][0].set_color("#7fc97f")
    # lines[-1][4].set_label("X^2")
    # lines[-1][5].set_label("X^1")
    axis[-1].set_xlabel("X position ({} cells)".format(numberOfCells))
    axis[-1].set_ylabel("Discharge")
    # axis[-1].legend(ncol=1, loc="best")
    axis[-1].grid()
    
    # lines[1][4].set_data(ImpKinetic_10xDt_L2_err_H, ImpKinetic_10xDt_times)
    lines[1][4].set_data(X_, W_KinIter_E[ic,1])
    lines[1][3].set_data(X_, W_KinIter_TE[ic,1])
    lines[1][2].set_data(X_, W_KinExp[ic,1])
    lines[1][1].set_data(X_, W_HLL[ic,1])
    lines[1][0].set_data(X_exact, W_exact[:,1])
    # lines[1][1].set_data(errorBounds, 10e-2*errorBounds**(-2))
    # lines[1][2].set_data(errorBounds, 10e-4*errorBounds**(-1))
    # figure.canvas.draw()
    figure.canvas.flush_events()
    # figure.canvas.draw()
    # plt.draw()

    figure.savefig("Thacker_1D_comparison_T1.pdf", format="pdf", dpi=300)
    plt.show(block=True)
    
