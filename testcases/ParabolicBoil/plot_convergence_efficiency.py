#! /usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np

# Load data
IterKinetic_Index_array    = np.loadtxt("IterKinetic_Index_convergence.txt")
IterKinetic_Index_N_sizes  = IterKinetic_Index_array[:,0] #.astype(int)
IterKinetic_Index_L2_err_H = IterKinetic_Index_array[:,1]
IterKinetic_Index_L2_err_Q = IterKinetic_Index_array[:,2]
IterKinetic_Index_times    = IterKinetic_Index_array[:,3]

IterKinetic_HalfDisk_array    = np.loadtxt("IterKinetic_HalfDisk_convergence.txt")
IterKinetic_HalfDisk_N_sizes  = IterKinetic_HalfDisk_array[:,0] #.astype(int)
IterKinetic_HalfDisk_L2_err_H = IterKinetic_HalfDisk_array[:,1]
IterKinetic_HalfDisk_L2_err_Q = IterKinetic_HalfDisk_array[:,2]
IterKinetic_HalfDisk_times    = IterKinetic_HalfDisk_array[:,3]

HLL_array    = np.loadtxt("HLL_convergence.txt")
HLL_N_sizes  = HLL_array[:,0] #.astype(int)
HLL_L2_err_H = HLL_array[:,1]
HLL_L2_err_Q = HLL_array[:,2]
HLL_times    = HLL_array[:,3]

KinExp_Index_array    = np.loadtxt("KinExp_convergence.txt")
KinExp_Index_N_sizes  = KinExp_Index_array[:,0] #.astype(int)
KinExp_Index_L2_err_H = KinExp_Index_array[:,1]
KinExp_Index_L2_err_Q = KinExp_Index_array[:,2]
KinExp_Index_times    = KinExp_Index_array[:,3]

# Op25 = IterKinetic_Index_N_sizes**(-.25)
# Op5  = IterKinetic_Index_N_sizes**(-.5)
O1 = IterKinetic_Index_N_sizes**(-1)
O2 = IterKinetic_Index_N_sizes**(-2)
# halfOrderRef = IterKinetic_Index_N_sizes**(-.5)
# firstOrderRef = IterKinetic_Index_N_sizes**(-1)/2
# secondOrderRef = IterKinetic_Index_N_sizes**(-2)*6

errorBounds = np.array([np.nanmin(IterKinetic_Index_L2_err_H),
                        np.nanmax(IterKinetic_Index_L2_err_H)])

figure = plt.figure(figsize=(9, 3.5))
plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.15,
                    wspace=0.25, hspace=0.)
axis = []
lines = []

#Free surface and bathymetry on the same subfigure
axis.append(figure.add_subplot(121))
axis[-1].axis([6, 300, 10e-6, 2*10e-2])
axis[-1].set_title("Experimental order of convergence")
lines.append(axis[-1].loglog([], [], 'k--', [], [], 'k-', [], [], 'go-', [], [], 'ys-', [], [], 'cd-', [], [], 'bv-', [], [], 'v--'))
lines[-1][5].set_label("Iterative half-disk")
lines[-1][4].set_label("Iterative index")
lines[-1][3].set_label("Explicit kinetic")
lines[-1][2].set_label("HLL")
lines[-1][0].set_label("O1")
lines[-1][1].set_label("O2")
lines[-1][5].set_color("#a6cee3") # #fb9a99
lines[-1][4].set_color("#1f78b4")
lines[-1][3].set_color('#b2df8a')
lines[-1][2].set_color("#33a02c")
axis[-1].set_xlabel("Number of cells")
axis[-1].set_ylabel("$ |\!| h - h_{exact} |\!|_{L^2} $")
axis[-1].legend(ncol=1, loc="best")
axis[-1].grid()

#L2 error per CPU time in a separate subfigure
axis.append(figure.add_subplot(122))
axis[-1].axis([10e-2, 2.5*10e-5, 3*10e-5, .15*10e-1])
# axis[-1].set_title("CPU time = f(L2 error)")
axis[-1].set_title("CPU time = $f\, (|\!| h - h_{exact} |\!|_{L^2})$")
lines.append(axis[-1].loglog([], [], 'go-', [], [], 'ys-', [], [], 'cd-', [], [], 'bv-'))
lines[-1][3].set_label("Iterative half-disk")
lines[-1][2].set_label("Iterative index")
lines[-1][1].set_label("Explicit kinetic")
lines[-1][0].set_label("HLL")
lines[-1][3].set_color("#a6cee3") # #fb9a99
lines[-1][2].set_color("#1f78b4")
lines[-1][1].set_color('#b2df8a')
lines[-1][0].set_color("#33a02c")
# lines[-1][4].set_label("X^2")
# lines[-1][5].set_label("X^1")
axis[-1].set_xlabel("$ |\!| h - h_{exact} |\!|_{L^2} $")
axis[-1].set_ylabel("CPU time (s)")
axis[-1].legend(ncol=1, loc="upper left")
axis[-1].grid()

#Plot data
lines[0][5].set_data(IterKinetic_HalfDisk_N_sizes, IterKinetic_HalfDisk_L2_err_H)
lines[0][4].set_data(IterKinetic_Index_N_sizes, IterKinetic_Index_L2_err_H)
lines[0][3].set_data(KinExp_Index_N_sizes, KinExp_Index_L2_err_H)
lines[0][2].set_data(HLL_N_sizes, HLL_L2_err_H)
lines[0][0].set_data(IterKinetic_Index_N_sizes, O1/1.5)
lines[0][1].set_data(IterKinetic_Index_N_sizes, O2)

lines[1][3].set_data(IterKinetic_HalfDisk_L2_err_H, IterKinetic_HalfDisk_times)
lines[1][2].set_data(IterKinetic_Index_L2_err_H, IterKinetic_Index_times)
lines[1][1].set_data(KinExp_Index_L2_err_H, KinExp_Index_times)
lines[1][0].set_data(HLL_L2_err_H, HLL_times)
# lines[1][1].set_data(errorBounds, 10e-2*errorBounds**(-2))
# lines[1][2].set_data(errorBounds, 10e-4*errorBounds**(-1))
# figure.canvas.draw()
figure.canvas.flush_events()
# figure.canvas.draw()
# plt.draw()
figure.savefig("Thacker_1D_convergence.pdf", format="pdf", dpi=300)
plt.show()
