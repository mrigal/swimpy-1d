#! /usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from lib import SaintVenant1D, Mesh1D, OutputManager
from lib.numerical_schemes import *
from lib.time_integrators import CustomTimeIntegrator, ExplicitRungeKutta
from lib.reconstruction_operators import *


def bathymetry(x):
    return -2.5*np.ones(x.shape)

def initCond_h(x):
    return - bathymetry(x) + (.3<=x)*(x<.7)*np.cos(.5*np.pi*(x-.5)/.2)**4

def initCond_q(x):
    return 0*initCond_h(x)

def FileDumper(om, cl, W, mesh, scheme):
    ic = mesh.GetInteriorCellsRange()
    z = mesh.GetBathymetryArray()[ic]
    X = mesh.GetCellCentersArray()[ic]
    h = W[ic,0]
    q = W[ic,1]
    array = np.stack((X, h + z, q), axis=1)
    fileID = om.OpenDataFile(binary=False, mode="w", ext="txt")
    # fileID.write("# Header\n")
    # fileID.write("# Comment: Position, Free surface, Discharge, Bathymetry\n")
    np.savetxt(fileID, array, fmt="%g")
    fileID.close()

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    line[0][0].set_data(X[ic], W[ic,0] + z[ic])
    line[0][1].set_data(X[ic], z[ic])
    line[1][0].set_data(X[ic], W[ic,1])
    # figure.canvas.draw()
    figure.canvas.flush_events()
    # plt.draw()
    # axis[0].plot()
    # plt.show(block=True)

if "__main__" == __name__:

    #Setting the conservation law and bathymetry
    g = 2.5
    cl = SaintVenant1D()
    cl.SetGravityConstant(g)
    cl.SetBathymetry(bathymetry)

    #Setting the mesh
    xMin = 0
    xMax = 1
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)

    #Setting the time integrator
    initialTime = 0
    finalTime = .5

    #Setting the output manager
    numberOutputs = 250
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    # outputTimes = [finalTime]
    om = OutputManager()
    om.SetOutputTimes(outputTimes)

    plt.ion()
    figure = plt.figure(figsize=(9, 3.5))
    plt.subplots_adjust(left=0.075, right=0.925, top=0.9, bottom=0.15)
    axis = []
    lines = []
    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(121))
    axis[-1].axis([0, 1, -2.5, 1.25])
    # axis[-1].axis([0, 1, -10e-11, 10e-11])
    axis[-1].set_title("Free surface")
    lines.append(axis[-1].plot([], [], 'b:', [], [], 'k-'))
    lines[-1][0].set_label("Free surface")
    lines[-1][1].set_label("Bathymetry")
    axis[-1].legend()
    plt.grid()
    #Discharge in a separate subfigure
    axis.append(figure.add_subplot(122))
    axis[-1].axis([0, 1, -2, 2])
    # axis[-1].axis([0, 1, -5*10e-10, 5*10e-10])
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'r--'))
    lines[-1][0].set_label("Discharge")
    axis[-1].legend()
    plt.grid()
    #Uploading features into output manager
    om.SetRealTimePlotFigure(figure)
    om.SetRealTimePlotAxis(axis)
    om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    ############################################################################
    #Setting the scheme with boundary conditions
    numberOfCells = 50 #30 #50 #100
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    CFL_number = .45
    
    scheme = ImplicitKineticScheme()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    # scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    scheme.SetPeriodicBNDC()

    om.SetBaseFilename("data/Kinetic_periodic_BC")
    om.SetDumpDataToFile(FileDumper)

    om.ActivatePlotRealTime()
    # om.ActivateDumpDataToFile()
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    # om.DeactivateDumpDataToFile()
    om.DeactivatePlotRealTime()

    W_Kinetic = mesh.GetCellData()
    ############################################################################
    #Setting the scheme with boundary conditions
    numberOfCells = 512 #30 #50 #100
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    ti = ExplicitRungeKutta()
    A = np.array([[0, 0], [1, 0]]) #Partial update weights
    b = np.array([.5, .5])         #Final update weights
    c = np.array([0, 1])           #Partial time stepping
    ti.SetButcherTableau(A, b, c, name="Heun")
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    CFL_number = .45
    
    scheme = RoeScheme()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    # ro = NoReconstruction()
    # ro = HydroRec()
    ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)
    scheme.SetPeriodicBNDC()

    om.SetBaseFilename("data/HLL_periodic_BC")
    om.SetDumpDataToFile(FileDumper)

    om.ActivatePlotRealTime()
    # om.ActivateDumpDataToFile()
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    # om.DeactivateDumpDataToFile()
    om.DeactivatePlotRealTime()

    W_HLL = mesh.GetCellData()
    ############################################################################
