#! /usr/bin/python2

import numpy as np
import matplotlib.pyplot as plt
from lib import SaintVenant1D, Mesh1D, OutputManager
from lib.numerical_schemes import *
from lib.time_integrators import CustomTimeIntegrator, ExplicitRungeKutta
from lib.reconstruction_operators import *

# def bathymetry(x):
#     return (1 + np.cos(4*np.pi*x))*(x >= .25)*(x < .75) - 10
#     # return -2.5*np.ones(x.shape)

# def initCond_h(x):
#     # return 0.1*np.sin(2*np.pi*x) - bathymetry(x)
#     return - bathymetry(x)

# def initCond_q(x):
#     #return .75*initCond_h(x)
#     return -0*np.ones(x.shape)

def initCond_h(X):
    Z = bathymetry(X)
    H = (X<=.6)*np.maximum(0.,4.5-Z)
    return H

def initCond_q(x):
    return np.zeros(x.shape)

def bathymetry(X):
    a = .5
    b = .7
    c = .9
    mu = .5*(a+b)
    # Z = (X>=a)*(X<mu)*(X-a)*(X-a)*(-1)*10/(mu-a)/(mu-a)/(mu-a)*(X - mu-.5*(mu-a))
    Z = (X>=a)*(X<mu)*2.5*(1 + np.sin(np.pi*(X-a)/(mu-a) - .5*np.pi))
    Z += (X>=mu)*(X<b)*(4 + np.cos(np.pi*(X-mu)/(b-mu)))
    Z += 3*(X>=b)*(X<c)
    Z += (X>=c)*(3 + 3*(1 + np.sin(np.pi*(X-c)/(1-c) - .5*np.pi)))
    return Z

def mixedBoundaryConditionsType(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    types = np.empty(nComponents)
    if X == 0:
        types[0] = Rusanov.NEUMANN_CONDITION
        types[1] = Rusanov.DIRICHLET_CONDITION#*(time < 0.1) + Rusanov.NEUMANN_CONDITION*(time > 0.2)
    elif X == 1:
        types[0] = Rusanov.DIRICHLET_CONDITION
        types[1] = Rusanov.NEUMANN_CONDITION
    else:
        raise ValueError("Unexpected boundary position.")
    return types

def mixedBoundaryConditionsValue(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    values = np.empty(nComponents)
    if X == 0:
        values[0] = 0.
        values[1] = 0*5*(1 - (time>0.01)*(1-np.exp(-20*(time-0.01))))
    elif X == 1:
        values[0] = 0.
        values[1] = 0.
    else:
        raise ValueError("Unexpected boundary position.")
    return values

def FileDumper(om, cl, W, mesh, scheme):
    ic = mesh.GetInteriorCellsRange()
    z = mesh.GetBathymetryArray()[ic]
    X = mesh.GetCellCentersArray()[ic]
    h = W[ic,0]
    q = W[ic,1]
    array = np.stack((X, h + z, q, z), axis=1)
    fileID = om.OpenDataFile(binary=False, mode="w", ext="txt")
    # fileID.write("# Header\n")
    # fileID.write("# Comment: Position, Free surface, Discharge, Bathymetry\n")
    np.savetxt(fileID, array, fmt="%g")
    fileID.close()

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    line[0][0].set_data(X[ic], W[ic,0] + z[ic])
    line[0][1].set_data(X[ic], z[ic])
    line[1][0].set_data(X[ic], W[ic,1])
    time = om.GetNextOutputTime()
    finalTime = om.GetFinalOutputTime()
    figure.suptitle("Iterative kinetic scheme + HydroRec_O2\nTime={:.5f} / {}".format(time, finalTime)) # vs Heun-Roe-HydroRec-O2
    # figure.canvas.draw()
    figure.canvas.flush_events()
    # plt.draw()
    # axis[0].plot()
    # plt.show(block=True)

if "__main__" == __name__:

    #Setting the conservation law and bathymetry
    g = 2.5
    cl = SaintVenant1D()
    cl.SetGravityConstant(g)
    cl.SetBathymetry(bathymetry)

    #Setting the mesh
    xMin = 0
    xMax = 1
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)

    #Setting the time integrator
    initialTime = 0
    finalTime = 1.5

    #Setting the output manager
    # numberOutputs = 10*30 #200
    numberOutputs = 150
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    # outputTimes = [finalTime]
    om = OutputManager()
    om.SetOutputTimes(outputTimes)

    plt.ion()
    # figure = plt.figure(figsize=(9, 4.5))
    figure = plt.figure(figsize=(18, 9))
    plt.subplots_adjust(left=0.075, right=0.925, top=0.775, bottom=0.15)
    axis = []
    lines = []
    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(121))
    axis[-1].axis([0, 1, -10.5, 10])
    # axis[-1].axis([0, 1, -10e-11, 10e-11])
    axis[-1].set_title("Free surface")
    lines.append(axis[-1].plot([], [], 'b:', [], [], 'k-'))
    lines[-1][0].set_label("Free surface")
    lines[-1][1].set_label("Bathymetry")
    axis[-1].legend()
    plt.xlabel("X position ({} cells)".format(150))
    plt.grid()
    #Discharge in a separate subfigure
    axis.append(figure.add_subplot(122))
    axis[-1].axis([0, 1, -4, 4])
    # axis[-1].axis([0, 1, -5*10e-10, 5*10e-10])
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'r--'))
    lines[-1][0].set_label("Discharge")
    axis[-1].legend()
    plt.xlabel("X position ({} cells)".format(150))
    plt.grid()
    #Uploading features into output manager
    om.SetRealTimePlotFigure(figure)
    om.SetRealTimePlotAxis(axis)
    om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    ############################################################################
    # #Setting the scheme with boundary conditions
    # numberOfCells = 100 #30 #50 #100
    # numberOfGhosts = 4 #Should be even, required for boundary conditions
    # mesh.SetUniformMesh(numberOfCells)
    # mesh.SetNumberOfGhosts(numberOfGhosts)
    # mesh.GenerateMesh()
    
    # ti = CustomTimeIntegrator()
    # ti.SetStartingTime(initialTime)
    # ti.SetEndingTime(finalTime)

    # CFL_number = .45

    # scheme = IterativeKineticScheme()
    # scheme.SetMaxwellian_Index()
    # # scheme.SetMaxwellian_HalfDisk()
    # # scheme.SetStoppingCriteria(scheme.TOLERANCE)
    # # scheme.SetStoppingCriteria(scheme.ENTROPY)
    # scheme.SetStoppingCriteria(scheme.TOLERANCE_AND_ENTROPY)
    # scheme.SetMaxIter(50)
    # scheme.SetTolerance(10**(-12))
    # scheme.SetTimeIntegrator(ti)
    # scheme.SetCFLNumber(CFL_number)
    # # scheme.SetNumericalViscosity(1.125)
    # #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    # scheme.SetResolveGravityTimescale(True)
    # # ro = NoReconstruction()
    # ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    # scheme.SetReconstructionOperator(ro)
    # # scheme.SetWeaklyPrescribedSubsonicRiemannBNDC_H(
    # #     boundaryConditionsType,
    # #     boundaryConditionsValue
    # # )
    # scheme.SetMixedBNDC(
    #     mixedBoundaryConditionsType,
    #     mixedBoundaryConditionsValue
    # )
    # # scheme.SetPeriodicBNDC()

    # om.SetBaseFilename("data_WetDry/Iterative_RiemannBC")
    # om.SetDumpDataToFile(FileDumper)

    # om.ActivatePlotRealTime()
    # #om.ActivateDumpDataToFile()
    # scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    # #om.DeactivateDumpDataToFile()
    # om.DeactivatePlotRealTime()

    # assert(False)
    ############################################################################
    #Setting the scheme with boundary conditions
    numberOfCells = 150 #30 #50 #100
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    ti = ExplicitRungeKutta()
    A = np.array([[0, 0], [1, 0]]) #Partial update weights
    b = np.array([.5, .5])         #Final update weights
    c = np.array([0, 1])           #Partial time stepping
    ti.SetButcherTableau(A, b, c, name="Heun")
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)
    
    CFL_number = .45
    
    scheme = HLL()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    # ro = NoReconstruction()
    # ro = HydroRec()
    ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)
    # scheme.SetWeaklyPrescribedSubsonicRiemannBNDC_H(
    #     boundaryConditionsType,
    #     boundaryConditionsValue
    # )
    scheme.SetMixedBNDC(
        mixedBoundaryConditionsType,
        mixedBoundaryConditionsValue
    )
    # scheme.SetPeriodicBNDC()

    om.SetBaseFilename("data_WetDry/HLL_RiemannBC")
    om.SetDumpDataToFile(FileDumper)

    om.ActivatePlotRealTime()
    #om.ActivateDumpDataToFile()
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    #om.DeactivateDumpDataToFile()
    om.DeactivatePlotRealTime()

    ############################################################################
