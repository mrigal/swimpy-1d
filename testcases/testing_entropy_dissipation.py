#! /usr/bin/python2

import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from lib import SaintVenant1D, Mesh1D, OutputManager
from lib.numerical_schemes import *
from lib.time_integrators import CustomTimeIntegrator, ExplicitRungeKutta
from lib.reconstruction_operators import *

def initCond_h(x):
    # return 20 + (x>=.25)*(x<=.75)*(np.cos(8*np.pi*(x-.5)) - 1)
    return - bathymetry(x)

def initCond_v(x):
    return 1 * np.ones(x.shape)

def initCond_q(x):
    h = initCond_h(x)
    v = initCond_v(x)
    return h*v

def bathymetry(x):
    return -5 + (.3<=x)*(x<=.7)*.5*(1 + np.cos(2*np.pi*(x-.5)/.4))
    # return -2.5*np.ones(x.shape)

def FileDumper(om, cl, W, mesh, scheme):
    ic = mesh.GetInteriorCellsRange()
    z = mesh.GetBathymetryArray()[ic]
    X = mesh.GetCellCentersArray()[ic]
    h = W[ic,0]
    # q = W[ic,1]
    # v = q/h
    v = cl.ComputeVelocity(W[ic,:])
    g = cl.GetGravityConstant()
    Dx = mesh.GetDx()[ic]
    K = 5.
    E = .5*h*v*v + .5*g*h*h + g*h*(z + K)
    print(np.sum(E*Dx))
    # array = np.stack((X, h + z, v, z + 17.5), axis=1)
    # fileID = om.OpenDataFile(binary=False, mode="w", ext="txt")
    # # fileID.write("# Header\n")
    # # fileID.write("# Comment: Position, Free surface, Discharge, Bathymetry\n")
    # np.savetxt(fileID, array, fmt="%g")
    # fileID.close()

def plotter(om, cl, W, mesh, scheme):
    ic = mesh.GetInteriorCellsRange()
    z = mesh.GetBathymetryArray()[ic]
    X = mesh.GetCellCentersArray()[ic]
    h = W[ic,0]
    # q = W[ic,1]
    # v = q/h
    v = cl.ComputeVelocity(W[ic,:])
    g = cl.GetGravityConstant()
    Dx = mesh.GetDx()[ic]
    K = 5.
    E = .5*h*v*v + .5*g*h*h + g*h*(z + K)
    time = om.GetNextOutputTime()
    
    fileID = open("total_energy.dat", "a")
    fileID.write(
        "{} {}\n".format(time, np.sum(E*Dx))
    )
    fileID.close()
    
    # line = om.GetRealTimePlotLines()
    # figure = om.GetRealTimePlotFigure()
    # axis = om.GetRealTimePlotAxis()
    # z = mesh.GetBathymetryArray()
    # X = mesh.GetCellCentersArray()
    # ic = mesh.GetInteriorCellsRange()
    # line[0][0].set_data(X[ic], W[ic,0] + z[ic])
    # line[0][1].set_data(X[ic], z[ic])
    # line[1][0].set_data(X[ic], W[ic,1])
    # # line[1][0].set_data(X[ic], W[ic,1]/W[ic,0])
    # time = om.GetNextOutputTime()
    # finalTime = om.GetFinalOutputTime()
    # figure.suptitle("Iterative kinetic scheme + HydroRec_O2\nTime={:.5f} / {}".format(time, finalTime)) # vs Heun-Roe-HydroRec-O2
    # # figure.canvas.draw()
    # figure.canvas.flush_events()
    # # plt.draw()
    # # axis[0].plot()
    # # plt.show(block=True)

if "__main__" == __name__:

    #Setting the conservation law and bathymetry
    g = 2.5
    cl = SaintVenant1D()
    cl.SetGravityConstant(g)
    cl.SetBathymetry(bathymetry)

    #Setting the mesh
    xMin = 0
    xMax = 1
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)

    #Setting the time integrator
    initialTime = 0
    finalTime = .05

    #Setting the output manager
    numberOutputs = 200
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    # outputTimes = [finalTime]
    om = OutputManager()
    om.SetOutputTimes(outputTimes)

    # plt.ion()
    # # figure = plt.figure(figsize=(9, 4.5))
    # figure = plt.figure(figsize=(18, 9))
    # plt.subplots_adjust(left=0.075, right=0.925, top=0.775, bottom=0.15)
    # axis = []
    # lines = []
    # #Free surface and bathymetry on the same subfigure
    # axis.append(figure.add_subplot(121))
    # axis[-1].axis([0, 1, -5.5, .5])
    # # axis[-1].axis([0, 1, -10e-11, 10e-11])
    # axis[-1].set_title("Free surface")
    # lines.append(axis[-1].plot([], [], 'b:', [], [], 'k-'))
    # lines[-1][0].set_label("Free surface")
    # lines[-1][1].set_label("Bathymetry")
    # axis[-1].legend()
    # plt.xlabel("X position ({} cells)".format(150))
    # plt.grid()
    # #Discharge in a separate subfigure
    # axis.append(figure.add_subplot(122))
    # axis[-1].axis([0, 1, 2.5, 7.5])
    # # axis[-1].axis([0, 1, -5*10e-10, 5*10e-10])
    # axis[-1].set_title("Discharge")
    # lines.append(axis[-1].plot([], [], 'r--'))
    # lines[-1][0].set_label("Discharge")
    # axis[-1].legend()
    # plt.xlabel("X position ({} cells)".format(150))
    # plt.grid()
    # #Uploading features into output manager
    # om.SetRealTimePlotFigure(figure)
    # om.SetRealTimePlotAxis(axis)
    # om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    ############################################################################
    #Setting the scheme with boundary conditions
    numberOfCells = 100 #30 #50 #100
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    CFL_number = .45

    scheme = IterativeKineticScheme()
    # scheme.SetMaxwellian_Index()
    scheme.SetMaxwellian_HalfDisk()
    scheme.SetAlpha(1)
    # scheme.SetStoppingCriteria(scheme.TOLERANCE)
    # scheme.SetStoppingCriteria(scheme.ENTROPY); scheme.SetAlpha(0)
    scheme.SetStoppingCriteria(scheme.TOLERANCE_AND_ENTROPY)
    scheme.SetMaxIter(200)
    scheme.SetTolerance(10**(-9))
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    # scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    # ro = NoReconstruction()
    ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)
    scheme.SetPeriodicBNDC()

    om.SetBaseFilename("data_EntropyDissip/Iterative_PerBC_SteadyState")
    om.SetDumpDataToFile(FileDumper)

    om.ActivatePlotRealTime()
    # om.ActivateDumpDataToFile()
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    # om.DeactivateDumpDataToFile()
    om.DeactivatePlotRealTime()

    totalEnergyIterative = np.loadtxt("total_energy.dat")
    os.remove("total_energy.dat")

    # assert(False)
    ############################################################################

        #Setting the scheme with boundary conditions
    numberOfCells = 100 #30 #50 #100
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    CFL_number = .45

    scheme = IterativeKineticScheme()
    # scheme.SetMaxwellian_Index()
    scheme.SetMaxwellian_HalfDisk()
    scheme.SetAlpha(1)
    # scheme.SetStoppingCriteria(scheme.TOLERANCE)
    scheme.SetStoppingCriteria(scheme.ENTROPY); scheme.SetAlpha(0)
    # scheme.SetStoppingCriteria(scheme.TOLERANCE_AND_ENTROPY)
    scheme.SetMaxIter(200)
    scheme.SetTolerance(10**(-9))
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    # scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    # ro = NoReconstruction()
    ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)
    scheme.SetPeriodicBNDC()

    om.SetBaseFilename("data_EntropyDissip/Iterative_PerBC_SteadyState")
    om.SetDumpDataToFile(FileDumper)

    om.ActivatePlotRealTime()
    # om.ActivateDumpDataToFile()
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    # om.DeactivateDumpDataToFile()
    om.DeactivatePlotRealTime()

    totalEnergyIterative_Entropy = np.loadtxt("total_energy.dat")
    os.remove("total_energy.dat")

    ############################################################################
    #Setting the scheme with boundary conditions
    numberOfCells = 100 #30 #50 #100
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    ti = ExplicitRungeKutta()
    
    # A = np.array([[0, 0], [1, 0]]) #Partial update weights
    # b = np.array([.5, .5])         #Final update weights
    # c = np.array([0, 1])           #Partial time stepping
    # ti.SetButcherTableau(A, b, c, name="Heun")
    # ti.SetStartingTime(initialTime)
    # ti.SetEndingTime(finalTime)

    A = np.array([[0.]]) #Partial update weights
    b = np.array([1.])         #Final update weights
    c = np.array([0.])           #Partial time stepping
    ti.SetButcherTableau(A, b, c, name="ForwardEuler")
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    CFL_number = .45
    
    scheme = HLL()
    # scheme = ExplicitKineticScheme()
    # # scheme.SetMaxwellian_Index()
    # scheme.SetMaxwellian_HalfDisk()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    # ro = NoReconstruction()
    ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)
    scheme.SetPeriodicBNDC()

    om.SetBaseFilename("data_EntropyDissip/ExpKin_PerBC_SteadyState")
    om.SetDumpDataToFile(FileDumper)

    om.ActivatePlotRealTime()
    # om.ActivateDumpDataToFile()
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    # om.DeactivateDumpDataToFile()
    om.DeactivatePlotRealTime()
    
    totalEnergyHLL = np.loadtxt("total_energy.dat")
    os.remove("total_energy.dat")

    ############################################################################
    #Setting the scheme with boundary conditions
    numberOfCells = 100 #30 #50 #100
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    ti = ExplicitRungeKutta()
    
    # A = np.array([[0, 0], [1, 0]]) #Partial update weights
    # b = np.array([.5, .5])         #Final update weights
    # c = np.array([0, 1])           #Partial time stepping
    # ti.SetButcherTableau(A, b, c, name="Heun")
    # ti.SetStartingTime(initialTime)
    # ti.SetEndingTime(finalTime)

    A = np.array([[0.]]) #Partial update weights
    b = np.array([1.])         #Final update weights
    c = np.array([0.])           #Partial time stepping
    ti.SetButcherTableau(A, b, c, name="ForwardEuler")
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    CFL_number = .45
    
    # scheme = HLL()
    scheme = ExplicitKineticScheme()
    # scheme.SetMaxwellian_Index()
    scheme.SetMaxwellian_HalfDisk()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    # ro = NoReconstruction()
    ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)
    scheme.SetPeriodicBNDC()

    om.SetBaseFilename("data_EntropyDissip/ExpKin_PerBC_SteadyState")
    om.SetDumpDataToFile(FileDumper)

    om.ActivatePlotRealTime()
    # om.ActivateDumpDataToFile()
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    # om.DeactivateDumpDataToFile()
    om.DeactivatePlotRealTime()
    
    totalEnergyExplicit = np.loadtxt("total_energy.dat")
    os.remove("total_energy.dat")

    ############################################################################
    #Setting the scheme with boundary conditions
    numberOfCells = 100 #30 #50 #100
    numberOfGhosts = 4 #Should be even, required for boundary conditions
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    CFL_number = .45

    scheme = IterativeKineticScheme()
    scheme.SetMaxwellian_Index()
    # scheme.SetMaxwellian_HalfDisk()
    scheme.SetAlpha(1)
    # scheme.SetStoppingCriteria(scheme.TOLERANCE)
    # scheme.SetStoppingCriteria(scheme.ENTROPY); scheme.SetAlpha(0)
    scheme.SetStoppingCriteria(scheme.TOLERANCE_AND_ENTROPY)
    scheme.SetMaxIter(200)
    scheme.SetTolerance(10**(-9))
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    # scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    # ro = NoReconstruction()
    ro = HydroRec()
    # ro = HydroRec_O2(); ro.SetLimiter(minmodLimiter, name="minmod")
    scheme.SetReconstructionOperator(ro)
    scheme.SetPeriodicBNDC()

    om.SetBaseFilename("data_EntropyDissip/Iterative_PerBC_SteadyState")
    om.SetDumpDataToFile(FileDumper)

    om.ActivatePlotRealTime()
    # om.ActivateDumpDataToFile()
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    # om.DeactivateDumpDataToFile()
    om.DeactivatePlotRealTime()

    totalEnergyIterative_Index = np.loadtxt("total_energy.dat")
    os.remove("total_energy.dat")
    ############################################################################
        
    # # print(totalEnergy)
    # plt.plot(totalEnergyIterative[:,0], totalEnergyIterative[:,1], label="Iterative")
    # plt.plot(totalEnergyExplicit[:,0], totalEnergyExplicit[:,1], label=("Explicit"))
    # # plt.legend("Iterative", "Explicit")
    # plt.xlabel("Time")
    # plt.ylabel("Total energy")
    # plt.title("Comparing kinetic schemes")
    # plt.grid()
    # plt.legend()
    # plt.savefig("total_energy.pdf")

    # os.remove("total_energy.dat")

    ############################################################################

    figure = plt.figure(figsize=(6, 4.5))
    plt.subplots_adjust(left=0.2, right=0.9, top=0.9, bottom=0.15,
                        wspace=0.25, hspace=0.)
    axis = []
    lines = []

    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(111))
    # axis.append(figure.addplot())
    axis[-1].axis([-0.005, 0.055, -0.0035, 0.0005])
    axis[-1].set_title("Total energy dissipation")
    lines.append(axis[-1].plot([], [], '-', [], [], ':', [], [], '-', [], [], '-', [], [], '--'))
    # lines.append(axis[-1].plot([], [], 'k-', [], [], 'gs', [], [], 'yo', [], [], 'c.', [], [], 'bx'))
    

    lines[-1][4].set_label("IterKin index (Tol+Entr.)")
    lines[-1][3].set_label("IterKin half-disk (Entropy)")
    lines[-1][2].set_label("IterKin half-disk (Tol+Entr.)")
    lines[-1][1].set_label("Explicit kinetic half-disk")
    lines[-1][0].set_label("Explicit HLL")
    lines[-1][4].set_linewidth("2")
    lines[-1][3].set_linewidth("2")
    lines[-1][2].set_linewidth("2")
    lines[-1][1].set_linewidth("2")
    lines[-1][0].set_linewidth("2")
    lines[-1][4].set_color("#a6cee3") # #fb9a99
    lines[-1][3].set_color("#1f78b4")
    lines[-1][2].set_color('#b2df8a')
    lines[-1][1].set_color("#33a02c")
    lines[-1][0].set_color("#fb9a99")
    axis[-1].set_xlabel("Time")
    axis[-1].set_ylabel("Total energy relative to initial value")
    axis[-1].legend(ncol=1, loc="best")
    axis[-1].grid()

    totalEnergyHLL[:,1] -= totalEnergyHLL[0,1]
    totalEnergyExplicit[:,1] -= totalEnergyExplicit[0,1]
    totalEnergyIterative[:,1] -= totalEnergyIterative[0,1]
    totalEnergyIterative_Index[:,1] -= totalEnergyIterative_Index[0,1]
    totalEnergyIterative_Entropy[:,1] -= totalEnergyIterative_Entropy[0,1]

    # axis[-1].yaxis.set_major_formatter(FormatStrFormatter('%.3e2'))
    axis[-1].ticklabel_format(axis='y', scilimits=[-3, 3])

    lines[-1][0].set_data(totalEnergyHLL[:,0], totalEnergyExplicit[:,1])
    lines[-1][1].set_data(totalEnergyExplicit[:,0], totalEnergyExplicit[:,1])
    lines[-1][2].set_data(totalEnergyIterative[:,0], totalEnergyIterative[:,1])
    lines[-1][3].set_data(totalEnergyIterative_Entropy[:,0], totalEnergyIterative_Entropy[:,1])
    lines[-1][4].set_data(totalEnergyIterative_Index[:,0], totalEnergyIterative_Index[:,1])

    # figure.savefig("total_energy_dissipation.png", format="png", dpi=600)
    figure.savefig("total_energy_dissipation.pdf", format="pdf", dpi=600)
    plt.show(block=True)

    os.remove("total_energy.dat")
