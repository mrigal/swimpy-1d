#! /usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
from lib import SaintVenant1D, Mesh1D, OutputManager
from lib.numerical_schemes import *
from lib.time_integrators import CustomTimeIntegrator
from lib.reconstruction_operators import *


def bathymetry(x):
    return -2.5*np.ones(x.shape)

def initCond_h(x):
    return - bathymetry(x)

def initCond_q(x):
    return np.zeros(x.shape)

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    line[0][0].set_data(X[ic], W[ic,0] + z[ic])
    line[0][1].set_data(X[ic], z[ic])
    line[1][0].set_data(X[ic], W[ic,1])
    # figure.canvas.draw()
    figure.canvas.flush_events()
    # plt.draw()

if "__main__" == __name__:

    #Setting the conservation law and bathymetry
    g = 9.81
    cl = SaintVenant1D()
    cl.SetGravityConstant(g)
    cl.SetBathymetry(bathymetry)

    #Setting the mesh
    xMin = 0
    xMax = 1
    numberOfCells = 50 #30 #50 #100
    numberOfGhosts = 2 #Should be even, required for boundary conditions
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()

    #Setting the time integrator
    initialTime = 0
    finalTime = .5

    # ti = ExplicitRungeKutta()
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    #Setting the output manager
    numberOutputs = 50
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    om = OutputManager()
    om.SetOutputTimes(outputTimes)

    plt.ion()
    figure = plt.figure(figsize=(9, 3.5))
    plt.subplots_adjust(left=0.075, right=0.925, top=0.9, bottom=0.15)
    axis = []
    lines = []
    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(121))
    axis[-1].axis([0, 1, -10e-11, 10e-11])
    axis[-1].set_title("Free surface")
    lines.append(axis[-1].plot([], [], 'b:', [], [], 'k-'))
    lines[-1][0].set_label("Free surface")
    lines[-1][1].set_label("Bathymetry")
    axis[-1].legend()
    plt.grid()
    #Discharge in a separate subfigure
    axis.append(figure.add_subplot(122))
    axis[-1].axis([0, 1, -5*10e-10, 5*10e-10])
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'r--'))
    lines[-1][0].set_label("Discharge")
    axis[-1].legend()
    plt.grid()
    #Uploading features into output manager
    om.SetRealTimePlotFigure(figure)
    om.SetRealTimePlotAxis(axis)
    om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    #Setting the scheme with boundary conditions
    CFL_number = .45
    scheme = ImplicitKineticScheme()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.125)
    #Set to False to speedup computations by neglecting gravity waves (large timesteps)
    scheme.SetResolveGravityTimescale(True)
    scheme.SetPeriodicBNDC()

    om.ActivatePlotRealTime()
    scheme.Solve(cl, mesh, initialCondition, om, silent=False)
    om.DeactivatePlotRealTime()
