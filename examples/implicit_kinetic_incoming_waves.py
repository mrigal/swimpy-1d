#! /usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from lib import SaintVenant1D, Mesh1D, OutputManager
from lib.numerical_schemes import *
from lib.time_integrators import *
from lib.reconstruction_operators import *

def bathymetry(x):
    # return (1 + np.cos(4*np.pi*x))*(x >= .25)*(x < .75) - 2.5
    return -2.5*np.ones(x.shape)

def initCond_h(x):
    # return 0.1*np.sin(2*np.pi*x) - bathymetry(x)
    return - bathymetry(x)

def initCond_q(x):
    # return .75# *initCond_h(x)
    return np.ones(x.shape)

def boundaryConditionsType(X, time):
    raise NotImplementedError

def boundaryConditionsValue(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    values = np.empty(nComponents)
    if X == 0:
        value = initCond_h(X) + .1*(1 - np.sin(8*np.pi*time))
    elif X == 1:
        value = initCond_h(X) # + .1*(1 - np.cos(4*np.pi*time)) # + .5*np.cos(.5*np.pi*time)
    else:
        raise ValueError("Unexpected boundary position.")
    return value

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    line[0][0].set_data(X[ic], W[ic,0] + z[ic])
    line[0][1].set_data(X[ic], z[ic])
    line[1][0].set_data(X[ic], W[ic,1])
    time = om.GetNextOutputTime()
    finalTime = om.GetFinalOutputTime()
    figure.suptitle("Time={:.5f} / {}".format(time, finalTime))
    # figure.canvas.draw()
    figure.canvas.flush_events()
    # plt.draw()

if "__main__" == __name__:

    #Setting the conservation law and bathymetry
    g = 9.81
    # Froude = 1E1
    # g = 1/Froude/Froude
    cl = SaintVenant1D()
    cl.SetGravityConstant(g)
    cl.SetBathymetry(bathymetry)

    # Froude = 10**(-1)
    # cl.SetFroudeCaracteristicNumber(Froude)

    #Setting the mesh
    xMin = 0
    xMax = 1
    numberOfCells = 64
    numberOfGhosts = 2 #Should be even, required for boundary conditions
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()

    #Setting the time integrator
    initialTime = 0
    finalTime = .2

    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    # A = np.array([[0,]])
    # b = np.array([1.])
    # c = np.array([0.])
    # ti.SetButcherTableau(A, b, c, name="Explicit Euler")

    # A = np.array([[0, 0], [1, 0]])
    # b = np.array([.5, .5])
    # c = np.array([0, 1])
    # ti.SetButcherTableau(A, b, c, name="Heun")

    # A = np.array([[0, 0], [.5, 0]])
    # b = np.array([0, 1])
    # c = np.array([0, .5])
    # ti.SetButcherTableau(A, b, c, name="Midpoint")

    #Setting the output manager
    numberOutputs = 200
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    om = OutputManager()
    om.SetOutputTimes(outputTimes)

    # subplotGrid = [121, 122]
    # styles = ['b-', 'r-']
    # om.SetPlotRealTime(plotter_h, subplots=subplotGrid, linestyles=styles)

    plt.ion()
    figure = plt.figure(figsize=(9, 3.5))
    # figure.tight_layout()
    plt.subplots_adjust(left=0.075, right=0.925, top=0.85, bottom=0.15)
    axis = []
    lines = []
    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(121))
    axis[-1].axis([0, 1, -2.5, 1])
    axis[-1].set_title("Free surface")
    lines.append(axis[-1].plot([], [], 'b:', [], [], 'k-'))
    lines[-1][0].set_label("Free surface")
    lines[-1][1].set_label("Bathymetry")
    axis[-1].legend()
    #Discharge in a separate subfigure
    axis.append(figure.add_subplot(122))
    axis[-1].axis([0, 1, -1.5, 4])
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'r--'))
    lines[-1][0].set_label("Discharge")
    axis[-1].legend()
    #Uploading features into output manager
    om.SetRealTimePlotFigure(figure)
    om.SetRealTimePlotAxis(axis)
    om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    #Setting the scheme with boundary conditions
    CFL_number = .45
    # scheme = Rusanov()
    # scheme = HLL()
    # scheme = RoeScheme()
    # scheme = ExplicitKineticScheme()
    scheme = ImplicitKineticScheme()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.)
    #Set to False to speedup computations by neglecting gravity waves
    #(large timesteps)
    # scheme.SetResolveGravityTimescale(True)
    ro = NoReconstruction() #Optional, set to NoReconstruction by default
    # ro = MUSCL_O2()
    # ro = HydroRec()
    # ro = HydroRec_O2()
    scheme.SetReconstructionOperator(ro)
    scheme.SetWeaklyPrescribedSubsonicRiemannBNDC_H(
        boundaryConditionsType,
        boundaryConditionsValue 
    )
    # scheme.SetMixedBNDC(
    #     mixedBoundaryConditionsType,
    #     mixedBoundaryConditionsValue
    # )
    # scheme.SetPeriodicBNDC()

    om.ActivatePlotRealTime()
    om.DeactivateDumpDataToFile()

    scheme.Solve(cl, mesh, initialCondition, om, silent=False)

    om.DeactivatePlotRealTime()
