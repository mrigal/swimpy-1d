#! /usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from lib import OutputManager

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    line[0][0].set_data(X[ic], W[ic,0] + z[ic])
    line[0][1].set_data(X[ic], z[ic])
    line[1][0].set_data(X[ic], W[ic,1])
    figure.canvas.flush_events()


if "__main__" == __name__:

    initialTime = 0
    finalTime = 1.5

    #Setting the output manager
    numberOutputs = 100
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    om = OutputManager()
    om.SetOutputTimes(outputTimes)

    #Figure will refresh itself instantaneously
    plt.ion()
    figure = plt.figure(figsize=(9, 3.5))
    axis = []
    lines = []

    #Tighter margins
    plt.subplots_adjust(left=0.075, right=0.925, top=0.9, bottom=0.15)
    
    #Free surface and bathymetry on the same subfigure
    axis.append(figure.add_subplot(121))
    axis[-1].axis([0, 1, -2.5, 1]) #Can be incorporated in the plotter for dynamic axis
    axis[-1].set_title("Free surface")
    lines.append(axis[-1].plot([], [], 'b:', [], [], 'k-'))
    lines[-1][0].set_label("Free surface")
    lines[-1][1].set_label("Bathymetry")
    axis[-1].legend()
    
    #Discharge in a separate subfigure
    axis.append(figure.add_subplot(122))
    axis[-1].axis([0, 1, -1.5, 2])
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'r--'))
    lines[-1][0].set_label("Discharge")
    axis[-1].legend()
    
    #Interfacing plotter features with output manager
    om.SetRealTimePlotFigure(figure)
    om.SetRealTimePlotAxis(axis)
    om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    om.ResetOutputCounter() #Not required when SchemeClass.Solve() method is called
    om.ActivatePlotRealTime()

    print(om)
