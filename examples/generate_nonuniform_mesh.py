#! /usr/bin/python3

import numpy as np
from lib import Mesh1D

if "__main__" == __name__:

    print("Generating nonuniform mesh providing ghost vertices")

    listOfVertices = [-.125, 0, .125, .375, .625, .875, 1, 1.125]
    numberOfGhosts = 2 #Should be even
    mesh = Mesh1D()
    mesh.SetNonuniformMesh(listOfVertices, ghosts=True)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    #Allocate data array
    mesh.SetCellDataSize(3) #Three values per cell: h, q, z
    mesh.AllocateCellData()
    
    ic = mesh.GetInteriorCellsRange()
    
    data = mesh.GetCellData()
    data[ic,0] = 1
    data[ic,1] = .5
    data[ic,2] = -.25
    
    print(mesh)
    print(mesh.GetCellData())
    
    print("\nGenerating nonuniform mesh without providing ghost vertices")
    
    listOfVertices = [0, .125, .375, .625, .875, 1]
    numberOfGhosts = 4 #Should be even
    mesh = Mesh1D()
    mesh.SetNonuniformMesh(listOfVertices, ghosts=False)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    #Allocate data array
    mesh.SetCellDataSize(3) #Three values per cell: h, q, z
    mesh.AllocateCellData()
    
    ic = mesh.GetInteriorCellsRange()
    
    data = mesh.GetCellData()
    data[ic,0] = 1
    data[ic,1] = .5
    data[ic,2] = -.25
    
    print(mesh)
    print(mesh.GetVerticesArray())
    print(mesh.GetCellData())
    print("")
