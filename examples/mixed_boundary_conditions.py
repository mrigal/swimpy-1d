#! /usr/bin/python3

from lib import SaintVenant1D
from lib.numerical_schemes import Rusanov
from lib.time_integrators import ExplicitEuler
from lib.reconstruction_operators import HydroRec

def bathymetry(x):
    return (1 + np.cos(4*np.pi*x))*(x >= .25)*(x < .75) - 2.5
def initCond_h(x):
    return 0.1*np.sin(2*np.pi*x) - bathymetry(x)
def initCond_q(x):
    return .25*initCond_h(x)

def mixedBoundaryConditionsType(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    types = np.empty(nComponents)
    if X == 0:
        types[0] = Rusanov.DIRICHLET_CONDITION
        types[1] = Rusanov.NEUMANN_CONDITION
    elif X == 1:
        types[1] = Rusanov.DIRICHLET_CONDITION
        types[0] = Rusanov.NEUMANN_CONDITION
    else:
        raise ValueError("Unexpected boundary position.")
    return types

def mixedBoundaryConditionsValue(X, time):
    nComponents = SaintVenant1D.SYSTEM_SIZE
    values = np.empty(nComponents)
    if X == 0:
        values[0] = 2.5*(1 - .05*np.sin(time/4))
        values[1] = 0.
    elif X == 1:
        values[0] = 0.
        values[1] = 2.5*.25*(1 + .2*np.sin(3*time))
    else:
        raise ValueError("Unexpected boundary position.")
    return values

if "__main__" == __name__:

    #Setting the time integrator
    initialTime = 0
    finalTime = 1.5
    timeIntegrator = ExplicitEuler()
    timeIntegrator.SetStartingTime(initialTime)
    timeIntegrator.SetEndingTime(finalTime)

    print(timeIntegrator)

    #Setting the scheme to Rusanov with explicit euler time integration and
    #first order hydrostatic reconstruction
    CFL_number = 0.45
    scheme = Rusanov()
    scheme.SetTimeIntegrator(timeIntegrator)
    scheme.SetCFLNumber(CFL_number)
    ro  = HydroRec()
    scheme.SetReconstructionOperator(ro)
    scheme.SetMixedBNDC(
        mixedBoundaryConditionsType,
        mixedBoundaryConditionsValue
    )

    print(scheme)
