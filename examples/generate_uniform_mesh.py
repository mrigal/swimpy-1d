#! /usr/bin/python3

from lib import Mesh1D

if "__main__" == __name__:
    
    xMin = 0
    xMax = 1
    numberOfCells = 2
    numberOfGhosts = 2 #Should be even
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()
    
    #Allocate data array
    mesh.SetCellDataSize(3) #Three values per cell: h, q, z
    mesh.AllocateCellData()
    
    ic = mesh.GetInteriorCellsRange()
    
    data = mesh.GetCellData()
    data[ic,0] = 1
    data[ic,1] = .5
    data[ic,2] = -.25
    
    print(mesh)
    print(mesh.GetCellData())
