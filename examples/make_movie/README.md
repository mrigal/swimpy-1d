# Generating videos from data files

The procedure is described in the following steps. The idea is to automatically generate a tikz TEX file for each data file, then generate pdf files by looping over every TEX files. Finally, the pdf files are converted into JPEG ones and assembled into a video. The generation of the TEX file is controlled from the script generate_pdf.sh, which can be modified to adapt the number of data files, their name and the appearance of the plot, as well as the labels, title, legends, etc...

To summarize, those are the steps to follow:

* Generate the data files with swimpy;
* In generate_pdf.sh, set the number of data files and their base name accordingly;
* Further customize generate_pdf.sh if necessary;
* Run the previous script with the command "./generate_pdf.sh";
* Compile the video with "make movie";
* The generated file is named video.mp4;
* Clean the folder with "make clean" or "make cleanall" (won't delete the video);
