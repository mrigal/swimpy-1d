#! /bin/bash
for i in {0..50}
do
    y=$(printf "%03d" $i)
    # time=$(python3 -c "print( 'time={:.2f}s/{}s'.format($i / float(299) * .5, .5))")
    time=$(python3 -c "print( '\$\\\text{{time}}={{\\\tt {:.2f}}}/{{\\\tt {}}}\$'.format($i / float(50) * .5, .5))")
   echo > document_${y}.tex "\RequirePackage{luatex85}
\documentclass{standalone}
\usepackage{pgfplots}
\pgfplotsset{compat=1.15}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{empheq}
\usepackage{latexsym}
\usepackage{amsmath}

\begin{document}
    \begin{tikzpicture}
      \begin{axis}[
          width=9cm, height=6cm,
          grid=both, %major or minor
	  minor x tick num=1,
	  minor y tick num=2,
	  minor grid style={dashed,very thin,black!12!white},
          xlabel={\$x\$ position (512 cells)},
          xmin=0., xmax=1.,
          ylabel={Free surface},
          title={Implicit Kinetic O1 vs Roe+hydorRecO2, ($time)},
          ymin=-1.25, ymax=1.25,
          enlarge x limits=false,
          legend cell align={left},
          legend pos=outer north east,
          %% legend style={at=south west ,anchor=south west}
          legend entries={Roe, Kinetic},
          legend style={font=\footnotesize},
          max space between ticks={36},
        ]
	\addplot[
        draw=black,
        thick,
        red,
        mark=none,
        ] table[
        x index=0, y index=1,
        ] {HLL_periodic_BC_$y.txt};
        \addplot[
        draw=black,
        thick,
        blue,
        mark=none,
        ] table[
        x index=0, y index=1,
        ] {Kinetic_periodic_BC_$y.txt};
      \end{axis}
    \end{tikzpicture}
  \end{document}"
done

# for i in {0..10}
# do
#     sem -j+0
#     lualatex document_$y.tex
#     rm document_$y.tex
# done
# sem --wait

# for i in {0..100}
# do
#     lualatex document_$y.tex &
#     convert -density 400 input.pdf picture.png
# done
