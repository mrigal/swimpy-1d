#! /usr/bin/python3

from lib import SaintVenant1D
from lib.numerical_schemes import Rusanov
from lib.time_integrators import ExplicitEuler
from lib.reconstruction_operators import HydroRec


if "__main__" == __name__:

    #Setting the time integrator
    initialTime = 0
    finalTime = 1.5
    timeIntegrator = ExplicitEuler()
    timeIntegrator.SetStartingTime(initialTime)
    timeIntegrator.SetEndingTime(finalTime)

    print(timeIntegrator)

    #Setting the scheme to Rusanov with explicit euler time integration and
    #first order hydrostatic reconstruction
    CFL_number = 0.45
    scheme = Rusanov()
    scheme.SetTimeIntegrator(timeIntegrator)
    scheme.SetCFLNumber(CFL_number)
    ro = HydroRec()
    scheme.SetReconstructionOperator(ro)
    scheme.SetPeriodicBNDC()

    print(scheme)
