#! /usr/bin/python3

import numpy as np
from lib.time_integrators import ExplicitRungeKutta


if "__main__" == __name__:

    #Setting the time integrator
    initialTime = 0
    finalTime = 1.5
    timeIntegrator = ExplicitRungeKutta()
    timeIntegrator.SetStartingTime(initialTime)
    timeIntegrator.SetEndingTime(finalTime)

    #Butcher tableau corresponding to the Heun method (trapezoidal rule)
    A = np.array([[0, 0], [1, 0]]) #Partial update weights
    b = np.array([.5, .5])         #Final update weights
    c = np.array([0, 1])           #Partial time stepping
    timeIntegrator.SetButcherTableau(A, b, c, name="Heun")

    timeIntegrator.InitializeTimer()

    print(timeIntegrator)
