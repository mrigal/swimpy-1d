#! /usr/bin/python3

from lib import SaintVenant1D, Mesh1D
from lib.numerical_schemes import Rusanov
from lib.time_integrators import ExplicitEuler
from lib.reconstruction_operators import *


if "__main__" == __name__:

    #Setting the mesh
    xMin = 0
    xMax = 1
    numberOfCells = 128
    #At least 4 ghost cells required for the second order hydrostatic
    #reconstruction
    numberOfGhosts = 4 
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()

    #Setting the time integrator
    initialTime = 0
    finalTime = 1.5
    ti = ExplicitEuler()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    #Setting the scheme with second order hydrostatic reconstruction
    CFL_number = .45
    scheme = Rusanov()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    # ro = NoReconstruction() #Optional, set to NoReconstruction by default
    # ro = MUSCL_O2()
    ro = HydroRec()
    # ro = HydroRec_O2()
    # ro.SetLimiter(minmodLimiter, name="minmod") #Optional, default value is minmod
    scheme.SetReconstructionOperator(ro)
    scheme.SetPeriodicBNDC()

    print(scheme)
