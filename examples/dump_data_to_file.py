#! /usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from lib import Mesh1D, OutputManager


def dump_data_to_file(om, cl, W, mesh, scheme):
    ic = mesh.GetInteriorCellsRange()
    X = mesh.GetCellCentersArray()[ic]
    array = np.concatenate((X[:,np.newaxis], W), axis=1)
    fileID = om.OpenDataFile(binary=False, mode="w", ext="txt")
    fileID.write("# Header\n")
    fileID.write("# Comment: Position, Free surface, Discharge, Bathymetry\n")
    np.savetxt(fileID, array, fmt="%g")
    fileID.close()


if "__main__" == __name__:

    # Generate mesh
    mesh = Mesh1D()
    xMin = 0
    xMax = 1
    numberOfCells = 4
    numberOfGhosts = 2 #Should be even
    mesh.SetDomainBounds(xMin, xMax)
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()

    #Setting the output manager
    om = OutputManager()
    initialTime = 0
    finalTime = 1.5
    numberOutputs = 50
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    om.SetOutputTimes(outputTimes)

    om.SetBaseFilename("testing_output")
    om.SetDumpDataToFile(dump_data_to_file)
    om.ResetOutputCounter()

    # Data arrays
    dataArray_1 = np.array(
        [[0., 0.25, -0.5],
         [-0.1, 0.25, -0.4],
         [-0.1, 0.125, -0.3],
         [0., 0., -0.2]]
    )

    dataArray_2 = np.array(
        [[0., 0.2, -0.5],
         [-0.2, 0.1, -0.4],
         [0.2, 0.05, -0.3],
         [0., -0.05, -0.2]]
    )

    om.ActivateDumpDataToFile() # By default it is deactivated
    
    fileName_1 = om.GetDataFileName(ext="txt")
    print("Generating file {}.".format(fileName_1))
    om.DoDumpDataToFile(None, dataArray_1, mesh, None)
    om.IncreaseOutputCounter()
    
    fileName_2 = om.GetDataFileName(ext="txt")
    print("Generating file {}.".format(fileName_2))
    om.DoDumpDataToFile(None, dataArray_2, mesh, None)
    om.IncreaseOutputCounter()

    print(om)
    
    om.DeactivateDumpDataToFile()

    #Read the second file
    print("Reading file {}.".format(fileName_2))
    fileID = open(fileName_2, "r")
    print(fileID.readline()[:-1]) #Commented lines
    print(fileID.readline()[:-1])
    readArray = np.loadtxt(fileID) #Loading data array
    print(readArray)

    fileID.close()
