#! /usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from lib import SaintVenant1D, Mesh1D, OutputManager
from lib.numerical_schemes import ImplicitKineticScheme
from lib.time_integrators import CustomTimeIntegrator

def flatBathymetry(x):
    return -10*np.ones(x.shape)

def initCond_h(x):
    return - flatBathymetry(x)

def initCond_q(x):
    return -0*np.ones(x.shape)

def boundaryConditionsValue(X, time):
    #Enforce water height given by initial condition to preserve lake at rest
    value = initCond_h(X)
    return value

def plotter(om, cl, W, mesh, scheme):
    line = om.GetRealTimePlotLines()
    figure = om.GetRealTimePlotFigure()
    axis = om.GetRealTimePlotAxis()
    z = mesh.GetBathymetryArray()
    X = mesh.GetCellCentersArray()
    ic = mesh.GetInteriorCellsRange()
    line[0][0].set_data(X[ic], W[ic,0] + z[ic])
    line[1][0].set_data(X[ic], W[ic,1])
    time = om.GetNextOutputTime()
    finalTime = om.GetFinalOutputTime()
    figure.suptitle("Time={:.5f} / {}".format(time, finalTime))
    figure.canvas.flush_events()

if "__main__" == __name__:

    #Setting the conservation law and bathymetry
    g = 9.81
    cl = SaintVenant1D()
    cl.SetGravityConstant(g)
    cl.SetBathymetry(flatBathymetry)

    #Setting the mesh
    xMin = 0
    xMax = 1
    numberOfCells = 64
    numberOfGhosts = 2 #Should be even, required for boundary conditions
    mesh = Mesh1D()
    mesh.SetDomainBounds(xMin, xMax)
    mesh.SetUniformMesh(numberOfCells)
    mesh.SetNumberOfGhosts(numberOfGhosts)
    mesh.GenerateMesh()

    #Setting the time integrator
    initialTime = 0
    finalTime = .5
    ti = CustomTimeIntegrator()
    ti.SetStartingTime(initialTime)
    ti.SetEndingTime(finalTime)

    #Setting the output manager
    numberOutputs = 100
    outputTimes = np.linspace(initialTime, finalTime, numberOutputs)
    om = OutputManager()
    om.SetOutputTimes(outputTimes)

    plt.ion()
    figure = plt.figure(figsize=(9, 3.5))
    plt.subplots_adjust(left=0.075, right=0.925, top=0.85, bottom=0.15)
    axis = []
    lines = []
    #Free surface on left subfigure
    axis.append(figure.add_subplot(121))
    axis[-1].axis([0, 1, -2*10e-11, 2*10e-11])
    axis[-1].set_title("Free surface")
    lines.append(axis[-1].plot([], [], 'b:'))
    lines[-1][0].set_label("Free surface")
    axis[-1].legend()
    plt.grid()
    #Discharge in right subfigure
    axis.append(figure.add_subplot(122))
    axis[-1].axis([0, 1, -3*10e-8, 3*10e-8])
    axis[-1].set_title("Discharge")
    lines.append(axis[-1].plot([], [], 'r--'))
    lines[-1][0].set_label("Discharge")
    axis[-1].legend()
    plt.grid()
    #Uploading features into output manager
    om.SetRealTimePlotFigure(figure)
    om.SetRealTimePlotAxis(axis)
    om.SetRealTimePlotLines(lines)
    om.SetRealTimePlotter(plotter)

    #Setting the initial condition
    initialCondition = [initCond_h, initCond_q]

    #Setting the scheme with boundary conditions
    CFL_number = .45
    scheme = ImplicitKineticScheme()
    scheme.SetTimeIntegrator(ti)
    scheme.SetCFLNumber(CFL_number)
    scheme.SetNumericalViscosity(1.)
    
    #Set to False to speedup computations by neglecting gravity waves
    #(large timesteps)
    scheme.SetResolveGravityTimescale(False)

    scheme.SetWeaklyPrescribedSubsonicRiemannBNDC_H(
        None, boundaryConditionsValue,
    )

    om.ActivatePlotRealTime()
    om.DeactivateDumpDataToFile()

    scheme.Solve(cl, mesh, initialCondition, om, silent=False)

    om.DeactivatePlotRealTime()
