#! /usr/bin/python

import numpy as np
from lib.SaintVenant1D import *
from lib.Mesh1D import *
from lib.numerical_schemes.SchemeClass import *
from lib.time_integrators.TimeIntegratorClass import *
from lib.OutputManager import *

def bndConditionType(X, time): #periodic bnd
    return np.ones((X.shape[0], 2))

def bndConditionValue(X, time): #periodic bnd
    return np.zeros((X.shape[0], 2))

def initCond_h(X):
    return 5*np.ones(X.shape)

def initCond_q(X):
    return -.25*np.ones(X.shape)

def bathymetry(X):
    return np.zeros(X.shape)

initCond = [initCond_h, initCond_q]

cl = SaintVenant1D()
cl.SetGravityConstant(9.81)
cl.SetBathymetry(bathymetry)

xMin = 0
xMax = 1
numberOfCells = 4
numberOfGhosts = 2 #Should be even
mesh = Mesh1D()
mesh.SetDomainBounds(xMin, xMax)
mesh.SetUniformMesh(numberOfCells)
mesh.SetNumberOfGhosts(numberOfGhosts)
mesh.GenerateMesh()

#Allocate data array
mesh.SetCellDataSize(2) #Three values per cell: h, q, z
mesh.AllocateCellData()

ti = TimeIntegratorClass()
om = OutputManager()

scheme = SchemeClass(ti)
scheme.SetBoudaryConditions(bndConditionType, bndConditionValue)
scheme.InitializeScheme(cl, mesh, initCond, om)

W = np.zeros((mesh.GetNumberOfCells(), 2))
ic = mesh.GetInteriorCellsRange()
W[ic,0] = initCond_h(np.array(ic))
W[ic,1] = initCond_q(np.array(ic))

print(mesh.GetCellData())
data = scheme.ApplyBNDC(cl, mesh, W, 0) #scheme.ApplyBNDC(cl, mesh, data, time)
mesh.SetCellData(data)
print(mesh.GetCellData())
